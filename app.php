<?php
/**
 * Phalcon Project Environment configuration file.
 * Requires PhalconPHP installed
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//include CrazyCake phalcon loader
require is_link(__DIR__."/packages/cc-phalcon") ? __DIR__."/packages/cc-phalcon/autoload.php" : __DIR__."/packages/cc-phalcon.phar";

class PhalconApp extends \CrazyCake\Phalcon\AppLoader
{
    /**
     * Required app configuration
     */
    protected function config()
    {
        return [
            //Project Path
            "projectPath" => __DIR__."/",
            //Module Settings
            "modules" => [
                "frontend" => [
                    "loader"    => ["models", "helpers"],
                    "core"      => ["facebook", "checkout", "tickets", "qr", "transbank", "mobile"],
                    "langs"     => ["es"],
                    "version"   => "1.5.0",
                    //production
                    "baseUrl"   => "https://www.liveon.cl/",
                    //"staticUrl" => "https://cdn.liveon.cl/frontend/",
                    "enableSSL" => true
                ],
                "backend" => [
                    "loader"  => ["models", "frontend/models"],
                    "core"    => ["checkout", "tickets"],
                    "langs"   => ["es"],
                    "version" => "0.0.5",
                    //production
                    "baseUrl" => "http://darkside.backend.space.time.liveon.cl/"
                ],
                "api" => [
                    "loader"     => ["models", "frontend/models",],
                    "core"       => ["facebook", "checkout", "tickets", "mobile"],
                    "langs"      => ["es"],
                    "version"    => "1.1.2",
                    "key"        => "*Cr4ZyCak3_L1v30n?", //HTTP header API Key (basic security)
                    "keyEnabled" => true,
                    //production
                    "baseUrl"    => "https://darkside.api.phidib.system.liveon.cl/",
                ],
                "cli" => [
                    "loader" => ["models", "frontend/models"],
                    "core"   => ["checkout"]
                ]
            ],
            //App DI properties [$this->config->app->property]
            "app" => [
                //project properties
                "name"      => "LiveOn", //App name
                "namespace" => "liveon", //App namespace (no usar espacios, underscore ni guiones)
                //crypto
                "cryptKey" => 'CC$LOx*', //Cypher seed
                //emails
                "emails" => [
                    "sender"  => "app@liveon.cl",
                    "support" => "soporte@liveon.cl",
                    "contact" => "contacto@liveon.cl"
                ],
                //push-services
                "gcm" => [
                    "apiKey" => "AIzaSyAYP648Qfk9YmtBrwRDRJ5LIjDARS5QuCc"
                ],
                "apn" => [
                    "entrustCaCertFile" => "pushservices/apn_entrust.ca.cer",
                    "devPemFile"        => "pushservices/apn_dev.pem",
                    "prodPemFile"       => "pushservices/apn_prod.pem",
                    "passphrase"        => "xxxxxx"
                ],
                //mandrill
                "mandrill" => [
                    "accessKey" => "RE3z78hfRD-qJm4EIlVnVw", //Mandrill API Key
                ],
                //amazon [Bucket is defined by environment]
                "aws" => [
                    "accessKey" => "AKIAI6K4UWDZMKGOKV4Q",                     //Access Key
                    "secretKey" => "NYSY0Ajiig3HPPt2CpFZMs9dA7zyVlOr5GpqVpWa", //Secret Key
                    "s3Bucket"  => "liveon"                                    //S3 bucket prefix name (example: liveon-dev)
                ],
                //google
                "google" => [
                    "analyticsUA"  => "UA-48396138-2",                            //Analytics UA identifier (for frontend)
                    "reCaptchaID"  => "6LcPEgATAAAAAFgS7BYStybvZ841I7VWt80rjy-2", //reCaptcha ID (frontend & backend)
                    "reCaptchaKey" => "6LcPEgATAAAAANVUzYEvY_gTjW61LF4DhOIlJnyT", //reCaptcha Secret key (frontend & backend)
                ],
                //facebook
                "facebook" => [
                    "appScope"     => "email,user_birthday,user_likes,publish_actions", //app required permissions
                    "appID"        => "356284327833424",                                //app ID
                    "appKey"       => "6a23299d03315fe732ceeedbe8d6b483",               //app Secret key
                    "webhookToken" => "4M1hICQowx4Lq4GoTn13IX7FSiq2mY7H"                //webhooks token
                ],
                //instagram
                "instagram" => [
                    "appScope" => "basic",                                   //app required permissions
                    "appID"    => "ef012b1e3af243a0b8fcdbc87fcddd5a",        //app ID
                    "appKey"   => "3b52d51f3cb3440c89b9b29b961b83e6",        //app Secret key
                    "appUrl"   => "http://instagram.com/liveonapp?ref=badge" //default app URL
                ]
            ]
        ];
    }
}
