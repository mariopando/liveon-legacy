<?php
/**
 * Phalcon App Routes files
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

return function($app) {

	//welcome message
	$app->get('/', [new WsCoreController(), "welcome"]);

	//fetch event(s)
	$app->get('/sync/event/basic', [new WsEventController(), "basicData"]);

	//get event assets
	$app->get('/sync/event/exchange', [new WsEventController(), "exchangeData"]);

	//get event tickets
	$app->get('/sync/event/tickets', [new WsEventController(), "ticketsData"]);

	//get event tickets
	$app->get('/sync/event/social', [new WsEventController(), "socialData"]);

	//get users registered at given event
	$app->get('/sync/event/users', [new WsUserEventTicketController(), "users"]);

	//get users fallback data
	$app->get('/event/users/fallback', [new WsUserEventTicketController(), "usersFallback"]);

	//@dev get event-user hash, receives a ticket hash
	$app->get('/event/user/ticket', [new WsUserEventTicketController(), "ticket"]);

	//event users socials actions, defaults facebook
	$app->post('/event/user/social/action', [new WsUserSocialActionController(), "facebookAction"]);

	//generate event invitations codes
	$app->post('/event/invitations', [new WsEventController(), "invitationsCodes"]);

	//cleans and event
	$app->get('/event/clean', [new WsEventController(), "eventClean"]);

	//push notifications subscribe
	$app->post('/push/subscribe', [new WsPushNotificationController(), "subscribeAction"]);

	//push notification send
	$app->post('/push/send', [new WsPushNotificationController(), "sendAction"]);

	//queue a push notification
	$app->post('/push/queue', [new WsPushNotificationController(), "queueNotification"]);

	//push notification received
	$app->post('/push/received', [new WsPushNotificationController(), "receivedAction"]);

	//not found handler
	$app->notFound( function() use (&$app) {
		//$app->response->setStatusCode(404, "Not Found")->sendHeaders();
		$service = new WsCoreController();
		$service->serviceNotFound();
	});
};
