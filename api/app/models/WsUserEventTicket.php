<?php

//extended from linked model file
class WsUserEventTicket extends UserEventTicket
{
    /**
     * Database Table Source
     */
    public function getSource()
    {
        return "user_event_ticket";
    }

    /** ------------------------------------------ § ------------------------------------------------- **/

    /**
     * Get event users data for API owner device (RAW SQL)
     * Event info is not necesary, just a model bridge
     * @access public
     * @static
     * @param int $event_id - The event ID
     * @param boolean $extended - Extended properties
     * @return array
     */
    public static function getByEventId($event_id = 0, $extended = false)
    {
        //extended properties
        $extended_props = $extended ? ", et.name AS ticket_name, uet.code AS ticket_code, et.id AS ticket_id" : "";

        //query to get UserEventTicket. TODO: include ticket day filter
        $sql = "SELECT u.id, u.first_name, u.last_name, uet.code, uet.qr_hash, uf.id AS fb_id
                       $extended_props
                FROM user_event_ticket AS uet
                INNER JOIN event_ticket AS et
                    ON (et.id = uet.ticket_id)
                INNER JOIN user u
                    ON (u.id = uet.user_id)
                LEFT OUTER JOIN user_facebook uf
                    ON (u.id = uf.user_id)
                WHERE et.event_id = ?
                GROUP BY uet.id
                ORDER BY uet.id ASC";
        //print_r($sql);exit;

        $result = self::getByQuery($sql, [$event_id], "UserEventTicket");

        if(!$result)
            return null;

        //orm to array
        $result = $result->toArray();

        //append default assets to result
        self::_appendExchangeData($event_id, $result);

        return $result;
    }

    /** ------------------------------------------ § ------------------------------------------------- **/

    /**
     * Appends default assets
     * @static
     * @param int $event_id - The event ID
     * @param array $result - The result array
     */
    private static function _appendExchangeData($event_id = 0, &$result)
    {
        //loop result
        foreach ($result as &$object) {
            //var_dump($object);exit;

            //get event ticket assets data
            $eassets = self::getByQuery(
               "SELECT uet.qr_hash, ea.namespace, eta.default_quantity, COUNT(ueta.id) AS user_quantity
                FROM event_ticket_asset eta
                    INNER JOIN easset ea ON (ea.id = eta.easset_id)
                    INNER JOIN user_event_ticket uet ON (uet.qr_hash = ?)
                    INNER JOIN event_ticket et ON (eta.ticket_id = et.id)
                    LEFT OUTER JOIN user_event_ticket_asset ueta ON (
                        ueta.user_event_ticket_id = uet.id AND
                        ueta.event_ticket_asset_id = eta.id AND
                        ueta.state = 'ready'
                    )
                WHERE et.event_id = ?
                ",
                [$object["qr_hash"], $event_id],
                "UserEventTicketAsset"
            );

            if(!$eassets)
                continue;

            if(!isset($object["exchange"]))
                $object["exchange"] = [];

            foreach ($eassets as $easset) {

                $total = (int)$easset->default_quantity + (int)$easset->user_quantity;

                if(empty($total))
                    continue;

                $object["exchange"][$easset->namespace] = $total;
            }

            if(empty($object["exchange"]))
                unset($object["exchange"]);
        }
    }
}
