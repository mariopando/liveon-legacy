<?php

//other imports
use CrazyCake\Mobile\BasePushNotification;

class WsPushNotification extends BasePushNotification
{
    /**
     * Database Table Source
     */
    public function getSource()
    {
        return "push_notification";
    }
}
