<?php

//extended from linked model file
class WsEvent extends Event
{
    /**
     * Database Table Source
     */
    public function getSource()
    {
        return "event";
    }

    /** ------------------------------------------ § ------------------------------------------------- **/

    /**
     * Get events with basic data (filter by id & state)
     * @access public
     * @static
     * @param int $event_id
     * @return Event
     */
    public static function getBasicData($event_id = 0)
    {
        $conditions = empty($event_id) ? "id > 0" : "id = '".(int)$event_id."'";
        $conditions .= " AND (state = 'open' OR state = 'invisible')";
        //get event
        $events = self::find($conditions);

        if(!$events)
            return [];

        //create a split struct
        $result = [];
        foreach ($events as $event) {

            $obj = $event->toArray();

            //append extended props
            $is_running = $event->isRunning();
            $obj["is_running"] = (bool)$is_running;

            //append places
            $eventPlace  = EventPlace::findFirstByEventId($event->id);
            $obj["place"] = $eventPlace->eplace->toArray();

            //push object
            $result[] = $obj;
        }

        return $result;
    }
}
