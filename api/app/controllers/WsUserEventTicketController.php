<?php
/**
 * WsUsersTicketsController: API for users objects
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

class WsUserEventTicketController extends WsCoreController
{
    /**
     * Constructor Event
     */
    protected function onConstruct()
    {
        //call parent construct 1st
        parent::onConstruct();

        //extended error codes
        $this->CODES['1700'] = "user-hash not found";
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * (GET) Gets a User by ticket hash
     * @return json response
     */
    public function ticket()
    {
        //get params
        $data = $this->_handleRequestParams([
            "hash" => "string"
        ], "GET");

        //get ticket associated in user events tickets
        $user   = UserEventTicket::getUserByQrHash($data["hash"]);
        $ticket = UserEventTicket::findFirstByQrHash($data["hash"]);

        //otherwise, ticket not exists
        if(!$ticket)
            $this->_sendJsonResponse(1700);

        //get ticket
        $eventTicket = EventTicket::getById($ticket->ticket_id);
        $event       = $eventTicket->event;

        //get user, if exists
        if($user = User::getById($ticket->user_id))
            $user = $user->toArray(["first_name", "last_name", "email"]);

        //set response
        $response = [
            "user" => $user,
            "ticket" => [
                "code"     => $ticket->code,
                "name"     => $eventTicket->name,
                "price"    => $eventTicket->price,
                "currency" => $eventTicket->currency,
            ],
            "event" => [
                "id"   => $event->id,
                "name" => $event->name
            ]
        ];

        //send response
        $this->_sendJsonResponse(200, $response);
    }

    /**
     * (GET) Get last generated setup of event users
     * @return json response
     */
    public function users()
    {
        $data = $this->_handleRequestParams([
            "@refresh" => "int"
        ], "GET");

        $event = $this->_handleObjectIdRequestParam("event_id", true);
        //get data
        $result = WsUserEventTicket::getByEventId($event->id);
        //handle response
        $this->_handleCacheResponse("sync_".$event->id, $result, (boolean)$data["refresh"]);
    }

    /**
     * (GET) Gets event users data for fallback actions
     */
    public function usersFallback()
    {
        $data = $this->_handleRequestParams([
            "@refresh" => "int"
        ], "GET");

        $event = $this->_handleObjectIdRequestParam("event_id", true);
        //get data
        $result = WsUserEventTicket::getByEventId($event->id, true);
        //handle response
        $this->_handleCacheResponse("fallback_".$event->id, $result, (boolean)$data["refresh"]);
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */
}
