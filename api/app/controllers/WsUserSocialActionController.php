<?php
/**
 * WsUsersSocialController: API for facebook user object
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//Facebook PHP SDK
use Facebook\Exceptions\FacebookSDKException;
//other imports
use CrazyCake\Facebook\FacebookActions;

class WsUserSocialActionController extends WsCoreController
{
    /* Trait */
    use FacebookActions;

    /**
     * Constructor Event
     */
    protected function onConstruct()
    {
        //call parent construct 1st
        parent::onConstruct();

        //extended error codes
        $this->CODES['1500'] = "invalid qr hash";
        $this->CODES['1505'] = "failed while publishing action";
        $this->CODES['1510'] = "post skipped";

        //init trait
        $this->initFacebookActions([
            //setup
            "upload_path"       => PUBLIC_PATH."uploads/temp/",
            "publish_day_limit" => (APP_ENVIRONMENT == "production"), //false for debug,
            //objects
            "object_fb_relation"  => "eventFacebook",
            "og_namespace"        => "liveon_app",
            "og_story_object"     => "event",
            "og_story_action"     => "attend",
            "og_default_place_id" => "203114826397169",
            "og_default_message"  => "Acceso digital a tu mundo relevante."
        ]);
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * (POST) Publish a new facebook action in users feed throught Facebook API
     * @return json response
     */
    public function facebookAction()
    {
        //get user already validated (user_id is required)
        $data = $this->_handleRequestParams([
            "qr_hash" => "string",
            "action"  => "string",
            "@type"   => "string"
        ], "POST");

        //check action
        if(!in_array($data["action"], UserSocialAction::$ACTIONS))
            $this->_sendJsonResponse(400);

        //get user
        $user   = UserEventTicket::getUserByQrHash($data["qr_hash"]);
        $ticket = UserEventTicket::findFirstByQrHash($data["qr_hash"]);

        //check user & ticket
        if(!$user || !$ticket)
            $this->_sendJsonResponse(1500);
        //print_r($ticket->toArray());exit;

        //get event
        $event = $ticket->eventTicket->event;

        if(!$event)
            $this->_sendJsonResponse(400);

        try {
            //publish action
            $object_id = $this->publish($user->userFacebook, $event, $data);

            if(!$object_id)
                throw new Exception("Received an empty FB object id");

            UserSocialAction::saveAction(
                $user->id,
                $event->id,
                $data["action"],
                "facebook",
                $object_id
            );

            //OK SUCCESS
            $this->_sendJsonResponse(200, $object_id);
        }
        catch (Exception $e) {

            //save failed action
            UserSocialAction::saveAction(
                $user->id,
                $event->id,
                $data["action"],
                "facebook",
                null,
                'failed'
            );

            $this->logger->error('WsUserSocialController::facebookAction -> An error ocurred posting action. User:'.$user->id.' Error:'.$e->getMessage());
            //Error
            $this->_sendJsonResponse(1505, $e->getMessage());
        }
    }

    /**
     * Required Trait method
     * @param object $object The orm object
     * @return array
     */
    public function setStoryData($object)
    {
        return [
            "title"       => $object->name,
            "description" => $object->caption,
            "place"       => $object->eventPlace->eplace->name,
            "url"         => $object->_ext["landing_url"],
            "image"       => $object->_ext["image_url_square"]
        ];
    }
}
