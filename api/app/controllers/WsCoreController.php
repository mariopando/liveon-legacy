<?php
/**
 * WsCoreController: Main API controller
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//CrazyCake
use CrazyCake\Core\WsCore;

class WsCoreController extends WsCore
{
    /**
     * Constructor Event
     */
    protected function onConstruct()
    {
        //call parent construct 1st
        parent::onConstruct();
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Welcome API
     */
    public function welcome()
    {
        $payload = "Welcome to CrazyCake ".$this->config->app->name." API (v. ".$this->version.")";

        $this->_sendJsonResponse(200, $payload);
    }
}
