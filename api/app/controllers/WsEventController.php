<?php
/**
 * WsEventController: API for event object
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

class WsEventController extends WsCoreController
{
    /**
     * Constructor Event
     */
    protected function onConstruct()
    {
        //call parent construct 1st
        parent::onConstruct();

        //extended error codes
        $this->CODES['1200'] = "invalid invitation data";
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * (GET) fetch events with basic data (filter by id)
     * @param event_id (optional)
     * @return json response
     */
    public function basicData()
    {
        //get event
        $event = $this->_handleObjectIdRequestParam("event_id", true);

        $event_id = $event ? $event->id : null;

        //get model data
        $payload = WsEvent::getBasicData($event_id);

        //send response
        $this->_sendJsonResponse(200, $payload);
    }

    /**
     * (GET) returns the event assets exchange data
     * @param event_id
     * @return json response
     */
    public function exchangeData()
    {
        //get event
        $event = $this->_handleObjectIdRequestParam("event_id");

        //set payload
        $result = EventTicketAsset::getByEventId($event->id);

        $payload = [];
        foreach ($result as $object) {
            $easset = array_merge(
                $object->toArray(['default_quantity, price, currency']),
                $object->easset->toArray()
            );
            //append props
            $payload[] = $easset;
        }

        //send response
        $this->_sendJsonResponse(200, $payload);
    }

    /**
     * (GET) returns the event tickets data
     * @param event_id
     * @return json response
     */
    public function ticketsData()
    {
        //get event
        $event = $this->_handleObjectIdRequestParam("event_id");

        //get objects
        $result = EventTicket::findByEventId($event->id);
        //set payload
        $payload = $result ? $result->toArray() : [];

        //send response
        $this->_sendJsonResponse(200, $payload);
    }

    /**
     * (GET) returns the event social data
     * @param event_id
     * @return json response
     */
    public function socialData()
    {
        //get event
        $event = $this->_handleObjectIdRequestParam("event_id");

        //get objects
        $eventSocials = EventSocial::findFirstByEventId($event->id);
        //set payload
        $payload = $eventSocials ? $eventSocials->toArray(["required"]) : null;

        if(empty($payload))
            $payload = ["required" => "0"];

        //append fb data
        //if($fbData = EventFacebook::findFirstByEventId($event->id))
        //    $payload["facebook"] = $fbData->toArray(["hashtag", "checkin_url", "checkin_text", "story_text", "photo_text"]);

        //send response
        $this->_sendJsonResponse(200, $payload);
    }

    /**
     * (POST) generate event invitations codes
     * @param event_id
     * @return json response
     */
    public function invitationsCodes()
    {
        $data = $this->_handleRequestParams([
            "ticket_id"    => "int",
            "quantity"     => "int",
        ], "POST");

        try {
            //check quantity
            if(empty($data["quantity"]) || $data["quantity"] > 3000)
                throw new Exception("quantity invalid value");

            //check ticket id
            if(!EventTicket::getById($data["ticket_id"]))
                throw new Exception("EventTicket with ID '".$data["ticket_id"]."' not found");

            //set counter;
            $count = 0;
            $codes = [];

            //start trx
            $this->db->begin();
            for ($i = 0; $i < $data["quantity"]; $i++) {

                //new invitation code
                $object = new EventInvitation();
                $object->event_ticket_id = $data["ticket_id"];

                if(!$object->save())
                    throw new Exception("Error saving object, ".$object->filterMessages(true));

                $count++;
                $codes[] = $object->code;
            }
            $this->db->commit();
            //end trx

            $payload = ["count" => $count];

            if(APP_ENVIRONMENT != "production")
                $payload["codes"] = $codes;

            //send response
            $this->_sendJsonResponse(200, $payload);
        }
        catch (Exception $e) {
            $this->_sendJsonResponse(1200, $e->getMessage());
        }
    }

    /**
     * [GET] Cleans and event, only for testing
     */
    public function eventClean()
    {
        if(APP_ENVIRONMENT == "production") {
            $this->_sendJsonResponse(404);
        }

        //handle object
        $event = $this->_handleObjectIdRequestParam("event_id");
        //clean event data, first get Tickets
        $tickets = $event->eventTicket;

        //loop
        foreach ($tickets as $ticket) {

            //clean user_event_ticket
            $ticket = UserEventTicket::findByTicketId($ticket->id);
            $ticket->delete();
        }

        //delete cached json files
        $this->_cleanCacheResponse();
        //send response
        $this->_sendJsonResponse(200, "done");
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */
}
