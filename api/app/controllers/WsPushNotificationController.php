<?php
/**
 * WsPushNotifications
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//other imports
use CrazyCake\Mobile\PushManager;
use CrazyCake\Mobile\PushWebServices;

class WsPushNotificationController extends WsCoreController
{
    /* Trait */
    use PushManager;
    use PushWebServices;

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * (POST) Queue a push notification
     * @return json response
     */
    public function queueNotification()
    {
        $data = $this->_handleRequestParams([
            "payload" => ""
        ], "POST");

        try {
            //decrypt data
            $data = (array)$this->cryptify->decryptData($data["payload"], true);

            if(empty($data["action"]) || empty($data["objects"]))
                throw new Exception("Action & object params are required");

            //set push object
            $push = [
                "service" => "gcm",
                "uuids"   => WsPushNotification::getSubscribers("gcm", true),
                "message" => "LiveOn Event Notification",
                "payload" => [] //set it later
            ];
            $this->logger->debug("WsPushNotification::queueNotification -> received data: ".json_encode($data));

            //get objects
            $objects = $data["objects"];

            //action cases
            switch ($data["action"]) {
                //new user event ticket
                case "new_user_event_ticket": $action = "new_user"; break;
                //permission has updated
                case "facebook_perm_updated": $action = "fb_state"; break;
                //de fault
                default: $action = $data["action"]; break;
            }

            //for each object ID
            foreach ($objects as $object_id) {

                //get event user
                $event_user = WsEvent::getEventUserForApi($object_id);

                if(!$event_user) continue;

                //set payload [action is the same for all pushes]
                $push["payload"] = [
                    "action" => $action,
                    "data"   => $event_user
                ];
                //send push notification
                $this->sendNotification($push);
            }

            //send response
            $this->_sendJsonResponse(200, $push);
        }
        catch(Exception $e) {
            //log Error
            $this->logger->error("WsPushNotificationController::queueNotification -> an error occurred: ".$e->getMessage());
            //send response
            $this->_sendJsonResponse(500, $e->getMessage());
        }
    }
}
