-- +++ Queries +++ --

-- Compras exitosas web desde user_checkout --
-- web --
SELECT uc.buy_order, DATE_FORMAT(uc.local_time, '%d/%m/%Y') AS date,
	(
		SELECT (COUNT(uco.id)*uco.quantity) AS count
		FROM user_checkout_object uco
			INNER JOIN event_ticket et ON et.id = uco.object_id
		WHERE uco.buy_order = uc.buy_order
			AND uco.object_class = 'EventTicket'
			AND (et.event_id = 6)
	) AS num_items,
	(
		SELECT SUM(et.price*uco.quantity)
		FROM user_checkout_object uco
			INNER JOIN event_ticket et ON et.id = object_id
		WHERE buy_order = uc.buy_order
			AND object_class = 'EventTicket'
			AND (et.event_id = 6)
	 ) AS amount,
	'WebPay' AS order_type,
	trx.type
FROM user_checkout uc
	INNER JOIN user_checkout_trx trx ON trx.buy_order = uc.buy_order
WHERE uc.state = 'success'
HAVING num_items > 0
ORDER BY date ASC;


-- Compras exitosas web con impuestos --
-- web --
SELECT uc.buy_order, DATE_FORMAT(uc.local_time,'%d-%m-%Y') AS date,
	'WebPay' AS order_type,
	trx.type,
	trx.trx_id,
	et.name AS ticket_name,
	uco.quantity AS cantidad, et.price AS precio_ticket, et.currency, (uco.quantity*et.price) AS total_brute,
	(0.19*(uco.quantity*et.price)) AS IVA_charge,
	(IF(trx.type="credit", 0.035105*(uco.quantity*et.price), 0.019159*(uco.quantity*et.price) )) AS tbk_charge,
	((uco.quantity*et.price) - 0.19*(uco.quantity*et.price)) AS total_with_IVA,
	(uco.quantity*et.price - (IF(trx.type="credit", 0.035105*(uco.quantity*et.price), 0.019159*(uco.quantity*et.price) ))) AS total_with_tbk_charge
FROM user_checkout uc
	INNER JOIN user_checkout_object uco ON uco.buy_order = uc.buy_order
	INNER JOIN event_ticket et ON et.id = uco.object_id
	INNER JOIN user_checkout_trx trx ON trx.buy_order = uc.buy_order
WHERE uc.state = "success"
	AND uco.object_class = 'EventTicket'
	AND (et.event_id = 6)
ORDER BY buy_order, date ASC;


-- Total Entradas activas --
SELECT et.id, et.name,
	(@total_web:=(SELECT COUNT(uet.ticket_id) FROM user_event_ticket uet WHERE et.id = uet.ticket_id)) AS total_web,
	(@total_web) AS total
FROM event_ticket et
WHERE et.event_id = 6;


-- Total assets activos --
SELECT id, name, SUM(counter) AS total
FROM (
	SELECT ea.id, ea.name, COUNT(ea.id) AS counter
	FROM event_ticket_asset eta
		INNER JOIN event_ticket et ON et.id = eta.ticket_id
		INNER JOIN easset ea ON ea.id = eta.easset_id
		INNER JOIN user_event_ticket_asset ueta ON ueta.event_ticket_asset_id = eta.id
	WHERE et.event_id = 1
	GROUP BY ea.id
	UNION
	SELECT ea.id, ea.name, 0 AS counter
	FROM event_ticket_asset eta
		INNER JOIN event_ticket et ON et.id = eta.ticket_id
		INNER JOIN easset ea ON ea.id = eta.easset_id
		LEFT OUTER JOIN user_event_ticket_asset ueta ON ueta.event_ticket_asset_id IS NULL
	WHERE et.event_id = ?
	GROUP BY ea.id
) AS stats
GROUP BY id


-- Total Usuarios que tienen una entrada y están asociados a Facebook --
SELECT u.first_name, u.last_name, u.email, uf.id AS facebook_id, uf.created_at
FROM user_event_ticket uet
	INNER JOIN users u ON u.id = uet.user_id
	LEFT OUTER JOIN user_facebook uf ON uf.user_id = u.id
	INNER JOIN event_ticket et ON et.id = uet.ticket_id
	INNER JOIN event e ON e.id = et.event_id
	LEFT OUTER JOIN user_event_field uef ON (uef.user_id = u.id AND uef.efield_id)
WHERE
	uet.ticket_id = 11
GROUP BY u.id;


-- Usuarios que estan participando en un evento (Marketing DB)
SELECT u.first_name, u.last_name, u.email, u.gender, u.birthday,
	   et.name AS ticket_name, uet.code AS ticket_code, uet.qr_hash AS ticket_hash
FROM user_event_ticket uet
	INNER JOIN user u ON u.id = uet.user_id
	INNER JOIN event_ticket et ON et.id = uet.ticket_id
	INNER JOIN event e ON e.id = et.event_id
WHERE
	et.event_id = 6
GROUP BY uet.code
ORDER BY u.email ASC;

-- Usuarios que tienen más de una entrada --
SELECT u.email, u.first_name, u.last_name,
	(SELECT COUNT(DISTINCT id) FROM user_event_ticket WHERE user_id = u.id) AS total_tickets
FROM user_event_ticket uet
	INNER JOIN event_ticket et ON et.id = uet.ticket_id
	INNER JOIN event e ON e.id = et.event_id
	INNER JOIN users u ON u.id = uet.user_id
WHERE
	et.event_id = 1 OR
	et.event_id = 2 OR
	et.event_id = 3
GROUP BY u.id
HAVING
	total_tickets > 1;


-- Usuarios con entradas distintas --
SELECT u.email, u.first_name, u.last_name, et.name, uet.code,
	(SELECT COUNT(DISTINCT ticket_id) FROM user_event_ticket WHERE user_id = u.id) AS total_tickets
FROM user_event_ticket uet
	INNER JOIN event_ticket et ON et.id = uet.ticket_id
	INNER JOIN event e ON e.id = et.event_id
	INNER JOIN users u ON u.id = uet.user_id
WHERE
	et.event_id = 1 OR
	et.event_id = 2
HAVING
	total_tickets > 1
ORDER by uet.user_id, u.email ASC


-- Usuarios que han comprado exitosamente ciertas entradas --
SELECT u.email, u.first_name, u.last_name,
	(SELECT COUNT(DISTINCT id) FROM user_event_ticket WHERE user_id = u.id) AS total_tickets
FROM user_event_ticket uet
	INNER JOIN event_ticket et ON et.id = uet.ticket_id
	INNER JOIN event e ON e.id = et.event_id
	INNER JOIN user u ON u.id = uet.user_id
	INNER JOIN user_checkout uc ON (uc.user_id = u.id AND uc.state = 'success')
WHERE
	uet.ticket_id = 1 OR uet.ticket_id = 2 OR uet.ticket_id = 4
GROUP BY u.id
HAVING
	total_tickets = 1


-- Users event actions --
SELECT *
FROM user_social_action
WHERE state = 'success'
	AND action = 'story'

-- Transferencias de entradas exitosas --
SELECT uett.*, uet.ticket_id, uet.code, uet.qr_hash,
		u.first_name, u.last_name, u.email
FROM user_event_ticket_transfer uett
INNER JOIN user_event_ticket uet ON uet.id = uett.user_event_ticket_id
INNER JOIN user u ON uet.user_id = u.id
WHERE uett.state = 'success'


-- Users tickets sin buy order --
SELECT uet.id, uet.user_id, uet.ticket_id, uet.code, uc.buy_order, uet.created_at,
	   u.first_name, u.last_name, u.email,
	   uett.id AS transfer_id,
	   ei.id AS invitation_id
FROM user_event_ticket uet
	LEFT OUTER JOIN user_checkout uc ON uc.user_id = uet.user_id
	LEFT OUTER JOIN user u ON u.id = uet.user_id
	LEFT OUTER JOIN user_event_ticket_transfer uett ON uett.user_event_ticket_id = uet.id
	LEFT OUTER JOIN event_invitation ei ON ei.activation_user_id = uet.user_id
WHERE uc.buy_order IS NULL
