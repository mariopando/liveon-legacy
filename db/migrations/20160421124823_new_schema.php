<?php

use Phinx\Migration\AbstractMigration;

class NewSchema extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute("rename table eassets to easset");
        $this->execute("rename table efields to efield");
        $this->execute("rename table eplaces to eplace");
        $this->execute("rename table events to event");
        $this->execute("rename table events_facebook to event_facebook");
        $this->execute("rename table events_fields to event_field");
        $this->execute("rename table events_invitations to event_invitation");
        $this->execute("rename table events_places to event_place");
        $this->execute("rename table events_socials to event_social");
        $this->execute("rename table events_tickets to event_ticket");
        $this->execute("rename table events_tickets_assets to event_ticket_asset");
        $this->execute("rename table users to user");
        $this->execute("rename table users_checkouts to user_checkout");
        $this->execute("rename table users_checkouts_objects to user_checkout_object");
        $this->execute("rename table users_checkouts_trx to user_checkout_trx");
        $this->execute("rename table users_events_fields to user_event_field");
        $this->execute("rename table users_events_tickets to user_event_ticket");
        $this->execute("rename table users_events_tickets_assets to user_event_ticket_asset");
        $this->execute("rename table users_events_tickets_transfers to user_event_ticket_transfer");
        $this->execute("rename table users_facebook to user_facebook");
        $this->execute("rename table users_facebook_pages to user_facebook_page");
        $this->execute("rename table users_socials_actions to user_social_action");
        $this->execute("rename table users_tokens to user_token");
        $this->execute("rename table push_notifications to push_notification");

        //props
        $this->execute("ALTER TABLE `user_checkout_object` CHANGE `object_class` `object_class` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL");


        $this->execute("ALTER TABLE `event_social` DROP FOREIGN KEY `event_social_ibfk_1`; ALTER TABLE `event_social` ADD CONSTRAINT `event_social_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `event`(`id`) ON DELETE CASCADE ON UPDATE CASCADE");
    }
}
