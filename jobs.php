<?php
/**
 * App CronJobs
 * Add this line to your crontab file:
 * 		* * * * * cd /path/to/project && php jobs.php 1>> /dev/null 2>&1
 * @link https://github.com/jobbyphp/jobby
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//include CrazyCake phalcon loader
require "app.php";

try {
    //new phalcon app
    $app = new PhalconApp("cli");

    //jobby instance
    $jobby = new \Jobby\Jobby();

    //cleans pending checkouts [cada 10 min]
    $jobby->add("FrontendUserCheckoutCleaner", [
        "command"  => "php cli/cli.php main userCheckoutCleaner",
        "schedule" => "*/10 * * * *",
        "output"   => "frontend/app/logs/jobs_".date("d-m-Y").".log",
        "enabled"  => true,
    ]);

    //token cleaner [2:10 am cada día]
    $jobby->add("FrontendUserTokenCleaner", [
        "command"  => "php cli/cli.php main userTokenCleaner",
        "schedule" => "10 2 * * *",
        "output"   => "frontend/app/logs/jobs_".date("d-m-Y").".log",
        "enabled"  => true,
    ]);

    //run jobs
    $jobby->run();
}
catch (Exception $e) {
    echo $e->getMessage();
}
