<?php
/**
 * CLI Main Task: main tasks for CLI commands
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//CrazyCake
use CrazyCake\Core\TaskCore;

class MainTask extends TaskCore
{
    /**
     * Main Action Executer
     */
    public function mainAction()
    {
        parent::mainAction();
    }

    /**
     * User Tokens cleaner (for token checkout)
     */
    public function userTokenCleanerAction()
    {
        //delete tokens default expiration time
        $objs_deleted = UserToken::deleteExpired();

        //rows affected
        if($objs_deleted)
            echo "[".date("d-m-Y H:i:s")."] userTokenCleaner -> Expired Tokens deleted: ".$objs_deleted."\n";
    }

    /**
     * Users Checkouts cleaner (for expired checkout)
     */
    public function userCheckoutCleanerAction()
    {
        //delete pending checkouts with default expiration time
        $objs_deleted = UserCheckout::deleteExpired();

        //rows affected
        if($objs_deleted)
            echo "[".date("d-m-Y H:i:s")."] userCheckoutCleaner -> Pending Expired Checkouts deleted: ".$objs_deleted."\n";
    }
}
