LiveOn WebApp project
=====================

Requisitos de entorno
---------------------

- Apache 2.4.x or greater HTTP Server
- Phalcon PHP 2.x framework. [Get Phalcon](https://phalconphp.com/es/download).
- PHP 5.6.x
- PHP extensions: `curl`, `mcrypt`, `gd`, `gettext` and `mysql`
- MySQL 5.6.x server (5.5 also works)
- Composer (Library dependencies, `packages/composer` folder)
- Redis Server
- wkhtmltopdf (instalación abajo) [ref](http://wkhtmltopdf.org/)


Requisitos local development
----------------------------

- Para testear `cc-phalcon` crear *symLink* en directorio `packages/` apuntando al proyecto.
```ln -s ../../cc-phalcon packages/```

- nodeJs with global **(-g)** npm packages: `gulp`, `browserify`, `uglifyjs`.
```bash _app.bash npm-global```

Tasks
-----

Ejecutar **_app.bash** para ejecutar bash CLI (alias bap)
```
bash _app.bash
```

Jobs (jobby lib)
----------------

Incluir en crontab `crontab -e`
```
* * * * * cd /path/to/liveon-webapp/ && php jobs.php 1>> /dev/null 2>&1
```

Database Migrations (phinx lib)
-------------------------------

Phinx: [documentation](https://phinx.org/)
```
bash _app.bash phinx <commands>
```

Redis Server
-------------

Instalar server y cliente (testing) con
```sudo apt-get install redis-server redis-cli```


wkhtmltopdf (v 0.12.x)
----------------------

####Ubuntu Server
Instalar via shell:
```
bash _app.bash wkhtmltopdf
```

finalmente, testear con:
```
/usr/local/bin/wkhtmltopdf.sh --lowquality http://www.google.com test.pdf
```

####OSX
Instalación en OSX
+ Bajar [binario](http://wkhtmltopdf.org/downloads.html)


Translations (GetText)
----------------------

Para ver los 'locales' instalados en la instancia (getText)
```
locale -a
```

Composer
--------

Usage
```
php composer.phar install
```

Update Libraries
```
php composer.phar update
```

Update One Vendor
```
php composer.phar update foo/bar
```

Add Library
```
php composer.phar require "foo/bar:1.0.0"
```

Optimization loader for production
```
php composer.phar dump-autoload --optimize
```

Facebook
--------

Para test de login de Facebook en localhost, registrar *localhost.liveon.cl* en nuestro archivo `/etc/hosts` y
acceder con dicha URL a nuestro localhost.
```
127.0.0.1	localhost localhost.liveon.cl
```

###OpenGraph

[Graph Explorer](https://developers.facebook.com/tools/explorer)
[Object Browser](https://developers.facebook.com/tools/object-browser)

Ejemplo Graph Explorer
```
./1430373997281412 (object_id)
./730926267018069 (post_id)
```
