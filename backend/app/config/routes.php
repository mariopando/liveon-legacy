<?php
/**
 * Phalcon App Routes files
 * 404 error page is managed by Route 404 Plugin automatically
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

return function($router) {

	//default route
	$router->add("/", [
	    'controller' => 'index',
	    'action'     => 'index'
	]);
};
