{# oldBrowser View #}
<h1>{{ trans._("Oops, ha ocurrido algo") }}</h1>
<h2>{{ trans._("Tu navegador es muy viejo. ¡Actualízalo!") }}</h2>
<h3>{{ trans._("Descarga la versión más reciente de tu navegador favorito.") }}</h3>

{# Special cases for legacy browsers #}
{% if client.isLegacy %}
	<div class="browsers-legacy text-center">
		<a href="http://www.google.com/chrome/">Chrome</a>
		<a href="http://www.mozilla.com/firefox/">Firefox</a>
		<a href="http://www.apple.com/safari/">Safari</a>
		<a href="http://www.opera.com/browser/">Opera</a>
		<a href="http://www.microsoft.com/windows/Internet-explorer/">Internet Explorer</a>
	</div>
{% else %}
	<div class="browsers text-center">
		<div class="row align-center browser-grid">
			<div class="columns medium-4 large-2 cr">
				<a href="http://www.google.com/chrome/" data-tooltip aria-haspopup="true" class="has-tip" title="Chrome">&nbsp;</a>
			</div>
			<div class="columns medium-4 large-2 ff">
				<a href="http://www.mozilla.com/firefox/" data-tooltip aria-haspopup="true" class="has-tip" title="Firefox">&nbsp;</a>
			</div>
			<div class="columns medium-4 large-2 sf">
				<a href="http://www.apple.com/safari/" data-tooltip aria-haspopup="true" class="has-tip" title="Safari">&nbsp;</a>
			</div>
			<div class="column large-2 op show-for-large">
				<a href="http://www.opera.com/browser/" data-tooltip aria-haspopup="true" class="has-tip" title="Opera">&nbsp;</a>
			</div>
			<div class="columns large-2 ie show-for-large">
				<a href="http://www.microsoft.com/windows/Internet-explorer/" data-tooltip aria-haspopup="true" class="has-tip" title="Internet Explorer">&nbsp;</a>
			</div>
			<div class="column large-2 tc show-for-large">
				<a href="http://www.torchbrowser.com/" data-tooltip aria-haspopup="true" class="has-tip" title="Torch Browser">&nbsp;</a>
			</div>
		</div>
	</div>
{% endif %}
