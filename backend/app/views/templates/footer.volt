{# Footer, render a fallback for legacy browsers #}
{% if client.isLegacy %}
	<div class="app-footer-legacy text-center">
		<p>&copy; {{ date('Y') }} <span>{{ app.name }}</span> </p>
	</div>
{% else %}
	<footer id="footer">
		{# default footer #}
		<ul >
			<li><span>&copy; {{ date('Y') }}</span> <a href="{{ url() }}" class="marked">Chile</a></li>
		</ul>
	</footer>
{% endif %}
