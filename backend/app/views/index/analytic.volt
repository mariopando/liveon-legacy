{# Analytic view #}
<div id="vue-analytic" class="app-analytics-wrapper">

	<h4>Eventos</h4>

	<div class="app-form">

		{# event selector #}
		<div class="event-selector" v-cloak>
			<select v-model="eventSelected" v-on:change="eventSelectorChanged">
				<option value="0"> -- </option>
				<option v-for="event in events" v-bind:value="event.id" v-text="event.name"></option>
			</select>
		</div>

		{# tickets brief #}
		<div class="tickets-brief" v-if="ticketsBrief.length > 0" v-cloak>
			<h5>Entradas activas</h5>
			<div class="row collapse head text-left">
				<div class="small-1 columns"> <span>ID</span> </div>
				<div class="small-5 columns"> <span>Nombre</span> </div>
				<div class="small-3 columns"> <span>Web</span> </div>
				<div class="small-3 columns"> <span>Total</span> </div>
			</div>
			{# binds a ticket row #}
			<div v-for="ticket in ticketsBrief">
				{# cell #}
				<div class="row collapse cell">
					<div class="small-1 columns"> <span v-text="ticket.id"></span> </div>
					<div class="small-5 columns"> <span v-text="ticket.name"></span> </div>
					<div class="small-3 columns"> <span v-text="ticket.total_web"></span> </div>
					<div class="small-3 columns"> <span v-text="ticket.total"></span> </div>
				</div>
			</div>
			{# resume #}
			<div class="row collapse cell last">
				<div class="small-1 columns"> &nbsp; </div>
				<div class="small-5 columns"> <span>Total</span> </div>
				<div class="small-3 columns"> <span v-text="overallWebTickets"></span> </div>
				<div class="small-3 columns"> <span v-text="overallTotalTickets"></span> </div>
			</div>
		</div>

		{# assets brief #}
		<div class="tickets-brief margin-top" v-if="assetsBrief.length > 0" v-cloak>
			<h5>Extras activos</h5>
			<div class="row collapse head text-left">
				<div class="small-1 columns"> <span>ID</span> </div>
				<div class="small-8 columns"> <span>Nombre</span> </div>
				<div class="small-3 columns"> <span>Total</span> </div>
			</div>
			{# binds a ticket row #}
			<div v-for="asset in assetsBrief">
				{# cell #}
				<div class="row collapse cell">
					<div class="small-1 columns"> <span v-text="asset.id"></span> </div>
					<div class="small-8 columns"> <span v-text="asset.name"></span> </div>
					<div class="small-3 columns"> <span v-text="asset.total"></span> </div>
				</div>
			</div>
			{# resume #}
			<div class="row collapse cell last">
				<div class="small-1 columns"> &nbsp; </div>
				<div class="small-8 columns"> <span>Total</span> </div>
				<div class="small-3 columns"> <span v-text="overallTotalAssets"></span> </div>
			</div>
		</div>

		{# tickets graph #}
		<div class="tickets-sales" v-cloak>
			<h5>Detalle Mes {{ date('M. Y') }}</h5>

			<div>
				<p> N. Entradas \ día del mes</p>
				<div id="sales-graph"></div>
			</div>
		</div>

	</div>
</div>

{# space #}
<div style="padding-top:3%;">&nbsp;</div>
