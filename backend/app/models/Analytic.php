<?php

//other imports
use CrazyCake\Models\Base;

class Analytic extends Base
{
    /* extended properties */

    /** ------------------------------------------- § --------------------------------------------------
       Event
    ------------------------------------------------------------------------------------------------- **/
    public function afterFetch()
    {
        parent::afterFetch();
    }
    /** ------------------------------------------ § ------------------------------------------------- **/

    /**
     * required late static binding, overrides parent who function
     * @link http://php.net/manual/en/language.oop5.late-static-bindings.php
     * @return string
     */
    public static function who()
    {
        return __CLASS__;
    }

    /**
     * GET tickets brief Analytics
     * @param int $event_id The event_id filter
     */
    public static function getTicketsBrief($event_id = 0)
    {
        return self::getByQuery(
            "SELECT et.id, et.name,
            	(@total_web:=(SELECT COUNT(uet.ticket_id) FROM user_event_ticket uet WHERE et.id = uet.ticket_id)) AS total_web,
            	(@total_web) AS total
            FROM event_ticket et
            WHERE et.event_id = ?
            ",
            //bindings
            [$event_id],
            //className
            "EventTicket"
        );
    }

    /**
     * GET tickets brief Analytics
     * @param int $event_id The event_id filter
     */
    public static function getAssetsBrief($event_id = 0)
    {
        return self::getByQuery(
            "SELECT id, name, SUM(counter) AS total
                FROM (
                	SELECT ea.id, ea.name, COUNT(ea.id) AS counter
                	FROM event_ticket_asset eta
                		INNER JOIN event_ticket et ON et.id = eta.ticket_id
                		INNER JOIN easset ea ON ea.id = eta.easset_id
                		INNER JOIN user_event_ticket_asset ueta ON ueta.event_ticket_asset_id = eta.id
                	WHERE et.event_id = ?
                	GROUP BY ea.id
                	UNION
                	SELECT ea.id, ea.name, 0 AS counter
                	FROM event_ticket_asset eta
                		INNER JOIN event_ticket et ON et.id = eta.ticket_id
                		INNER JOIN easset ea ON ea.id = eta.easset_id
                		LEFT OUTER JOIN user_event_ticket_asset ueta ON ueta.event_ticket_asset_id IS NULL
                	WHERE et.event_id = ?
                	GROUP BY ea.id
                ) AS stats
                GROUP BY id
            ",
            //bindings
            [$event_id, $event_id],
            //className
            "EventTicket"
        );
    }

    /**
     * GET tickets sales Analytics
     * @param int $event_id The event_id filter
     */
    public static function getTicketsSalesInDate($event_id = 0, $sdate = '', $edate = '')
    {
        return self::getByQuery(
           "SELECT date AS time, SUM(num_items) AS Q, MAX(date) AS max_day
            FROM (
            	SELECT uc.buy_order, 'web' AS type, DATE_FORMAT(uc.local_time, '%d') AS date,
            		(
            			SELECT (COUNT(uco.id)*uco.quantity) AS count
            			FROM user_checkout_object uco
            				INNER JOIN event_ticket et ON et.id = uco.object_id
            			WHERE uco.buy_order = uc.buy_order
            				AND uco.object_class = 'EventTicket'
            				AND et.event_id = ?
            		) AS num_items,
            		(
            		  	SELECT SUM(et.price*uco.quantity)
            		  	FROM user_checkout_object uco
            				INNER JOIN event_ticket et ON et.id = object_id
            			WHERE buy_order = uc.buy_order
            				AND object_class = 'EventTicket'
            				AND et.event_id = ?
            		 ) AS amount
            	FROM user_checkout uc
            	WHERE uc.state = 'success'
            		AND uc.local_time BETWEEN '$sdate' AND '$edate'
            	HAVING num_items > 0
            ) AS R
            GROUP BY date
            ORDER BY date ASC
            ",
            //bindings
            [$event_id, $event_id],
            //className
            "UserCheckout"
        );
    }
}
