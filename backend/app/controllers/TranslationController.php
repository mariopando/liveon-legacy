<?php
/**
 * TranslationsController
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 * @TODO: Bring all controllers translations here
 */

class TranslationController extends CoreController
{
    /**
     * Javascript Translations
     * @static
     */
    public static function getJsTranslations()
    {
        //get translate service
        $di    = \Phalcon\DI::getDefault();
        $trans = $di->getShared('trans');

        return [
        	"ALERTS" => [
        		'INTERNAL_ERROR' 	=> $trans->_('Oops, ha ocurrido un problema, solucionaremos esto a la brevedad.'),
        		'SERVER_TIMEOUT'	=> $trans->_('Hemos perdido la comunicación, prueba revisando tu conexión a Internet.'),
        		'CSRF' 				=> $trans->_('Esta página ha estado inactiva por mucho tiempo, haz %a_open%click aquí%a_close% para refrescarla.',
                                                  ["a_open" => '<a href="javascript:location.reload();">', "a_close" => "</a>"]),
        		'NOT_FOUND' 		=> $trans->_('Oops, el enlace está roto. Porfavor inténtalo más tarde.'),
        		'BAD_REQUEST' 		=> $trans->_('Lo sentimos, no hemos logrado procesar tu petición. Intenta refrescando esta página.'),
        		'ACCESS_FORBIDDEN' 	=> $trans->_('Tu sesión ha caducado, porfavor %a_open%ingresa nuevamente aquí%a_close%.',
                                                 ["a_open" => '<a href="javascript:core.redirectTo(\'signIn\');">', "a_close" => "</a>"])
        	],
        	"ACTIONS" => [
        		'OK' 		 => $trans->_('Ok'),
        		'ACCEPT' 	 => $trans->_('Aceptar'),
        		'CANCEL' 	 => $trans->_('Cancelar'),
        		'NOT_NOW' 	 => $trans->_('Ahora No'),
        		'SEND' 		 => $trans->_('Enviar'),
        		'GOT_IT' 	 => $trans->_('Entendido'),
        		'LOADING' 	 => $trans->_('Espera un momento ...'),
                'TRANSFER'   => $trans->_('Transferir'),
                'NULLIFY'    => $trans->_('Anular')
        	]
        ];
    }
}
