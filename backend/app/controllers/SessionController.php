<?php
/**
 * SessionController: parent class for main account based controllers
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

use CrazyCake\Account\AccountSession;

class SessionController extends CoreController
{
    /* traits */
    use AccountSession;

    /**
     * @var array
     */
    private $UI_DATA;

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Set user Session as logged in
     * @param object $user The user object is passed
     */
    protected function setUserSessionAsLoggedIn($user)
    {
        //set session data (id & auth props are set by defult)
        return array(
            "id_hashed"    => $user->id_hashed,
            "email"        => $user->email,
            "first_name"   => $user->first_name,
            "last_name"    => $user->last_name,
            "last_login"   => $user->last_login,
            "account_flag" => $user->account_flag
        );
    }

    /**
     * Get logged in user session data
     * @param array $session The session array
     * @return boolean
     */
    protected function getUserSessionData($session)
    {
        //account UI data
        $ui_paths = [
            "local_profile" => $this->_staticUrl("uploads/account_images/<hash>.png"),
        ];

        //return unmodified session data
        if (isset($session['profile_img_path']))
            return $session;

        //set fallback as default
        $image_path = $ui_paths['fallback_profile'];

        //local or cdn image path (for non-social account users)
        if (is_file($ui_paths['local_profile']))
            $image_path = str_replace("<hash>", $session["id_hashed"], $ui_paths['local_profile']);

        //set image path
        $session['profile_img_path'] = $image_path;

        //return modified session properties
        return $session;
    }
}
