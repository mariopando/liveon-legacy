<?php
/**
 * AnalyticController, handles Analytic pages
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

class AnalyticController extends SessionController
{
    /**
     * View - Index page
     */
    public function indexAction()
    {
        //get events
        $events = Event::find([
            "state = 'open' OR state = 'invisible' ",
            "order" => "name"
        ]);

        //set view data
        $this->view->setVars([
            "events" => $events,
        ]);

        //load js modules
        $this->_loadJsModules([
            "analytic" => $events->toArray()
        ]);

        //pick view
        $this->view->pick("index/analytic");
    }

    /**
     * AJAX (GET) - Get event analytics
     */
    public function eventAction()
    {
        //make sure is ajax request
        $this->_onlyAjax();

        //get post params
        $data = $this->_handleRequestParams([
            'payload' => ''
        ], 'GET');

        //get event
        $event_id = $data["payload"];

        $start_date = date("Y-m-1");
        $end_date   = date("Y-m-t H:i:s"); //date("Y-m-t H:i:s", strtotime("-1 month"))

        $ticketsBrief = Analytic::getTicketsBrief($event_id);
        $ticketSales  = Analytic::getTicketsSalesInDate($event_id, $start_date, $end_date);
        $assetsBrief  = Analytic::getAssetsBrief($event_id);

        $payload = [
            "ticketsBrief" => ($ticketsBrief ? $ticketsBrief->toArray() : []),
            "ticketsSales" => ($ticketSales ? $ticketSales->toArray() : []),
            "assetsBrief"  => ($assetsBrief ? $assetsBrief->toArray() : [])
        ];

        //send response
        $this->_sendJsonResponse(200, $payload);
    }
}
