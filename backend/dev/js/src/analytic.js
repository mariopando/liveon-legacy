/** ---------------------------------------------------------------------------------------------------------------
    Analytics Module
---------------------------------------------------------------------------------------------------------------- **/
module.exports = function() {

    //++ Module
    var self        = this;
    self.moduleName = "analytic";

    //++ View Model
    self.vm = {
        data : {
            events        : {},
            eventSelected : '',
            assetsBrief   : [],
            ticketsBrief  : [],
            ticketsSales  : []
        },
        methods  : {},
        computed : {}
    };

    //++ UI Selectors
    _.assign(APP.UI, {
        //selectors
        sel_analytics_wrapper : "div.app-analytics-wrapper",
        sel_sales_graph_id    : "sales-graph"
    });

    /**
     * Init function
     */
    self.init = function(data) {
        //set events data
        self.vm.data.events = data;
    };

    /**
     * Event Selector Changed
     * @param {object} event - The event handler
     */
    self.vm.methods.eventSelectorChanged = function(e) {

        var $this = $(e.target);

        if(!self.vm.eventSelected) {
            //flush items
            self.vm.ticketsBrief = [];
            self.vm.assetsBrief  = [];
            return;
        }

        //loading...
        $this.prop('disabled', true);

        //request with promise
        core.ajaxRequest({ method : 'GET', url : APP.baseUrl + 'analytic/event' }, null, { "payload" : self.vm.eventSelected } )
        .then(function(payload) {

            //enable select
            $this.prop('disabled', false);

            if(!payload) return;

            //update content
            self.renderizeAnalytics(payload);

        }).done();
    };

    /**
     * Renderize contents
     * @param object payload The server payload
     */
    self.renderizeAnalytics = function(payload) {

        //flush objects
        self.vm.ticketsBrief = [];
        self.vm.assetsBrief  = [];

        for (var i = 0; i < payload.ticketsBrief.length; i++)
            self.vm.ticketsBrief.push(payload.ticketsBrief[i]);

        for (i = 0; i < payload.assetsBrief.length; i++)
            self.vm.assetsBrief.push(payload.assetsBrief[i]);

        //Graph Renderize
        if(typeof payload.ticketsSales == "undefined")
            return;

        //update array binding
        for (i = 0; i < payload.ticketsSales.length; i++)
            self.vm.ticketsSales.push(payload.ticketsSales[i]);

        //render graph
        core.modules.graph.plotStackedBarChar(payload.ticketsSales, APP.UI.sel_sales_graph_id);
    };

    /**
     * Get Overall
     * @param  {string} prop - 'total_physical', 'total_web' or 'total' opt
     * @param  {string} type - 'tickets' or 'assets' opt
     * @return {int} The sum value
     */
    self.calculateOverall = function(prop, type) {

        var sum = 0;

        var items = (type == "ticket") ? self.vm.ticketsBrief : self.vm.assetsBrief;

        for (var i = 0; i < items.length; i++)
            sum += parseInt(items[i][prop]);

        //console.log(items[0][prop], sum);

        return sum;
    };

    //++ Computed
    self.vm.computed.overallWebTickets      = function () { return self.calculateOverall('total_web', 'ticket'); };
    self.vm.computed.overallTotalTickets    = function () { return self.calculateOverall('total', 'ticket'); };
    self.vm.computed.overallTotalAssets     = function () { return self.calculateOverall('total', 'asset'); };

};
