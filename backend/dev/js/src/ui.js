/** ---------------------------------------------------------------------------------------------------------------
    UI Module
---------------------------------------------------------------------------------------------------------------- **/
module.exports = function() {

    //++ Module
    var self        = this;
    self.moduleName = "ui";

    //Extend App data
    _.assign(APP.UI, {
        //settings
        alert : {
            position    : "fixed",
            top         : "belowHeader",
            topForSmall : "0"
        },
        loading : {
            position    : "fixed",
            top         : "14%",
            topForSmall : "20%",
            center      : true
        }
    });

    /**
     * Init function
     */
    self.init = function() {


    };
};
