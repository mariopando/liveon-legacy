/** ---------------------------------------------------------------------------------------------------------------
    Graphs Module
---------------------------------------------------------------------------------------------------------------- **/
 module.exports = function() {

     //++ Module
     var self        = this;
     self.moduleName = "graph";

     //set global options
     Chart.defaults.global.responsive = true;

     /**
      * Renders a stacked Bar Char. Requires D3
      * @param  {object} wrapper A jquery object element
      */
     self.plotStackedBarChar = function(data, graph_id) {

         var xData = [];
         var yData = [];

         data.forEach(function(d) {
           xData.push(d.time);
           yData.push(d.Q);
         });

         //get max value
         var max_value = 0;
         for (var i = 0; i < xData.length; i++) {

             if(max_value < xData[i])
                max_value = xData[i];
         }

         var daysLabel = [];
         for (i = 0; i < max_value; i++) {
             daysLabel[i] = i+1;
         }

         data = {
            labels: daysLabel,
            datasets: [
                {
                    label: "Días del Mes",
                    fillColor: "rgba(220,220,220,0.2)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: xData
                },
                {
                    label: "N. Tickets",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: yData
                }
            ]
        };

        var options = {};
        //clean HTML contents
        var canvas = $("<canvas>").attr("id", graph_id + "-canvas");

        //append canvas
        $('#' + graph_id).html('').append(canvas);

        var context = document.getElementById(graph_id + "-canvas").getContext("2d");
        var chart   = new Chart(context).Line(data, options);
     };
 };
