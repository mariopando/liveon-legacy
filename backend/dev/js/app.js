/**
 * App.js
 * [Browserify can only analyze static requires.
 * It is not in the scope of browserify to handle dynamic requires.]
 */

//load bundle dependencies
require('webpack_core');
//framework
require('foundation');

//aditional libs
require('d3');
require('Chart');

/* Load modules */

var modules = [
    new (require('./src/ui.js'))(),
    new (require('./src/analytic.js'))(),
    new (require('./src/graph.js'))()
];

//set modules
core.setModules(modules);
