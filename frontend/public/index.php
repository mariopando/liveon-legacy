<?php
/**
 * Index Phalcon File. PHP Settings must be set in php.ini (both files: Apache & CLI)
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//include App Loader
include dirname(dirname(__DIR__))."/app.php";

try {
    $app = new PhalconApp("frontend");
    $app->start();
}
catch (Exception $e) {
    echo $e->getMessage();
}
