<?php
/**
 * Página de cierre de Webpay.
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//include App Loader
include dirname(dirname(__DIR__))."/app.php";

try {
     $app = new PhalconApp("frontend");
     $app->start();

     //handle kcc response
     (new \CrazyCake\Transbank\KccEndPoint())->handleResponse();
}
catch (Exception $e) {

     //save exception
     $log = new \Phalcon\Logger\Adapter\File(APP_PATH."logs/webpaykcc_".date("d-m-Y").".log");
     $log->error($e->getMessage());
     //send output
     die("<html>RECHAZADO</html>");
}
