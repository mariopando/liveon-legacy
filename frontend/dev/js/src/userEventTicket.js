/**
 * UserTickets View Model
 * @class UserTickets
 */
module.exports = function() {

    //++ Module
    var self        = this;
    self.moduleName = "userEventTicket";
    //Vue.config.debug = true;

    //++ View Model
    self.vm = {
        data : {
            ticketCategories : []
        },
        methods  : {},
        computed : {}
    };

    //++ Properties
    self.eventEnabled      = true;
    self.user              = {};
    self.unassignedTickets = 0;

    //++ Methods

    /**
     * Init
     * @param  {object} data Tickets
     */
    self.init = function(data) {

        var enabled = true;
        //console.log(data);

        //check event state exceptions
        if((data.event.state != "open" && data.event.state != "invisible") || data.event.type == "invitation") {

            enabled = false;
        }

        //set props
        self.user              = data.user;
        self.unassignedTickets = data.unassignedTickets;
        self.eventEnabled      = enabled;

        //set tickets categories
        var count         = data.tickets.length;
        var last_category = null;
        var categories    = [];
        var tag           = 1;

        for (var i = 0; i < count; i++) {

            var ticket = data.tickets[i];

            //new category
            if(last_category != ticket.name) {
                //set tag
                tag = 1;
                ticket.index = i;
                ticket.tag   = tag;
                //push new category
                categories.push({
                    name    : ticket.name,
                    tickets : [ticket]
                });
                //mark new category
                last_category = ticket.name;
            }
            else {
                //push category ticket
                ticket.index = i;
                ticket.tag   = tag;
                _.last(categories).tickets.push(ticket);
            }

            tag++;
        }

        //set vm property
        self.vm.data.ticketCategories = categories;
    };

    /**
     * Get PDF Invoice in a new window
     * @param  {string} url - The url
     * @param  {boolean} print - Flag for print cmd
     */
    self.vm.methods.getInvoice = function(url, print) {

        var windowSize = Foundation.MediaQuery.atLeast('medium') ? 640 : 320;

        var w = window.open(url, '_blank', 'menubar=no, width='+windowSize+', height='+windowSize+', left=100, top=100', true);

        return (print ? w.print() : true);
    };

    /**
     * New transfer Order
     * @param  {object} e - The event handler
     * @param  {int} vmIndex - The component tree index
     * @return void
     */
    self.vm.methods.newTransferOrder = function(e, vmIndex) {

        //define anonymous fn
        var sendRequest = function(formElement, vm) {
            //close modal
            $.modalityDialog.close();
            //request with promise
            core.ajaxRequest({ method : 'POST', url : APP.baseUrl + 'event_ticket_transfer/newOrder' }, formElement)
            .then(function(payload) {

                if(!payload) return;

                //set transfer object
                vm.ownerPending = APP.TRANS.TRANSFERS.PENDING.replace("{name}", payload.receiverName).replace("{email}", payload.receiverEmail);
                vm.obj.transfer = payload.transfer;
                //hide form & set another action
                vm.displayForm = false;
                self.setTransferFormAction(vm, "cancel");
                //show alert
                core.showAlert(payload.msg, "success");

            }).done();
        };

        //if form is not valid skips
        if(!core.modules.forms.isFormValid(e.target))
            return;

        //get object
        var vm = self.vm.$children[vmIndex];

        //show confirm dialog
        $.modalityDialog({
            title   : APP.TRANS.TRANSFERS.ACTION_TITLE,
            content : APP.TRANS.TRANSFERS.ACTION_MSG,
            buttons :[
                { label : APP.TRANS.ACTIONS.ACCEPT, click : function() {
                    sendRequest(e.target, vm);
                }},
                { label : APP.TRANS.ACTIONS.CANCEL, click : function() {
                    core.modules.forms.enableFormSubmitButtons(e.target, true);
                    $.modalityDialog.close();
                }}
            ]
        });
    };

    /**
     * Undo a sent transfer
     * @return void
     */
    self.cancelTransferOrder = function(vm) {

        //set request function
        var sendRequest = function(vm) {
            //close modal
            $.modalityDialog.close();
            //set data
            var data = { "transfer_id" : vm.obj.transfer.id };
            //console.log(data);return;

            //request with promise
        	core.ajaxRequest({ method : 'POST', url : APP.baseUrl + 'event_ticket_transfer/cancelTransferOrder' }, null, data)
    		.then(function(payload) {

                if(!payload) return;

                //switch form action
                self.setTransferFormAction(vm, "request");
                //show success
                core.showAlert(payload, "success");

            }).done();
        };

        //show confirm dialog
        $.modalityDialog({
            title: APP.TRANS.TRANSFERS.CANCEL_TITLE,
            content: APP.TRANS.TRANSFERS.CANCEL_MSG,
            buttons:[
                { label: APP.TRANS.ACTIONS.ACCEPT, click: function() { sendRequest(vm); }},
                { label: APP.TRANS.ACTIONS.CANCEL}
            ]
        });
    };

    /**
     * switch transfer form action
     * @param  {object} vm - The vm object
     * @param  {string} action - The action: request or cancel
     */
    self.setTransferFormAction = function(vm, action) {

        //update form action
        vm.formAction   = action;

        if(action == "request") {
            //set owner text
            vm.owner = self.user.first_name+" "+self.user.last_name;
            //set button title
            vm.buttonToggleText = APP.TRANS.ACTIONS.TRANSFER;
            //remove css class
            $("#"+vm.rowId).children(".ticket-row").removeClass("active");
        }
        //cancel
        else {
            var text = APP.TRANS.TRANSFERS.PENDING
                            .replace("{name}", vm.obj.transfer.receiver_name)
                            .replace("{email}", vm.obj.transfer.receiver_email);

            //set owner HTML text
            vm.owner = (typeof vm.ownerPending !== "undefined") ? vm.ownerPending : text;
            //change button props
            vm.buttonToggleText = APP.TRANS.ACTIONS.NULLIFY;
            //add css styles
            $("#"+vm.rowId).children(".ticket-row").addClass("active");
        }
    };

    /**
     * Close transfer forms
     * @param  {int} rowExcluded - Shows only the row excluded
     * @return {void}
     */
    self.hideTransferForms = function(rowExcluded) {

        //loop
        _(self.vm.$children).forEach(function(obj) {

            obj.displayForm = (obj.rowId == rowExcluded) ? true : false;
        });
    };

    //++ Component
    self.component = Vue.component('ticket', {
        template : '#vue-template-user-event-ticket',
        //params
        props : {
            obj              : {},
            rowId            : '',
            modalId          : '',
            owner            : '',
            buttonToggleText : '',
            formAction       : '',
            displayForm      : false,
            seeMore          : false
        },
        //fns
        methods: {
            getTicket : function() {

                //reveal modal
                if(core.framework == "foundation")
                    $("#"+this.modalId).foundation('open');
            },
            goToExtras : function () {

                location.href = APP.baseUrl + "checkout/assets/" + this.obj.code;
            },
            toggleForm : function() {

                if(this.formAction == "cancel")
                    return self.cancelTransferOrder(this);

                //display form and hide others
                this.displayForm = !this.displayForm;
                self.hideTransferForms(this.rowId);
            }
        },
        computed : {
            transferStatus : function() {

                var num = this.obj.tag;

                if(this.obj.tag < 10)
                    num = "0"+this.obj.tag;

                //create struct
                return '<table>'+
                            '<tr>' +
                                '<td><strong>'+num+'</strong></td>'+
                                '<td><span>'+this.owner+'</span></td>'+
                            '</tr>'+
                        '</table>';
            },
            extrasButtonDisabled : function() {

                if(!self.eventEnabled ||
                    this.obj._ext.state == 'closed' ||
                    this.obj._ext.assets_owned_count >= this.obj._ext.assets_count) {
                    return true;
                }

                return false;
            },
            transferButtonDisabled : function() {

                if(!self.eventEnabled || this.obj._ext.state == 'closed')
                    return true;

                return false;
            }
        },
        //events
        ready : function () {

            //set default props
            this.displayForm = false;
            this.seeMore     = false;

            //transfer owner
            if(typeof this.obj.transfer != "undefined")
                self.setTransferFormAction(this, "cancel");
            //nomal case
            else
                self.setTransferFormAction(this, "request");
        }
    });
};
