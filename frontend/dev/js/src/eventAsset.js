/**
 * Event Assets View Model
 */
module.exports = function() {

    //++ Module
    var self        = this;
    self.moduleName = "eventAsset";

    //parent controller
    self.moduleController = null;

    //++ Components

    self.component = Vue.component('eassets', {
        template : '#vue-template-event-asset',
        //params
        props : {
            asset : {}
        }
    });

    /**
     * Init
     */
    self.init = function(data) {

        if(typeof data.eassets == "undefined")
            return;

        //set module controller. TODO: separar esta lógica
        self.moduleController = core.modules.eventInvitation.vm;

        //assign extended model props to object
        _(data.eassets).forEach(function(obj) {
            //append selected quantity property
            obj.nameAttr  = "checkout_EventAsset_" + obj._ext.id_hashed;
            obj.selectedQ = 0;
            obj.seeMore   = false;
        });

        //set binding in parents vm (TODO: better handle this with Vuex)
        self.moduleController.data.eassets = data.eassets;
    };
};
