/**
 * Checkout View Model
 * @class Checkout
 */
module.exports = function() {

    //++ Module
    var self        = this;
    self.moduleName = "checkout";

    //++ View Model
    self.vm = {
        data : {
            gateway          : 'webpaykcc',
            formAction       : '',
            checkoutDisabled : true,
            objects          : [],
            amountOptions    : []
        },
        computed : {},
        methods  : {}
    };

    //++ Properties (default values)
    self.checkoutType   = "paid";
    self.amount         = 0;
    self.totalQ         = 0;

    //++ Methods

    /**
     * Init
     * @param  {object} data Tickets and checkout max number
     */
    self.init = function(data) {

        self.checkoutType = data.checkoutType;

        self.setAmountOptions(data.checkoutMax);
        self.setCheckoutObjects(data.collections);
    };

    /**
     * Set observable array
     * @param  {array} objects
     */
    self.setCheckoutObjects = function(collections) {

        var objects = [];

        //assign extended model props to object
        _(collections).forEach(function(collection, class_name) {

            _(collection).forEach(function(obj) {

                //set props. TODO: send a entirely object
                obj.nameAttr  = "checkout_" + class_name + '_' + obj._ext.id_hashed;
                obj.selectedQ = 0;
                obj.seeMore   = false;
                //
                objects.push(obj);
            });
        });

        //observable array
        self.vm.data.objects = objects;
    };

    /**
     * Set Amount selector
     * @param  {int} checkout max number
     */
    self.setAmountOptions = function(max) {

        //validate selected option
        var array = [];
        for (var i = 0; i <= max; i++)
            array[i] = i;

        //set binding prop
        self.vm.data.amountOptions = array;
    };

    /**
     * Amount Formatted computed function
     */
    self.vm.computed.amountFormatted = function() {

        var totalAmount = 0;
        var selectedQ   = 0;
        var totalQ      = 0;
        var obj         = {};
        var count       = this.objects.length;

        for (var i = 0; i < count; i++) {

            obj = this.objects[i];
            //set amount
            selectedQ = parseInt(obj.selectedQ);
            //sums
            totalAmount += parseInt(obj.price) * selectedQ;
            totalQ      += selectedQ;
        }

        //update properties
        self.amount = totalAmount;
        self.totalQ = totalQ;

        //enables checkout submit button
        self.vm.checkoutDisabled = self.totalQ === 0 ? true : false;

        /** format price, strip points
         * @todo move this function to a helper form module
         */
        totalAmount = totalAmount.toString().replace(/./g, function(c, i, a) {
            return i && c !== "." && ((a.length - i) % 3 === 0) ? '.' + c : c;
        });

        return totalAmount;
    };

    /**
     * Sends a checkout to generate a buy order
     * @param  {object} e - The event handler
     */
    self.vm.methods.sendCheckout = function(e) {

        //check amount for paid checkouts
        if(self.checkoutType == "paid" && self.amount <= 0)
            return core.showAlert(APP.TRANS.CHECKOUT.EMPTY_ORDER, "warning");

        //check totalQ for non-paid checkouts
        if((self.checkoutType == "free" || self.checkoutType == "invitation") && self.totalQ <= 0)
            return core.showAlert(APP.TRANS.CHECKOUT.EMPTY_ORDER, "warning");

        //request with promise
        core.ajaxRequest({ method : 'POST', url : APP.baseUrl + 'checkout/buyOrder' }, e.target)
        .then(function(payload) {

            if(!payload) return;

            //handle callback (reflection)
            var fn = self.vm[self.vm.gateway + "CheckoutHandler"];
            fn(e.target, payload);

        }).done();
    };

    /**
     * Handles a chargeless checkout (checkout with non-paid items)
     * TODO: not implemented yet.
     * @param  {object} formElement - The form element
     * @param  {object} response The - server response response
     */
    self.freeCheckoutHandler = function(formElement, response) {
        //request with promise
    	core.ajaxRequest({ method : 'POST', url : APP.baseUrl + 'checkout/free' }, null, response)
		.done();
    };
};
