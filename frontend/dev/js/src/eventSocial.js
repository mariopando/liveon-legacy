/**
 * Socials View Model
 * @class Socials
 */
module.exports = function() {

    //++ Module
    var self        = this;
    self.moduleName = "eventSocial";

    //++ View Model
    self.vm = {
        data : {
            facebookSwitchText     : APP.TRANS.FB.LOADING, //text-box msg
            facebookSwitchState    : 2,                    //loading as default
            facebookSwitchDisabled : true
        },
        methods : {}
    };

    //++ UI Selectors
    _.assign(APP.UI, {
        sel_social_fb_switch  : "#app-facebook-switch",
        class_modal_list_item : "modal-list-item"
    });

    //++ Properties

    //FB publish actions
    self.fbObject = {};
    //FB linked objects
    self.socialLinkedTickets = [];

    //++ Methods

    /**
     * Init
     * @param  {array} data The user data
     */
    self.init = function(data) {

        if(typeof data.socialLinkedTickets != "undefined")
            self.socialLinkedTickets = data.socialLinkedTickets;

        if(typeof data.fbObject != "undefined")
            self.fbObject = data.fbObject;

        core.modules.facebook.config.login_fn        = self.loginUserByFacebook;
        core.modules.facebook.config.login_failed_fn = self.loginUserByFacebookFailed;

        //call init functions
        if(typeof data.user != "undefined")
            self.checkFacebookLinkAppState();
    };

    /**
     * Listener - Logins user through FB sdk
     * @param {object} fb_payload - The facebook received payload
     */
    self.loginUserByFacebook = function(fb_payload) {

        //set text to loading
        self.vm.facebookSwitchText  = APP.TRANS.FB.LOADING;
        self.vm.facebookSwitchState = 2;
        //one request at time
        self.vm.facebookSwitchDisabled = true;
        //set payload options
        fb_payload.user_data   = 1;
        fb_payload.check_perms = core.modules.facebook.config.perms; //scope perms

        //ajax request
        core.ajaxRequest({ method : 'POST', url :  APP.baseUrl + 'facebook/login' }, null, fb_payload)
        .then(function(payload) {

            //enable back again button
            self.vm.facebookSwitchDisabled = false;

            //handle ajax response
            if(!payload || _.isNull(payload.perms)) {
                self.updateFacebookSwitchState(false);
                return;
            }

            self.updateFacebookSwitchState(true);

        }).done();
    };

    /**
     * Login by facebook failed event callback
     * @param  {object} error - The facebook OG exception
     * @param  {function} fn_pending - The pending function
     */
    self.loginUserByFacebookFailed = function(error, fn_pending) {

        self.updateFacebookSwitchState(false);
    };

    /**
     * Checks facebook account link state, validating FB scope.
     */
    self.checkFacebookLinkAppState = function() {

        //one request at time
        self.vm.facebookSwitchDisabled = true;

        //request with promise
        core.ajaxRequest({ method : 'GET', url : APP.baseUrl + 'facebook/checkLinkAppState' })
        .then(function(payload) {

            //enable back again button
            self.vm.facebookSwitchDisabled = false;

            if(!payload) return;

            //check response
            self.updateFacebookSwitchState(payload.status);

        }).done();
    };

    /**
     * Unlinks facebook user
     */
    self.unlinkFacebookUser = function() {

        var sendRequest = function() {

            //one request at time
            self.vm.facebookSwitchDisabled = true;
            //close Modality
            $.modalityDialog.close();

            //ajax request
            core.ajaxRequest({ method : 'POST', url : APP.baseUrl + 'facebook/unlinkUser'}, null)
            .then(function(payload) {

                //enable button back again
                self.vm.facebookSwitchDisabled = false;

                if(!payload) return;

                //set switch state
                self.updateFacebookSwitchState(false);

            }).done();
        };

        if(!self.socialLinkedTickets.length)
            return sendRequest();

        var socialLinkedTicketsHtml = "";

        //create HTML markup
        for (var i = 0; i < self.socialLinkedTickets.length; i++) {

            var obj = self.socialLinkedTickets[i];
            //console.log(obj);

            //content
            socialLinkedTicketsHtml += '<div class="'+APP.UI.class_modal_list_item+'">'+obj.name+'<span>'+obj.code+'</span></div>';
        }

        //show confirm dialog
        $.modalityDialog({
           title   : APP.TRANS.FB.UNLINK_TITLE,
           content : APP.TRANS.FB.UNLINK_MSG.replace("{n}", self.socialLinkedTickets.length) + socialLinkedTicketsHtml,
           buttons : [
               { label: APP.TRANS.ACTIONS.UNLINK, click: function() { sendRequest(); }},
               { label: APP.TRANS.ACTIONS.CANCEL }
           ]
       });
    };

    /**
     * Facebook switch event
     * @param  {object} e - The event Handler
     */
    self.vm.methods.facebookSwitchClicked = function(e) {

        //activated value, this method prevents conflicts time in firefox
        var activated = $(APP.UI.sel_social_fb_switch).prop("checked");
        //call facebook login
        var fb_button = $(e.currentTarget).siblings("button.app-btn-fb");

        if(activated == 1) {

            if(APP.dev) { console.log("Socials -> Disabling Facebook User Link"); }

            self.unlinkFacebookUser();
        }
        else {

            if(APP.dev) { console.log("Socials -> Enabling Facebook User Link"); }

            if(core.modules.facebook.config.disable_js_sdk)
                return fb_button.trigger("click");

            if(!core.modules.facebook.config.has_loaded)
                return core.showAlert(APP.TRANS.FB.LOADING_FALLBACK);

            return fb_button.trigger("click");
        }
    };

    /**
     * Updates facebook switch state
     * @param  {int} active - Active flag parameter
     */
    self.updateFacebookSwitchState = function(active) {

        if(!$(APP.UI.sel_social_fb_switch).length)
            return;

        if(active) {
            $(APP.UI.sel_social_fb_switch).prop("checked", true);
            self.vm.facebookSwitchState = 1;
            self.vm.facebookSwitchText  = APP.TRANS.FB.LINKED;
        }
        else {
            $(APP.UI.sel_social_fb_switch).prop("checked", false);
            self.vm.facebookSwitchState = 0;
            self.vm.facebookSwitchText  = APP.TRANS.FB.UNLINKED;
        }
    };

    /**
     * Publish a facebook feed story
     * @param  {object} event - The Event Handler
     */
    /*self.publishStory = function(e) {

        var button = $(e.currentTarget);
        button.attr('disabled','disabled');

        var params = {
            fbObject : self.fbObject,
            action   : "story"
        };

        core.ajaxRequest({ method : 'POST', url : APP.baseUrl + 'facebook/publish' }, null, params)
        .then(function(response) {

            //remove disabled
            button.removeAttr('disabled');

            //handle ajax response
            if(!core.handleAjaxResponse(response)) {
                core.showAlert(APP.TRANS.FB.PUBLISH_STORY_FAILED, 'alert');
                return;
            }

            //success
            core.showAlert(APP.TRANS.FB.PUBLISH_STORY_SUCCESS, 'success');

        }).done();
    };*/
};
