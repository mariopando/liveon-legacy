/**
 * Contents View Model
 * @class Contents
 */
module.exports = function() {

    //++ Module
    var self        = this;
    self.moduleName = "content";

    //++ View Model
    self.vm = {
        methods : {}
    };

    //++ Methods

	/**
	 * Send contact
	 * @param  {Object} event - The event handler
	 */
    self.vm.methods.sendContact = function(e) {
        //request with promise
    	core.ajaxRequest({ method : 'POST', url : APP.baseUrl + 'mailer/sendContact' }, e.target)
		.then(function(payload) {

            if(!payload) return;

			//success!
            core.showAlert(APP.TRANS.MESSAGES.CONTACT_SENT, "success");
            //reset form
            core.modules.forms.resetForm(e.target);

        }).done();
    };
};
