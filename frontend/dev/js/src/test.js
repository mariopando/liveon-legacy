/**
 * Test View Model
 * @class Test
 */
module.exports = function() {

    //++ Module
    var self        = this;
    self.moduleName = "test";
    self.bindModel  = true;

    //++ Methods

    /**
     * Init function
     */
    self.init = function() {

        //load test
        self.uiTest();
    };

    /**
     *  UI simple test
     */
    self.uiTest = function() {

        console.log("App testing! Hello world!");
    };
};
