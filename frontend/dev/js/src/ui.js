/**
 * UI View Model
 * @class UI
 */
module.exports = function() {

    //++ Module
    var self        = this;
    self.moduleName = "ui";
    self.player = null;

    //Extend App data
    _.assign(APP.UI, {
        //settings
        alert : {
            position    : "fixed",
            top         : "belowHeader",
            topForSmall : "0"
        },
        loading : {
            position    : "fixed",
            top         : "14%",
            topForSmall : "20%",
            center      : true
        },
        //selectors
        sel_first_content    : "#app-first-content",
        sel_middle_content   : ".app-middle-content",
        sel_header_profile   : "#app-header-profile",
        sel_header_menu      : "#app-header-menu"
    });

    /**
     * Init function
     */
    self.init = function() {
        //app load UI
        self.uiSetUp();
        //app load session menu
        self.loadSessionMenu();
    };

    /**
     *  App UI Setup. Elements can have a data-fit attr to fix offset precision
     */
    self.uiSetUp = function() {

        //++ selectors
        var header         = $(APP.UI.sel_header);
        var footer         = $(APP.UI.sel_header);
        var middle_content = $(APP.UI.sel_middle_content).eq(0); //make sure is only one
        var first_content  = $(APP.UI.sel_first_content);

        //++ Align middle content??
        if(middle_content.length) {

            //set consider footer var
            var consider_footer = false;

            if(middle_content.attr("data-footer") == 1)
                consider_footer = true;

            //set padding object
            var padding = $(window).height()*0.5 - header.height()/2 - middle_content.height()/2;

            //consider footer?
            if(consider_footer)
                padding -= footer.height()/2;

            middle_content.css("padding-top", padding + "px");

            if(first_content.siblings().length > 0)
                middle_content.css("padding-bottom", padding + "px");

            //fadeIn
            middle_content.velocity({ opacity: 1.0 }, 300);
        }

        //++ Top Bar (header) layout behavior on event scroll
        //skip scroll animation for mobile
        if(!first_content.length || UA.isMobile)
            return;

        //++ scroll event function
        var onScroll = function(event) {
            
            var threshold = -1*(header.position().top + header.outerHeight());
            var fit       = (typeof first_content.attr("data-fit") !== "undefined") ? parseInt(first_content.attr("data-fit")) : 0;
            var scroll    = $(window).scrollTop();
            var offset    = scroll - (first_content.offset().top + first_content.padding('top') + fit);
            //console.log("App UI -> ui_set_up, onScroll: ", threshold, fit, scroll, offset);

            //apply style to header
            if(offset > threshold)
                header.addClass("dark");
            else
                header.removeClass("dark");
        };
        //set scroll event
        $(window).scroll(onScroll);
    };

    /**
     * Loads session side bar menu
     */
    self.loadSessionMenu = function() {

        //set dom elements
        var profile   = $(APP.UI.sel_header_profile);
        var menu      = $(APP.UI.sel_header_menu);
        var x_value   = UA.isMobile ? 0 : -1*menu.width();
        var anim_time = 800; //ms

        if(!profile.length)
            return;

        //open menu with modality
        profile.click(function() {
            menu.modality({
                fixed : true,
                top : 0,
                right : 0,
                overlayAlpha : 60,
                onShowAnim : function() {
                    //exclude mobile
                    menu.css("right", x_value);
                    //show menu
                    menu.show();

                    if(UA.isMobile)
                        return;

                    menu.velocity({
                        translateX: x_value
                    },
                    anim_time,
                    "easeOutExpo");
                },
                onCloseAnim : function() {

                    if(UA.isMobile)
                        return  menu.hide();

                    //reverse animation
                    menu.velocity("reverse", function() {
                       menu.hide();
                    }, anim_time);
                }
            });
        });
        //append close event to button
        $(".close", menu).click(function(){
            $.modality.close();
        });
    };
};
