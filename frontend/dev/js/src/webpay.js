/**
 * Webpay View Model
 * @class Checkout
 */
module.exports = function() {

    //++ Module
    var self        = this;
    self.moduleName = "webpay";

    //++ Methods

    /**
     * Init
     */
    self.init = function() {

        if(typeof core.modules.checkout == "undefined")
            throw new Error("Checkout module not loaded!");

        //append bindings to checkout
        var vm = core.modules.checkout.vm;

        vm.data.tbk_monto        = '';
        vm.data.tbk_orden_compra = '';

        //set a method in view model
        vm.methods.webpaykccCheckoutHandler = self.webpaykccCheckoutHandler;
    };

    /**
     * Handles WebPay KCC checkout response
     * Requires a Main ViewModel with a formAction method.
     * @param  {object} formElement - The form element
     * @param  {object} response - The server response
     */
    self.webpaykccCheckoutHandler = function(formElement, response) {

        var vm = core.modules.checkout.vm;
        //set session_id, buy order & amount
        vm.$set("tbk_orden_compra", response.buy_order);
        vm.$set("tbk_monto", response.amountFormatted);
        //set form action
        vm.$set("formAction", $('input[name="TBK_URL_PAGO"]').val());
        //get form element
        var form = $(formElement);
        //remove event handler 'submit'
        form.off("submit");
        //submit form with delay
        setTimeout(function(){ return form.submit(); }, 100);
    };
};
