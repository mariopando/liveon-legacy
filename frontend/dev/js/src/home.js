/**
 * Home View Model
 * @class Home
 */
module.exports = function() {

    //++ Module
    var self        = this;
    self.moduleName = "home";

	//Extend App data for selectors
	_.assign(APP.UI, {
		sel_sections  	  : "section",
	    sel_index_context : "div.app-index-wrapper",
        sel_events_cards  : "div.events-cards-wrapper",
		sel_subtitles	  : "h2.subtitle",
		path_bg_image	  : "images/bgs/bg-event-main-$.jpg?v=" + APP.version
	});

	/**
     * Init function
     */
    self.init = function() {
		//page pagination
		$('div.page',context).height($('body').height());

		var context  = $(APP.UI.sel_index_context);

		//Background handle
		if( ! self.playVideoBackground(context) ){
			self.playImageBackground(context);
		}

		//Menu hover
		self.activeMenu(context);
	};

	/**
	 * Handle menu hover actions
	 * @param context
     */
	self.activeMenu = function(context){
		var columns_target_ref = $('div.menu .row .columns a', context); //a tags

        //hamburguer icon link
        $('a#menu_shortcut', context).click(function(){
            $('div.page.menu').velocity("scroll",{
                offset: $('html'),
                duration: 800,
                delay: 500
            });
        });

        //hover animation for a tag, each column
        columns_target_ref.each(function(){
            var ref = $(this);
            var column_parent = ref.parents('div.columns'); // parent div.column

            var columns_number = columns_target_ref.size();
			var current_index = column_parent.index();

            //hover icon animation
            ref.on('click',function() {
                columns_target_ref.parents('div.columns').not(column_parent).addClass('inactive').removeClass('active');
                column_parent.addClass('active fullcontent');
            });

            // Calculate position columns
            if( current_index < (columns_number / 2) ){
                var styles = {
                    'position': 'absolute',
                    'z-index':  4,
                    'left': column_parent.width() * current_index
                };

                //default
                ref.click(function(){
                    column_parent.css(styles);
                    column_parent.velocity({
                            left : 0 },
                        2000,
                        "easeOutExpo");
                });

                //close buttons
                $('span.content-close', ref.parent() ).click(function(){
                    var styles = {
                        'position': 'static',
                        'z-index':  2
                    };
                    column_parent.velocity({
                            left : column_parent.width() * current_index
                        },{
                            duration: 800,
                            easing: 'easeOutExpo',
                            begin: function(){
                                column_parent.next().css('margin-left',column_parent.width());
                                columns_target_ref.parents('div.columns').removeClass('fullcontent active inactive');
                            },
                            complete: function(){
                                columns_target_ref.parents('div.columns').css(styles);
                                column_parent.next().css('margin-left', 0);
                            }
                        }
                    );
                });

                $('.menu-link', column_parent).addClass('left');

            }else{
                var _const = (column_parent.width() * Math.abs(current_index - (--columns_number)));
                var styles = {
                    'position': 'absolute',
                    'z-index':  4,
                    'right': _const
                };

                //default
                ref.click(function(){
                    column_parent.css(styles);
                    column_parent.velocity({
                            right : 0 },
                        2000,
                        "easeOutExpo");
                });

                //close buttons
                $('span.content-close',ref.parent()).click(function(){
                    var styles = {
                        'position': 'static',
                        'z-index':  2
                    };
                    column_parent.velocity({
                            right : _const
                        },{
                            duration: 800,
                            easing: 'easeOutExpo',
                            begin: function(){
                                column_parent.prev().css('margin-right',column_parent.width());
                                columns_target_ref.parents('div.columns').removeClass('fullcontent active inactive');
                            },
                            complete: function(){
                                columns_target_ref.parents('div.columns').css(styles);
                                column_parent.prev().css('margin-right', 0);
                            }
                        }
                    );
                });

                $('.menu-link', column_parent).addClass('right');
            }

        });
	};

	/**
	 * Image Background
	 */
	self.playImageBackground = function(context){
		//++ selectors
		var sections = $(APP.UI.sel_sections, context);

		//fadeIn sections
		sections.velocity({ opacity: 1.0 }, 300);
		$(APP.UI.sel_subtitles, context).velocity( { opacity: 1.0 }, 300);
		$(APP.UI.sel_events_cards, context).velocity( { opacity: 1.0 }, 300);

		//skip background animation for mobile
		if(UA.isMobile)
			return;

		//++ background animation
		var imagesPath = APP.staticUrl + APP.UI.path_bg_image;
		var images 	   = core.preloadImages(imagesPath, 3);
		var index	   = 1;
		var loopTime   = 7000; //ms
		var timeOut; //var is declare out of scope if we want to clearTimeout(timeOut);

		//image swapper
		var imageSwapper = function() {

			//check that modality is not opened
			if($.modality.isActive()) {
				timeOut = setTimeout(imageSwapper, loopTime);
				return;
			}

			//set bg through css
			context.velocity( {opacity: 0.5}, 300, function() {
				//set CSS
				$(this).css({
					'background' 	  		  : 'url('+images[(index++)%images.length].src +') no-repeat center center fixed',
					'-webkit-background-size' : 'cover',
					'-moz-background-size' 	  : 'cover',
					'-o-background-size' 	  : 'cover',
					'background-size' 		  : 'cover'
				});

			}).velocity( {opacity: 1}, 300, function() {
				//animation complete
				timeOut = setTimeout(imageSwapper, loopTime);
			});
		};

		//start timer
		setTimeout(imageSwapper, loopTime);
	};

	/**
	 * Video Background
	 */
	self.playVideoBackground = function(context){

        var video = $('<source/>', {
		    src      : 'https://player.vimeo.com/external/164336686.m3u8?s=4df661964b85b59e6907ba21339309458e0935ab', // url example
		    type     : 'application/x-mpegURL'
		});

		video.appendTo( $('video', context) );

		//video js
		self.player = videojs('bg-video',{
			"controls": false, "autoplay": true, "preload": "auto", "loop": true, "controlBar": false, textTrackSettings: false
		},function(){
			// Player (this) is initialized and ready.
			this.volume(0);
        });

		return true; // TODO: handle with callback
	};
};
