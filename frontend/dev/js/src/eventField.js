/**
 * EventField View Model
 * @class EventField
 */
module.exports = function() {

    //++ Module
    var self        = this;
    self.moduleName = "eventField";

    //++ Components
    self.component = Vue.component('efields', {
        template : '#vue-template-event-field',
        //params
        props : {
            field : {}
        }
    });

    //++ Methods

    /**
     * Init
     */
    self.init = function(data) {

        //update efields data
        if(typeof data.efields == "undefined" || !data.efields.length)
            return;

        //update efields with userEfields data
        for (var i = 0; i < data.efields.length; i++) {

            var obj      = data.efields[i];
            obj.nameAttr = 'efield_'+obj.namespace;
            obj.value    = "";

            if(typeof data.userEfields == "undefined")
                continue;

            if(typeof data.userEfields[obj.namespace] != "undefined")
                obj.value = data.userEfields[obj.namespace].value;
        }

        //set binding in parents vm (TODO: better handle this with Vuex)
        var vm = core.modules.eventInvitation.vm;
        vm.data.efields = data.efields;
    };
};
