/**
 * EventTicketInvitation View Model
 * @class EventInvitation
 */
module.exports = function() {

    //++ Module
    var self        = this;
    self.moduleName = "eventInvitation";

    //++ View Model
    self.vm = {
        data : {
            event    : {},
            fbSynced : false,
            //credits [dev]
            credits            : 0,
            creditsColor       : "orange",
            creditsAmountsOpts : [],
        },
        computed : {},
        methods  : {}
    };

    //++ UI Seletors
    _.assign(APP.UI, {
        sel_event_invitation : "#event-invitation"
    });

    /**
     * Init
     */
    self.init = function(data) {

        if(!_.isNull(data))
            self.vm.data.event = data.event;

        //facebook setup
        core.modules.facebook.config.login_fn = self.loginUserByFacebook;
        //redirection event
        core.modules.facebook.config.before_redirection_fn = self.onBeforeLoginUserByFacebook;

        //credits
        self.setCreditsAmountOptions(10); //Hardcoded!
    };

    /**
     * Set Credits Amount selector
     */
    self.setCreditsAmountOptions = function(max) {

        //validate selected option
        var array = [];
        for (var i = 0; i <= max; i++)
            array[i] = i;

        //set bindings
        self.vm.data.credits            = max;
        self.vm.data.creditsAmountsOpts = array;
    };

    /**
     * Listener - Logins user through FB SDK
     * @param {object} fb_payload - The facebook received payload
     */
    self.loginUserByFacebook = function(fb_payload) {

        //check for invalid form
        if(!core.modules.forms.isFormValid(APP.UI.sel_event_invitation))
            return;

        //set payload options
        fb_payload.user_data   = 1;
        fb_payload.check_perms = core.modules.facebook.config.perms; //scope perms

        //ajax request
        core.ajaxRequest({ method : 'POST', url : APP.baseUrl + 'facebook/login' }, null, fb_payload)
        .then(function(payload) {

            if(!payload || !_.isArray(payload.perms))
                return core.showAlert(APP.TRANS.FB.PERMS_REQUIRED, 'notice');

            self.vm.fbSynced = true;
            self.activateInvitationCode();

        }).done();
    };

    /**
     * Listener - onBeforeLoginUserByFacebook, called only by auth redirection strategy
     */
    self.onBeforeLoginUserByFacebook = function(url) {

        var form = $(APP.UI.sel_event_invitation);

        //set cookie
        Cookies.set('event-invitation', form.serializeArray());
    };

    /**
     * Activates Ticket Invitation Code
     * @param {object} e - The event handler
     */
    self.activateInvitationCode = function() {

        if(self.vm.event.social && !self.vm.fbSynced)
            return;

        var form = $(APP.UI.sel_event_invitation);

        //ajax request
        core.ajaxRequest({ method : 'POST', url : APP.baseUrl + 'event_invitation/activateCode' }, form)
        .done();
    };

    /**
     * Get Credits left
     */
    self.vm.computed.getCreditsLeft = function() {

        //get eassets childrens
        var eassets = this.$refs.easset;

        if(!eassets)
            return this.credits;

        var selectedQs = _.map(eassets, 'asset.selectedQ');

        //array sum
        var totalQ = !selectedQs.length ? 0 : _.reduce(selectedQs, function(sum, n) {
            return parseInt(sum) + parseInt(n);
        }, 0);

        //get credits left
        var credits_left = this.credits - totalQ;

        //set styled color
        this.creditsColor = credits_left > 0 ? 'orange' : 'red';

        return credits_left;
    };

    /**
     * activationConfirm
     * @param {object} e - The event handler
     */
    self.vm.methods.activationConfirm = function(e) {

        self.activateInvitationCode();
    };

    /**
     * Check for invitation code
     * @param  {object} e - The event handler
     */
    self.vm.methods.checkInvitationCode = function(e) {

        //ajax request
        core.ajaxRequest({ method : 'POST', url : APP.baseUrl + 'event_invitation/checkCode' }, e.target)
        .then(function(payload) {

            if(!payload) return;

            if(!_.isUndefined(payload.invitationCode))
                location.href = APP.baseUrl + 'account/invitations/confirm/' + payload.invitationCode;

        }).done();
    };
};
