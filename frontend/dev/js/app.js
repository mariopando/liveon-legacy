/**
 * App.js
 * CrazyCake
 * Browserify can only analyze static requires.
 * It is not in the scope of browserify to handle dynamic requires.
 * @module WebpackApp
 */

//load bundle dependencies
require('webpack_core');

//framework
require('foundation');

//Video JS
require('videojs');

// Extend media support
require('videojs-contrib-media-sources');

// HLS Support
require('videojs-contrib-hls');

/* Load modules */

var modules = [
    new (require('./src/ui.js'))(),
    new (require('./src/home.js'))(),
    new (require('./src/account.js'))(),
    new (require('./src/content.js'))(),
    new (require('./src/eventInvitation.js'))(),
    new (require('./src/eventField.js'))(),
    new (require('./src/eventAsset.js'))(),
    new (require('./src/eventSocial.js'))(),
    new (require('./src/userEventTicket.js'))(),
    new (require('./src/checkout.js'))(),
    new (require('./src/webpay.js'))()
];
//set modules
core.setModules(modules);

//Facebook module debugger: disabled js SDK
core.modules.facebook.config.disable_js_sdk = false;
