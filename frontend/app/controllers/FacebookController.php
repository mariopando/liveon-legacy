<?php
/**
 * FacebookController: handles login and facebook actions
 * Requires SessionController
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//CrazyCake Traits
use CrazyCake\Facebook\FacebookAuth;

class FacebookController extends SessionController
{
    /* traits */
    use FacebookAuth;

    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        //always call parent constructor
        parent::onConstruct();

        //set configurations
        $this->initFacebookAuth([
            //texts
            "trans" => [
                //texts
                "session_error"    => $this->trans->_('Ocurrió un problema con tu sesión de Facebook, porfavor inténtalo nuevamente. Si aún se presenta este problema, prueba iniciando una nueva sesión en Facebook.'),
                "invalid_email"    => $this->trans->_('No hemos logrado obtener tu correo primario de Facebook, puede que tu correo no esté validado en tu cuenta de Facebook.
                                                            Haz %a_open%click aquí%a_close% para configurar tu correo primario o agregar otro.', ["a_open" => '<a href="'.self::$FB_EMAIL_SETTINGS_URL.'" target="_blank">', "a_close" => "</a>"]),
                "oauth_redirected" => $this->trans->_('Ocurrió un problema con tu sesión de Facebook, porfavor inténtalo nuevamente.'),
                "oauth_perms"      => $this->trans->_('Debes aceptar los permisos de la aplicación en tu cuenta de Facebook.'),
                "session_switched" => $this->trans->_('Es posible que tengas abierta otra sesión de Facebook, intenta cerrando tu sesión actual de Facebook.'),
                "account_switched" => $this->trans->_('Esta sesión de Facebook está vinculada a otra cuenta %app_name%, intenta con otra cuenta en Facebook.', ["app_name" => $this->config->app->name]),
                "account_disabled" => $this->trans->_('Esta cuenta se encuentra desactivada por incumplimiento a nuestros términos y condiciones, porfavor %a_open%comunícate aquí%a_close% con nuestro equipo.')
            ]
        ]);
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Listener - event when user is successfully logged in through Facebook API
     * @param array $route - The route array
     * @param array $response - The response data
     */
    protected function onSuccessAuth(&$route, $response)
    {
        //permissions not granted, warn user for redirections strategy only
        if($route["strategy"] == "redirection" && $response["perms"] === false) {
            //set flash msg
            $this->flash->notice($this->facebook_auth_conf['trans']['oauth_perms']);
            //special cases
            if($route["action"] == "onFacebookAuthRedirection")
                $route["action"] = "confirm"; //switch route
        }
        //if valid perms notify any 'running' event that user is attending
        else if(!empty($response["perms"])) {

            //get facebook user
            $user_fb = UserFacebook::findFirstById($response["properties"]["fb_id"]);
            //trigger notification
            $this->_handleNotificationEvent($user_fb->user_id, "perms_ok");
        }
    }

    /**
     * On App Deauthorize,
     * Called when user deletes the app from his FB account,
     * Also when user change any permission that a webook is listening.
     * @param  object $fb_user - The facebook user ORM object
     */
    protected function onAppDeauthorized($data = array())
    {
        $this->logger->debug("onAppDeauthorized, ok received: ".print_r($data, true));

        //1) If fb_user was removed from DB
        if(isset($data["action"]) && $data["action"] == "deleted") {
            //trigger notification
            $this->_handleNotificationEvent($data["user_id"], "deleted");
        }
        //2) User changed a 'subscribed' permission in his Facebook account [webhook],
        else if(isset($data["changed_fields"]) && in_array("publish_actions", $data["changed_fields"])) {

            //get facebook user
            $user_fb = UserFacebook::findFirstById($data["fb_id"]);

            if(!$user_fb)
                return;

            //check fac & permission status
            $scope = $this->config->app->facebook->appScope;
            $perms = $this->_getAccesTokenPermissions(null, $user_fb->user_id, $scope);

            if($perms) {
                //valid permissions, continue flux
                $this->_handleNotificationEvent($user_fb->user_id, "perms_ok");
            }
            else {
                //otherwise drop user, publish action perm is required!
                $this->_deleteFacebookUser($user_fb->id);
            }
        }
    }

    /**
     * Handle notification Event
     * 1) Email notification action
     * 2) Triggers a push notification to subscribed clients
     * @param  integer $user_id - The user ID
     * @param  string $event - The event occurred
     */
    private function _handleNotificationEvent($user_id = 0, $event = "")
    {
        //get ticket that requires social link
        $user_tickets_social = UserEventTicket::getCollectionWithSocialRequired($user_id);

        //special case when user is removed from db & is attending an event.
        if($user_tickets_social && $event == "deleted") {
            //send mail message
            $this->_sendMailMessage("sendMailForFacebookDeauth", [
                 "user"    => User::getById($user_id),
                 "tickets" => $user_tickets_social
            ]);
        }

        //check if user has running event tickets
        $user_tickets = UserEventTicket::getIdsFromEventRunning($user_id);

        if(!$user_tickets)
            return true;

        //send async request
        $this->_asyncRequest([
            "module"     => "api",
            "controller" => "push",
            "action"     => "queue",
            "method"     => "post",
            "socket"     => true,
            "payload"    => [
                "action"  => "facebook_perm_updated",
                "objects" => $user_tickets
            ]
        ]);

        return true;
    }

    /**
     * Ajax POST - publish a wall story in Facebook user account
     * [DEBUG]
     */
    public function publishAction()
    {
        //make sure is ajax request
        $this->_onlyAjax();

        if(APP_ENVIRONMENT === 'production')
            $this->_sendJsonResponse(404);

        //get post params
        $data = $this->_handleRequestParams([
            'fbObject' => 'array',
            'action'   => 'string'
        ]);

        $event   = Event::findFirstByNamespace($data["fbObject"]["namespace"]);
        $user    = User::getById($this->user_session['id']);
        $user_fb = $user->userFacebook;

        if(!$user || !$user_fb) die("No FB user found");

        //set params
        $data = [
            "message"      => "LiveOn Action Test",
            "link"         => "https://www.liveon.cl",
            "place"        => '203114826397169',
            "tags"         => $user_fb->id,
            "created_time" => gmdate("Y-m-d\TH:i:s")
        ];

        try {
            //set response
            $response = $this->fb->post('me/feed', $data, $user_fb->fac)->getGraphNode();
            //send JSON response
            $this->_sendJsonResponse(200, $response);
        }
        catch (Exception $e) {
            $this->_sendJsonResponse(200, $e->getMessage(), "alert");
        }
    }
}
