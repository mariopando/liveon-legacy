<?php
/**
 * TranslationsController
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 * @TODO: Bring all controllers translations here
 */

class TranslationController extends CoreController
{
    /**
     * Javascript Translations
     * @static
     */
    public static function getJsTranslations()
    {
        //get translate service
        $di    = \Phalcon\DI::getDefault();
        $trans = $di->getShared('trans');

        return [
        	"ALERTS" => [
        		'INTERNAL_ERROR' 	=> $trans->_('Oops, ha ocurrido un problema, solucionaremos esto a la brevedad.'),
        		'SERVER_TIMEOUT'	=> $trans->_('Hemos perdido la comunicación, prueba revisando tu conexión a Internet.'),
        		'CSRF' 				=> $trans->_('Esta página ha estado inactiva por mucho tiempo, haz %a_open%click aquí%a_close% para refrescarla.',
                                                  ["a_open" => '<a href="javascript:location.reload();">', "a_close" => "</a>"]),
        		'NOT_FOUND' 		=> $trans->_('Oops, el enlace está roto. Porfavor inténtalo más tarde.'),
        		'BAD_REQUEST' 		=> $trans->_('Lo sentimos, no hemos logrado procesar tu petición. Intenta refrescando esta página.'),
        		'ACCESS_FORBIDDEN' 	=> $trans->_('Tu sesión ha caducado, porfavor %a_open%ingresa nuevamente aquí%a_close%.',
                                                 ["a_open" => '<a href="javascript:core.redirectTo(\'signIn\');">', "a_close" => "</a>"])
        	],
        	"ACTIONS" => [
        		'OK' 		 => $trans->_('Ok'),
        		'ACCEPT' 	 => $trans->_('Aceptar'),
        		'CANCEL' 	 => $trans->_('Cancelar'),
        		'NOT_NOW' 	 => $trans->_('Ahora No'),
        		'SEND' 		 => $trans->_('Enviar'),
        		'GOT_IT' 	 => $trans->_('Entendido'),
        		'LOADING' 	 => $trans->_('cargando ...'),
                'TRANSFER'   => $trans->_('Transferir'),
                'NULLIFY'    => $trans->_('Anular'),
                'UNLINK' 	 => $trans->_('Desvincular'),
        	],
        	"MESSAGES" => [
        		'CONTACT_SENT' => $trans->_('¡Hemos recibido tu mensaje! Te responderemos a la brevedad.')
        	],
        	"FB" => [
        		//facebook
        		'LOADING' 				=> $trans->_('cargando ...'),
                'LOADING_FALLBACK'  	=> $trans->_('Espera unos momentos mientras facebook carga...'),
                'LINKED'            	=> $trans->_('Conectado'),
        		'UNLINKED'  	        => $trans->_('No conectado'),
                'PERMS_REQUIRED'  	    => $trans->_('%app_name% necesita los permisos de Facebook (correo, publicación en tu muro).', ['app_name' => $di->getShared('config')->app->name]),
        		'PUBLISH_STORY_SUCCESS' => $trans->_('¡Hemos publicado con éxito la historia en tu muro!'),
        		'PUBLISH_STORY_FAILED'  => $trans->_('No tenemos permisos para publicar la historia en tu muro.'),
                //EventTicket Related
                'UNLINK_TITLE' => $trans->_('Entradas vinculadas'),
                'UNLINK_MSG'   => $trans->_('Tienes %number% entrada(s) que necesita(n) Facebook activado.', ["number" => "{n}"]),
        	],
        	"CHECKOUT" => [
        		'EMPTY_ORDER' => $trans->_('Selecciona tu compra')
        	],
            "TRANSFERS" => [
                'ACTION_TITLE' => $trans->_('Transferir Entrada'),
                'ACTION_MSG'   => $trans->_('¿Estás seguro que quieres transferir esta entrada?'),
                'CANCEL_TITLE' => $trans->_('Cancelar Transferencia'),
                'CANCEL_MSG'   => $trans->_('¿Estás seguro que quieres anular esta transferencia?'),
                "PENDING"      => $trans->_('En espera de %name% %email%', ["name" => "{name}", "email" => "<i>{email}</i>"]),
                'UNASSIGNED'   => $trans->_('Para comprar extras debes tener sola una entrada asociada a tu nombre.')
            ]
        ];
    }
}
