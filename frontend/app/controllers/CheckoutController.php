<?php
/**
 * CheckoutController: handles payments for checkouts
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//CrazyCake Traits
use CrazyCake\Checkout\CheckoutManager;

class CheckoutController extends SessionController
{
    /* traits */
    use CheckoutManager;
    use CheckoutHelper;

    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        parent::onConstruct();

        //set configurations
        $this->initCheckoutManager([
            "debug"                => false, //debugging flag, prevents redirections
            "max_per_item_allowed" => 5,     //integer
            "max_user_acquisition" => 10,    //integer
            "default_currency"     => "CLP",
            //javascript modules
            "js_modules" => [
                "checkout", // no need to set params on main module
                "webpay" => null
            ],
            //texts
            "trans" => [
                //texts
                "notice_auth"         => $this->trans->_('Ingresa a tu cuenta %app_name% para obtener tu entrada.', ['app_name' => '<span class="marked">'.$this->config->app->name.'</span>']),
                "success_checkout"    => $this->trans->_('Estamos procesando tu orden, revisa tu correo dentro de un momento.'),
                "error_unexpected"    => $this->trans->_('Ha ocurrido algo inesperado, porfavor inténtalo más tarde.'),
                "error_no_stock"      => $this->trans->_('Lo sentimos, el item %name% está fuera de stock desde hace unos momentos.', ["name" => "<strong>{name}</strong>"]),
                "error_invoice_email" => $this->trans->_('Porfavor ingresa un correo de envío válido.'),
                "error_max_purchased" => $this->trans->_('Haz alcanzado el límite de compra de un producto, selecciona una cantidad menor.'),
                "error_max_total"     => $this->trans->_('Puedes comprar un máximo de %num% unidades.', ["num" => "<span>{num}</span>"])
            ]
        ]);
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Listener - On Buy Order Creation
     * @param object $checkout - The object checkout
     */
    public function onBeforeBuyOrderCreation(&$checkout)
    {
        //NOTE: checking max items validation
        if(UserCheckout::maxObjectsPurchased(
            $this->user_session["id"],
            $checkout,
            $this->checkout_manager_conf["max_user_acquisition"])) {

                throw new Exception($this->checkout_manager_conf["trans"]["error_max_purchased"]);
        }

        //set data for event type, #TODO implementar mas sistemas de pagos
        if($checkout->gateway == "webpaykcc") {
            //set formatted amount for Webpay
            $checkout->amountFormatted = WebpayController::formatAmountForKcc($checkout->amount);
        }
    }

    /**
     * Listener - On Succes Checkout Task Complete
     * @param int $user - The user ID
     * @param object $checkout - The object checkout
     */
    public function onSuccessCheckoutTaskComplete($user_id, &$checkout)
    {
        $this->logger->debug("CheckoutController::onSuccessCheckoutTaskComplete, checkout: ".json_encode($checkout));

        //set properties
        $checkout->event = Event::findFirstByNamespace($checkout->categories[0]);

        //1) Queue Event push notifications
        if($checkout->event && $checkout->event->isRunning()) {

            //send async request
            $this->_asyncRequest([
                "module"     => "api",
                "controller" => "push",
                "action"     => "queue",
                "method"     => "post",
                "socket"     => !$this->checkout_manager_conf["debug"], //no socket debugging
                "payload"    => [
                    "action"  => "new_user_event_ticket",
                    "objects" => $checkout->newObjectIds
                ]
            ]);
        }

        //2) se crea el invoice (PDF file)
        $task_invoice = (new StorageController())->generateInvoice($user_id, $checkout);

        if(isset($task_invoice->error))
            throw new Exception($task_invoice->error);

        //print_r($checkout->objects);exit;
        //3) send mail message
        $this->_sendMailMessage("sendMailForSuccessCheckout", [
             "user"     => User::getById($user_id),
             "checkout" => $checkout,
             "invoice"  => $task_invoice->binary
        ]);
    }

    /**
     * Listener - On Skipped payment
     * @param object $checkout - The object checkout
     */
    public function onSkippedPayment(&$checkout)
    {
        $checkout->comment = $this->trans->_("Rectificación de compra.");

        //check for a trx record
        $trx = UserCheckoutTrx::findFirstByBuyOrder($checkout->buy_order);

        if($trx) {
            $trx = UserCheckoutTrx::getById($trx->id);
            $checkout->trx = $trx->toArray();
        }
        //s($checkout);exit;
    }

    /**
     * View - Checkout view, gets ticket data
     */
    public function ticketsAction($namespace = "")
    {
        try {
            //get event
            $event = Event::findFirstByNamespace($namespace);
            $user  = User::getById($this->user_session['id']);

            if(!$event)
                throw new Exception("Invalid event namespace data ($namespace)");

            //get tickets
            $tickets = EventTicket::getCollectionForUI(10, 0, "e.id = '$event->id'", "et.price ASC", false);

            //check if event is closed or for non available tickets
            if(($event->state != "open" && $event->state != "invisible") || !$tickets) {

                //make event unavailable if all tickets soldouts
                if($event->state != "closed" && !$tickets)
                    $event->update(["state" => "closed"]);

                return $this->_redirectTo("event/".$event->namespace);
            }

            //check if user already subscribed to a non-paid event
            //NOTE: Por el momento los eventos gratis tienen 1 tipo de entrada, la cantidad ya es soportada.
            if ($event->type == "free" || $event->type == "invitation") {

                $user_ticket = UserEventTicket::getCollection($user->id, $event->id);

                if($user_ticket)
                    return $this->_redirectTo("account/tickets/".$event->namespace);
            }

            //load webpay setup
            (new WebpayController())->loadSetupForView();

            //set categories
            $categories = [$namespace];

            //reduce objects
            $objects = $tickets->reduce(["name", "small_print", "price", "currency", "state", "_ext"]);

            //custom vars
            $this->view->setVar("event", $event);
            //load common setup (and js modules)
            $this->_setupCheckoutView($event->type, $categories, ['EventTicket' => $objects]);
        }
        catch(Exception $e) {
            //var_dump($e);exit;
            $this->_redirectToNotFound();
        }
    }

    /**
     * View - Checkout view, gets assets data
     */
    public function assetsAction($ticket_code = "")
    {
        try {
            $userTicket  = UserEventTicket::getByCodeAndUserId($ticket_code, $this->user_session['id']);
            $event       = $userTicket->eventTicket->event;
            $fallbackUri = 'account/tickets/'.$event->namespace;

            if(!$event)
                throw new Exception("Invalid event for ticket code $ticket_code");

            //get tickets
            $assets = EventTicketAsset::getCollectionForUI($userTicket->eventTicket->id, $userTicket->id);
            //get unassigned tickets
            $unassignedTickets = UserEventTicket::countUnassigned($this->user_session['id'], $event->id);

            //validates assets, unassigned tickets & event
            if( !$assets
                || ($event->state != "open" && $event->state != "invisible")
                || ($userTicket->eventTicket->state == "closed")
            ){
                return $this->_redirectTo($fallbackUri);
            }

            //custom vars
            $this->view->setVar("event", $event);
            $this->view->setVar("user_ticket", $userTicket);

            //load webpay setup
            (new WebpayController())->loadSetupForView();

            //limit max acquisition to one
            $this->checkout_manager_conf["max_user_acquisition"] = 1;
            $this->checkout_manager_conf["max_per_item_allowed"] = 1;

            //categories
            $categories = [$event->namespace, $userTicket->id];

            //to array temporal, soluciona bug toArray props a cada objecto
            $objects = EventTicketAsset::customReduce($assets);

            //load common setup (and js modules)
            $this->_setupCheckoutView('paid', $categories, ['EventTicketAsset' => $objects]);
        }
        catch(Exception $e) {
            //var_dump($e);exit;
            $this->_redirectToNotFound();
        }
    }
}
