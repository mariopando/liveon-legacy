<?php
/**
 * EventController: all events operations
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

class EventController extends SessionController
{
    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        parent::onConstruct();
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * View - index for events
     */
    public function indexAction($namespace = "")
    {
        try {
            //$this->_logDatabaseStatements();
            $event = Event::findFirstByNamespace($namespace);
            //validates event
            if(!$event)
                throw new Exception("Invalid event with namespace ".$namespace);

            //get event tickets
            $tickets = EventTicket::getCollectionForUI(10, 0, "e.id = '".$event->id."'", "et.price ASC", false);
            //validates ticket
            if(!$tickets)
                throw new Exception("Invalid tickets for event".$namespace);

            //check is user already has this ticket (applies only for free events)
            if(($event->type == "free" || $event->type == "invitation") && $this->user_session) {

                $user_ticket = UserEventTicket::getCollection($this->user_session['id'], $event->id);
                $this->view->setVar("user_owns_ticket", $user_ticket ? true : false);
            }

            //set CEO robots to not follow invisible events
            if($event->state == "invisible")
                $this->view->setVar("html_disallow_robots", true);
        }
        catch(Exception $e) {
            return $this->_redirectToNotFound();
        }

        //set data for view
        $this->view->setVar("event", $event);
        $this->view->setVar("tickets", $tickets);
        //load js modules
        $this->_loadJsModules([
            "facebook" => null
        ]);
    }
}
