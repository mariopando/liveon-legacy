<?php
/**
 * IndexController, handles Index pages & send contact action
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

class IndexController extends SessionController
{
    /* consts */
    const INDEX_MAIN_EVENT_NAMESPACE = 'heineken_secret_liveset_3';

    /**
     * View - Index page
     */
    public function indexAction()
    {
        //HTML props
        $this->view->setVar("html_metas", true);
        //$this->view->setVar("html_title", $this->trans->_("Acceso a tu mundo relevante"));

        //NOTE: HARDCODED (main event)
        $ns = (APP_ENVIRONMENT == 'production') ? "heineken_secret_liveset_3" : self::INDEX_MAIN_EVENT_NAMESPACE;
        //get first X events
        $n = (APP_ENVIRONMENT == 'production') ? 5 : 5;

        $main_ticket = EventTicket::getCollectionForUI(1, 0, "namespace = '".$ns."'");
        $tickets     = EventTicket::getCollectionForUI($n, 0, "e.state != 'invisible'", "et.id DESC");
        //arg: "namespace != '".self::INDEX_MAIN_EVENT_NAMESPACE." AND '

        if($main_ticket)
            $this->view->setVar("main_ticket", $main_ticket->getFirst());

        if($tickets)
            $this->view->setVar("tickets", $tickets);

        //load js modules
        $this->_loadJsModules([
            "home" => null
        ]);
    }
}
