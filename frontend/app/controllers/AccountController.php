<?php
/**
 * AccountController: requires user be logged in, handle private user operations
 * @todo Build a trait from this controller
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

use CrazyCake\Account\AccountManager;

class AccountController extends SessionController
{
    /* traits */
    use AccountManager;

    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        parent::onConstruct();
        //set configurations
        $this->initAccountManager([
            "profile_pass_min_length" => 8,
            "trans" => [
                //texts
                "current_pass_empty" => $this->trans->_('Para modificar tu contraseña debes ingresar tu contraseña actual.'),
                "pass_too_short"     => $this->trans->_('Debes ingresar una contraseña de al menos 8 caracteres.'),
                "pass_dont_match"    => $this->trans->_('Tu contraseña actual no es correcta.'),
                "new_pass_equals"    => $this->trans->_('Tu nueva contraseña debe ser diferente a la actual.'),
                "invalid_names"      => $this->trans->_('Tu nombre no parece ser válido.'),
                "profile_saved"      => $this->trans->_('Tus cambios han sido guardados')
            ]
        ]);
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * View - index My Tickets
     * @todo phalcon toArray & afterFetch methods
     */
    public function indexAction()
    {
        //get my tickets filtered by events
        $result = UserEventTicket::getCollectionForUI($this->user_session['id'], null, true, null, "et.id DESC");

        if($result)
            $this->view->setVar("my_tickets", $result);

        //set current view
        $this->view->setVar("current_view", "tickets");
    }

    /**
     * View - my account profile
     */
    public function profileAction()
    {
        //load facebook login url
        (new FacebookController())->loadFacebookLoginURL([
            "controller" => "account",
            "action"     => "profile"
        ], true);

        //social linked objects
        $socialLinkedTickets = UserEventTicket::getCollectionWithSocialRequired($this->user_session['id']);

        //set current view
        $this->view->setVar("current_view", "profile");
        //load js modules
        $this->_loadJsModules([
            "account"  => null,
            "facebook" => null,
            "eventSocial"  => [
                "user"                => $this->user_session["fb_id"],
                "socialLinkedTickets" => $socialLinkedTickets ? $socialLinkedTickets->toArray() : []
            ]
        ]);
    }

    /**
     * Trait delegate function
     * @param  object $user The user object
     * @param  array $data The data array
     * @return array
     */
    protected function beforeUpdateProfile($user, $data)
    {
        return array();
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */
}
