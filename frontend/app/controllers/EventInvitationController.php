<?php
/**
 * EventInvitationController
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

class EventInvitationController extends SessionController
{
    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        parent::onConstruct();

        //msg keys
        $this->MSGS = [
            "NOTICE_AUTH"           => $this->trans->_('Ingresa a tu cuenta %app_name% para obtener tu entrada.', ['app_name' => '<span class="marked">'.$this->config->app->name.'</span>']),
            "INVALID_CODE"          => $this->trans->_("¿Te equivocaste de código?"),
            "UNAVAILABLE_TICKET"    => $this->trans->_("Lo sentimos, la entrada %name% ya no está disponible. Porfavor presenta tu invitación en el lugar del evento.", ["name" => "{name}"]),
            "ERROR_INVOICE_EMAIL"   => $this->trans->_('Porfavor ingresa un correo de envío válido.'),
            "ERROR_EMPTY FIELDS"    => $this->trans->_('Revisa la información del campo %name%.', ["name" => "{name}"]),
            "ERROR_UNEXPECTED"      => $this->trans->_('Ocurrió algo inesperado, porfavor inténtalo más tarde.'),
            "ERROR_EXISTING_TICKET" => $this->trans->_('Ya tienes una entrada activa para %name%.', ["name" => "{name}"]),
            "INVOICE_COMMENT"       => $this->trans->_('Código de Canje.'),
            "SUCCESS_ACTIVATION"    => $this->trans->_('Tienes una nueva entrada, revisa tu correo dentro de un momento.')
        ];
    }

    /**
     * Initializer
     */
    protected function initialize()
    {
        parent::initialize();

        if(!$this->_checkUserIsLoggedIn()) {

            //set a flash message for non authenticated users
            $this->flash->notice($this->MSGS["NOTICE_AUTH"]);
            //if not logged In, set this URI to redirected after logIn
            $this->_setSessionRedirectionOnLoggedIn();
        }

        //handle response, dispatch to auth/logout
        $this->_checkUserIsLoggedIn(true);
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * View - Invitations
     */
    public function invitationsAction()
    {
        //set current view
        $this->view->setVar("current_view", "invitations");
        //load js modules
        $this->_loadJsModules([
            "eventInvitation" => null
        ]);

        //pick layout
        $this->view->setLayout('invitations');
        $this->view->pick("invitations/index");
    }

    /**
     * View - Checks code and renders confirmation view (After code validated)
     * @param  string $code - The invitations code
     */
    public function confirmAction($code = null)
    {
        //handle invitations code
        $invitation = $this->_handleInvitationCode($code);
        //if invalid data force dispatch
        if(!$invitation)
            $this->_redirectTo("account/invitations");

        //get ticket & event
        $ticket = $invitation->eventTicket;
        $event  = $ticket->event;
        $user   = User::getById($this->user_session['id']);

        //check event and ticket state
        if($event->state === "closed" || $event->state === "finished" || $ticket->state === "closed") {
            //set flash message
            $this->flash->warning(str_replace("{name}", $ticket->name, $this->MSGS["UNAVAILABLE_TICKET"]));
            //redirect
            return $this->_redirectTo("account/invitations");
        }

        //check for last used invoice email
        $lastCheckout = UserCheckout::findFirst([
            "user_id = ?0",
            "order" => "local_time DESC",
            "bind"  => [$user->id]
        ]);
        //$this->_dump($lastCheckout);

        //set view data
        $this->view->setVars([
            "current_view"   => "invitations",
            "event"          => $event,
            "ticket"         => $ticket,
            "invitationCode" => $code,
            "invoiceEmail"   => $lastCheckout ? $lastCheckout->invoice_email : $user->email
        ]);

        //set data for JS
        $eventJsObj = $event->toArray(['namespace']);
        $eventJsObj["social"] = $event->eventSocial ? $event->eventSocial->toArray(["required"]) : null;

        //load js modules
        $js_modules = [
            "eventInvitation" => [
                "event" => $eventJsObj
            ],
            "eventField" => [
                "efields"     => EventField::getCollectionForUI($event->id),
                "userEfields" => UserEventField::getCollection($user->id)
            ],
            "eventAsset" => [
                "eassets" => EventTicketAsset::getCollectionForUI($ticket->id)
            ]
        ];

        //event requires social activation?
        if(!empty($event->eventSocial) && $event->eventSocial->required) {

            //check if user already has a ticket
            $user_ticket = UserEventTicket::getCollection($user->id, $event->id);

            if($user_ticket) {
                //set flash message & redirect
                $this->flash->warning(str_replace("{name}", $event->name, $this->MSGS["ERROR_EXISTING_TICKET"]));
                //redirect
                return $this->_redirectTo("account/invitations");
            }

            //set js module
            $js_modules["facebook"] = null;
            //load facebook login url for redirection support
            (new FacebookController())->loadFacebookLoginURL([
                "controller" => "event_invitation",
                "action"     => "onFacebookAuthRedirection",
                "payload"    => [$code]
            ], true);
        }

        //load js modules
        $this->_loadJsModules($js_modules);
        //pick view
        $this->view->setLayout('invitations');
        $this->view->pick("invitations/confirm");
    }

    /**
     * Listener - onFacebookAuth redirection handler
     */
    public function onFacebookAuthRedirectionAction($code = '')
    {
        try {
            //validate cookie
            if(!$this->cookies->has('event-invitation'))
                throw new Exception("no cookie found for invitation form");

            //get the cookie's value
            $data   = json_decode($this->cookies->get('event-invitation')->getValue(), true);
            $params = [];

            //order data params
            foreach ($data as $p)
                $params[$p["name"]] = $p["value"];

            if(!isset($params["invitationCode"]))
                throw new Exception("invalid cookie value for key invitationCode");

            $invitation = $this->invitationCodeActivation($params);

            if($invitation !== true) {
                $this->flash->warning($invitation);
                throw new Exception("invitation activation failed");
            }

            //OK, success
            $this->_redirectTo("account");
        }
        catch(Exception $e) {
            //same flux
            $this->confirmAction($code);
        }
    }

    /**
     * Ajax (GET) - checks a form activation code
     */
    public function checkCodeAction()
    {
        //make sure is ajax request
        $this->_onlyAjax();

        //get post params
        $data = $this->_handleRequestParams([
            'invitationCode' => 'string'
        ], 'POST');

        //get the code!
        $invitation = EventInvitation::findFirstByCode(trim($data["invitationCode"]));

        if(!$invitation || !is_null($invitation->activation_user_id))
            $this->_sendJsonResponse(200, $this->MSGS["INVALID_CODE"], "alert");

        //send response
        $this->_sendJsonResponse(200, $data);
    }

    /**
     * Ajax (POST) - activate the code
     * TODO: validate if user has FB activated?
     */
    public function activateCodeAction()
    {
        //make sure is ajax request
        $this->_onlyAjax();

        //get form data
        $data = $this->_handleRequestParams([
            "invitationCode" => "string",
            "@invoiceEmail"  => "string" //custom validation
        ], 'POST');

        $activation = $this->invitationCodeActivation($data);
        //print_r($activation);exit;
        if($activation === true)
            $this->_sendJsonResponse(200, ["redirect" => "account"]);
        else
            $this->_sendJsonResponse(200, $activation, "alert");
    }

    /**
     * Handler - Invitation code activation
     * @param array $data - The form input params
     */
    protected function invitationCodeActivation($data = array())
    {
        try {
            //get user ID and user object
            $user = User::getById($this->user_session["id"]);

            //handle invitation code
            $invitation = $this->_handleInvitationCode($data["invitationCode"]);

            if(!$invitation)
                throw new Exception($this->MSGS["ERROR_UNEXPECTED"]);

            //check invoice email if set
            if(!isset($data["invoiceEmail"]) || !filter_var($data['invoiceEmail'], FILTER_VALIDATE_EMAIL))
                throw new Exception($this->MSGS["ERROR_INVOICE_EMAIL"]);

            //lower case email
            $data["invoiceEmail"] = strtolower($data["invoiceEmail"]);

            //TODO: save new invoice email?

            //1) Get ticket object
            $ticket = $invitation->eventTicket;
            $event  = Event::getById($ticket->event_id);

            if(!$ticket || !$event)
                throw new Exception($this->MSGS["ERROR_UNEXPECTED"]);

            //2) Check event socials & if user already has a ticket
            if(!empty($event->eventSocial) && $event->eventSocial->required) {

                //check if user already has a ticket
                $user_ticket = UserEventTicket::getCollection($user->id, $event->id);

                if($user_ticket)
                    throw new Exception(str_replace("{name}", $event->name, $this->MSGS["ERROR_EXISTING_TICKET"]));
            }

            //3) Validates & saves events fields
            $eventFieldsData = EventField::filterData($event->id, $data);

            //checks for an error
            if($eventFieldsData->error_field)
                throw new Exception(str_replace("{name}", $eventFieldsData->error_field, $this->MSGS["ERROR_EMPTY FIELDS"]));

            //saves data if given
            if($eventFieldsData->data)
                UserEventField::saveData($user->id, $eventFieldsData->data);

            //4) Creates the checkout object
            $checkout = (object)[
                "buy_order"      => uniqid(),
                "type"           => "action",
                "categories"     => [$ticket->event->namespace],
                "comment"        => $this->MSGS["INVOICE_COMMENT"],
                "invoice_email"  => $data["invoiceEmail"]
            ];

            //instance checkout controller & parse checkout data
            $checkoutController = new CheckoutController();
            $checkoutController->parseCheckoutObjects($checkout, $data);
            //print_r($checkout->objectsClasses);exit;

            //5) Update status of activation (only for production)
            if(APP_ENVIRONMENT === "production")
                $invitation->update(["activation_user_id" => $user->id]);

            //6) Call objects success checkout logic
            (new EventTicketController())->onSuccessCheckout($user->id, $checkout);
            //set created ticket to checkout category
            $user_ticket_ids         = $checkout->newObjectIds;
            $checkout->categories[1] = $user_ticket_ids[0];

            //check for events assets (TODO: support mutation of newObjectIds)
            if(in_array("EventTicketAsset", $checkout->objectsClasses))
                (new EventTicketAssetController())->onSuccessCheckout($user->id, $checkout);

            //7) Call checkout success event & response msg
            $checkout->newObjectIds = $user_ticket_ids;
            $checkoutController->onSuccessCheckoutTaskComplete($user->id, $checkout);

            //Ok, set success flash message
            $this->flash->success($this->MSGS["SUCCESS_ACTIVATION"]);
            return true;
        }
        catch(Exception $e) {

            return $e->getMessage();
        }
    }
    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Handles invitation code
     * @param string $code - The code
     */
    private function _handleInvitationCode($code = null)
    {
        try {

            if (empty($code))
                throw new Exception("got empty input invitation code");

            //get ORM object
            $invitation = EventInvitation::findFirstByCode($code);

            //check that codes has not activated yet
            if(!$invitation || !is_null($invitation->activation_user_id))
                throw new Exception("Code ".$code." was not found");

            //get user ID and user object
            $user_id = $this->user_session["id"];

            //get ticket object.
            $ticket = $invitation->eventTicket;

            if(!$ticket)
                throw new Exception("Invitation (".$invitation->id.") don't supports a ticket object");

            return $invitation;
        }
        catch (Exception $e) {

            $actionName = $this->router->getActionName();

            $this->logger->error("EventInvitationController::$actionName -> Error in code activation (user_id: $user_id). Trace: ".$e->getMessage());

            //if ajax return repsonse
            if($this->request->isAjax())
                return $this->_sendJsonResponse(400);

            //set error message
            $this->view->setVar("error_message", $this->trans->_("Lo sentimos, este código ha caducado."));
            //dispath to expire page
            $this->dispatcher->forward(["controller" => "error", "action" => "expired"]);
            return false;
        }
    }
}
