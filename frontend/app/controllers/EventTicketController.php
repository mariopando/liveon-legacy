<?php
/**
 * EventTicketController: all events operations
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//imports
use CrazyCake\Models\BaseResultset;

class EventTicketController extends SessionController
{
    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        parent::onConstruct();

        //msg keys
        $this->MSGS = [
            "ASSIGNMENT_FAILED" => $this->trans->_("No hemos logrado vincularte a este evento porfavor %a_open%comunícate aquí%a_close% con nuestro equipo.",
                                                    ["a_open" => '<a href="javascript:core.redirectTo(\'contact\');">', "a_close" => '</a>'])
        ];
    }

    /**
     * Initializer
     */
    protected function initialize()
    {
        parent::initialize();

        //handle response, dispatch to auth/logout
        if($this->router->getActionName() == "tickets")
            $this->_checkUserIsLoggedIn(true);
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Listener - Event on success checkout
     * LOGIC:
     * 1) Crates userTickets records
     * 2) Substract item quantity
     * 3) Create userTicket QR images
     * @param  int $user_id The user ID
     * @param  object $checkout The checkout object
     * @return checkout object
     */
    public function onSuccessCheckout($user_id = 0, &$checkout)
    {
        //1) asociar el usuario al evento (UserEventTicket, UserEventTicketAsset)
        $task_assign = $this->_assignTicketsForUser($user_id, $checkout);

        if(isset($task_assign->error))
            throw new Exception($task_assign->error);

        //set user tickets IDS on checkout object
        $checkout->newObjectIds = $task_assign->newObjectIds;

        //2) substract checkout items quantity
        UserCheckoutObject::substractStock($checkout->objects);

        //POS order?
        if(empty($user_id))
            return $checkout;

        //3) se generan user tickets QR image files
        $task_qrs = $this->_createUserTickets($user_id, $checkout->newObjectIds);

        if(!$task_qrs)
            throw new Exception("Failed generating QR images for userId: $user_id and buyOrder: ".$checkout->buy_order);

        return $checkout;
    }

    /**
     * View - tickets detail action, event namespace is required
     */
    public function ticketsAction($namespace = "")
    {
        $event = Event::findFirstByNamespace($namespace);
        //validates event
        if(!$event)
            return $this->_redirectToAccount();

        //get my ticket for a given event
        $result = UserEventTicket::getCollectionForUI($this->user_session['id'], $event->id, false, null, "uet.ticket_id ASC");
        //validation
        if(!$result)
            return $this->_redirectToAccount();

        //get unassigned tickets
        $unassignedTickets = UserEventTicket::countUnassigned($this->user_session['id'], $event->id);

        //prepare tickets JS
        $ticketsJs = [];
        foreach ($result as $ticket) {

            $ticketsJs[] = array_merge(
                $ticket->toArray(["code", "_ext"]),
                $ticket->eventTicket->toArray(["name"])
            );
        }

        //append data to JS objects
        UserEventTicketTransfer::setDataForUserTicketsJs($ticketsJs);

        //set current view
        $this->view->setVars([
            "event"        => $event,
            "current_view" => "tickets",
            "invoice_url"  => $this->_baseUrl("storage/invoice/".$event->_ext["id_hashed"]),
            //for components
            "birthday_elements" => \CrazyCake\Helpers\Forms::getBirthdaySelectors()
        ]);

        //set data for JS
        $eventJsObj = $event->toArray(["namespace", "type", "state"]);
        $eventJsObj["social"] = $event->eventSocial ? $event->eventSocial->toArray(["required"]) : null;

        //load js modules
        $this->_loadJsModules([
            "userEventTicket" => [
                "user"              => $this->view->getVar("user_data"),
                "event"             => $eventJsObj,
                "tickets"           => $ticketsJs,
                "unassignedTickets" => $unassignedTickets
            ]
        ]);

        //pick layout
        $this->view->setLayout('account');
        $this->view->pick('account/tickets');
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Assign a user to an event.
     * Este metodo actua de igual forma si un evento es pagado o libre.
     * @param int $user_id The user id
     * @param object $checkout The checkout objects
     * @return object $response
     */
    private function _assignTicketsForUser($user_id, $checkout)
    {
        //create response
        $response = new \stdClass();

        //validates checkout
        if(!isset($checkout->objects) || empty($checkout->objects)) {
            $this->logger->error("EventTicketController::_assignTicketsForUser -> Received empty checkout objects");
            throw new Exception($this->MSGS['ASSIGNMENT_FAILED']);
        }

        //user tickets array
        $savedObjects = [];

        //1) Association between user & object (ticket, asset, ...)
        try {

            //+begin db transaction
            $this->db->begin();

            //loop through objects
            foreach ($checkout->objects as $obj) {

                //DB SAVE ACTION: associates object
                if($obj->object_class != "EventTicket")
                    continue;

                //foreach quantity creates an object and push to userTickets
                for ($i = 0; $i < $obj->quantity; $i++) {
                    array_push($savedObjects, $this->_newTicketForUser($user_id, $obj));
                }
            }

            //+commit db actions
            $this->db->commit();
        }
        catch (Exception $e) {
            //rollback action
            $this->db->rollback();
            //set and send response
            $response->error = $e->getMessage();
            return $response;
        }

        //set userTickets arrays
        $response->newObjectIds = BaseResultset::getIdsArray($savedObjects, "id");
        //return response
        return $response;
    }

    /**
     * Creates relation user => event-ticket
     * Called by reflection
     * @param int $user_id The user id
     * @param object $object The Checkout Object
     * @return ORM object
     */
    private function _newTicketForUser($user_id = 0, $object)
    {
        //create user-event-ticket record (qr code is generated automatically)
        $props = ["ticket_id" => $object->object_id];

        $props["user_id"] = $user_id;
        $userTicket = new UserEventTicket();

        if(!$userTicket->save($props)) {
            throw new Exception($this->MSGS['ASSIGNMENT_FAILED']);
        }

        return $userTicket;
    }

    /**
     * Creates user tickets, returns false if process failed
     * @param int $user_id The user id
     * @param array $userTicketIds The user tickets ORM objects
     * @return boolean
     */
    private function _createUserTickets($user_id = 0, $userTicketIds = array())
    {
        if(empty($userTicketIds))
            return false;

        //get user tickets
        $userTickets = UserEventTicket::getCollection($user_id, null, $userTicketIds);

        //Generate QRs for each ticket (QR GENERATOR)
        $qrMakerTask = (new StorageController())->generateQRForUserTickets($user_id, $userTickets);

        if(!isset($qrMakerTask->error))
            return true;

        //rollback if an error ocurred,clean db created objects
        foreach ($qrMakerTask->objects as $ormObject)
            $ormObject->delete();

        return false;
    }
}
