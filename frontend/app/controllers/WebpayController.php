<?php
/**
 * WebpayController: handles Webpay operations, requires user be logged in.
 * Uses Kcc WebPay module
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//CrazyCake Traits
use CrazyCake\Transbank\KccManager;

class WebpayController extends SessionController
{
    /* traits */
    use KccManager;

    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        parent::onConstruct();

        //set configurations
        $this->initKccManager([
            'paymentCgiUri' => 'cgi-bin/tbk_bp_pago.cgi',
            'successUri'    => 'webpay/success',
            'failedUri'     => 'webpay/failed',
            //texts
            "trans" => [
                "success_trx" => $this->trans->_('Tu compra se ha realizado exitosamente, revisa tu correo dentro de un momento.')
            ]
        ]);
    }
    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Listener before render success page
     * @param object $checkout The checkout object
     */
    public function onBeforeRenderSuccessPage(&$checkout)
    {
        //set categories
        $checkout->categories = explode(",", $checkout->categories);
        //set event data
        $checkout->event = Event::findFirstByNamespace($checkout->categories[0]);
    }
}
