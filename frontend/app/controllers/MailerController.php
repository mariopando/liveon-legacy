<?php
/**
 * MailerController: sends email messages and notifications.
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

use CrazyCake\Services\Mailer;

class MailerController extends CoreController
{
    /* traits */
    use Mailer;

    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        //always call parent constructor
        parent::onConstruct();

        //set configurations
        $this->initMailer([
            "appName"     => $this->config->app->name,
            "client"      => $this->client,
            "mandrillKey" => $this->config->app->mandrill->accessKey,
            "cssFile"     => PUBLIC_PATH.'assets/mailing.css',
            //emails
            "senderEmail"  => $this->config->app->emails->sender,
            'supportEmail' => $this->config->app->emails->support,
            "contactEmail" => $this->config->app->emails->contact,
            //texts
            "trans" => [
                //messages subjects
                "subject_activation" => $this->trans->_("Confirma tu cuenta"),
                "subject_password"   => $this->trans->_("Recupera tu contraseña")
            ]
        ]);
    }

    /** ----------------------------------------------------- § ---------------------------------------------------- **/

    /**
     * View for debuging - Renders a mail message or a template
     */
    public function renderAction($view = null)
    {
        if(APP_ENVIRONMENT == 'production' || empty($view))
            $this->_redirectToNotFound();

        $ticket = UserEventTicket::findFirst();

        if(!$ticket)
            die("No UserEventTicket records found");

        $user = User::getById($ticket->user_id);
        //extra data
        $tickets = UserEventTicket::getCollectionWithSocialRequired($user->id);
        //set rendered view vars
        $this->mailer_conf["data_url"]      = $this->_baseUrl('fake/path');
        $this->mailer_conf["data_user"]     = $user;
        $this->mailer_conf["data_event"]    = $ticket->eventTicket->event;
        $this->mailer_conf["data_ticket"]   = $ticket->eventTicket;
        $this->mailer_conf["data_tickets"]  = $tickets;
        $this->mailer_conf["data_receiver"] = (object)["name" => "Jim Doe", "email" => "jim@doe.com"];

        //for contact template
        $this->mailer_conf["data_name"]    = "John Doe";
        $this->mailer_conf["data_email"]   = "john@doe.com";
        $this->mailer_conf["data_message"] = "Mail example for contact.";

        //get HTML
        $html_raw = $this->_getInlineStyledHtml($view, $this->mailer_conf);
        //render view
        die($html_raw);
    }

    /**
     * Async Method - Sends an email for post checkout action.
     * This method is called as another internal request for thread behavior
     * @param array $data - The sending data
     * @return json response
     */
    public function sendMailForSuccessCheckout($data)
    {
        //get data
        $user     = $data["user"];
        $checkout = $data["checkout"];
        $event    = $checkout->event;

        if(!$checkout || !$event)
            throw new Exception("MailerController::sendMailForSuccessCheckout -> a checkout & event object are required");

        //set data for mailing
        $this->mailer_conf["data_user"]     = $user;
        $this->mailer_conf["data_event"]    = $event;
        $this->mailer_conf["data_checkout"] = $checkout;
        //set email
        $this->mailer_conf["data_email"] = isset($checkout->invoice_email) ? $checkout->invoice_email : $user->email;

        ///set layout & message properties
        if(in_array("EventTicket", $checkout->objectsClasses)) {

            $html_raw = $this->_getInlineStyledHtml("ticketCheckout", $this->mailer_conf);
            $subject  = $this->trans->_("Tu entrada para ".$event->name);
        }
        else {

            $html_raw = $this->_getInlineStyledHtml("assetCheckout", $this->mailer_conf);
            $subject  = $this->trans->_("Compra exitosa");
        }

        //attachments
        if(!$data["invoice"]) {
            $this->logger->error('MailerController::sendMailForSuccessCheckout -> Invalid input PDF filepath. UserID: '.$user->id.", Data: ".$data["invoice"]);
            $this->_sendJsonResponse(500);
        }

        //set receiver email & tags
        $to   = $this->mailer_conf["data_email"];
        $tags = ['checkout'];

        //append attachment
        $attachments = null;
        if($data["invoice"]) {
            $attachments = array([
                'content' => base64_encode($data["invoice"]),
                'type'    => "application/pdf",
                'name'    => $this->config->app->namespace."_".$checkout->buy_order."_".date('d-m-Y').'.pdf'
            ]);
        }

        //sends async email
        return $this->_sendMessage($html_raw, $subject, $to, $tags, $attachments);
    }

    /**
     * Async Handler - Sends mail for ticket transfer
     * @param array $data - An array with sending data
     * @return json response
     */
    public function sendMailForTicketTransfer($data)
    {
        //get user, event & transfer ticket
        $user     = $data["user"];
        $event    = $data["event"];
        $ticket   = $data["ticket"];
        $receiver = $data["receiver"];

        //set rendered view
        $this->mailer_conf["data_receiver"] = $receiver;
        $this->mailer_conf["data_email"]    = $receiver->email;
        $this->mailer_conf["data_user"]     = $user;
        $this->mailer_conf["data_event"]    = $event;
        $this->mailer_conf["data_ticket"]   = $ticket;
        $this->mailer_conf["data_url"]      = $data["url"];

        //get HTML
        $html_raw = $this->_getInlineStyledHtml("ticketTransfer", $this->mailer_conf);
        //set message properties
        $subject = $this->trans->_($user->first_name." ".$user->last_name." te ha transferido una entrada para ".$event->name);
        $to      = $this->mailer_conf["data_email"];
        $tags    = ['ticket', 'transfer'];
        //sends async email
        return $this->_sendMessage($html_raw, $subject, $to, $tags);
    }

    /**
     * Async Handler - Sends mail for ticket transfer success
     * @param array $data An array with the transfer & receiver_data
     * @return json response
     */
    public function sendMailForTicketTransferSuccess($data)
    {
        //get user & event
        $user     = $data["user"];
        $checkout = $data["checkout"];

        if(!$checkout || !$checkout->event)
            throw new Exception("MailerController::sendMailForTicketTransferSuccess -> a checkout & event object are required");

        //set rendered view
        $this->mailer_conf["data_user"]     = $user;
        $this->mailer_conf["data_email"]    = $user->email;
        $this->mailer_conf["data_event"]    = $checkout->event;
        $this->mailer_conf["data_ticket"]   = $checkout->eventTicket;
        $this->mailer_conf["data_checkout"] = $checkout;

        //get HTML
        $html_raw = $this->_getInlineStyledHtml("ticketTransferSuccess", $this->mailer_conf);
        //set message properties
        $subject = $this->trans->_("Transferencia de entrada exitosa");
        $to      = $this->mailer_conf["data_email"];
        $tags    = ['ticket', 'transfer', 'success'];

        //attachments
        $attachments = null;
        if($data["invoice"]) {
            //append attachment
            $attachments = array([
                'content' => base64_encode($data["invoice"]),
                'type'    => "application/pdf",
                'name'    => $this->config->app->namespace."_".$checkout->buy_order."_".date('d-m-Y').'.pdf'
            ]);
        }

        //sends async email
        return $this->_sendMessage($html_raw, $subject, $to, $tags, $attachments);
    }

    /**
     * Async Handler - Sends mail alert warnings for deauthorized facebook post action
     * @param int $user_id
     * @param array $data - An array with sending data
     * @return json response
     */
    public function sendMailForFacebookDeauth($data)
    {
        $user = $data["user"];

        //get user, event & transfer ticket
        $this->mailer_conf["data_email"]   = $user->email;
        $this->mailer_conf["data_user"]    = $data["user"];
        $this->mailer_conf["data_tickets"] = $data["tickets"];
        $this->mailer_conf["data_url"]     = $this->_baseUrl('account/profile');

        //get HTML
        $html_raw = $this->_getInlineStyledHtml("facebookDeauth", $this->mailer_conf);
        //set message properties
        $subject = $this->trans->_("Desvinculación de Facebook");
        $to      = $this->mailer_conf["data_email"];
        $tags    = ['facebook', 'deauthorize'];
        //sends async email
        return $this->_sendMessage($html_raw, $subject, $to, $tags);
    }
}
