<?php
/**
 * TestController: handles testing pages
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

class TestController extends SessionController
{
    /**
     * View - Test
     */
    public function indexAction()
    {
        //disable robots
        $this->view->setVar("html_disallow_robots", true);

        //load js modules
        $this->_loadJsModules([
            "test" => null
        ]);
    }
}
