<?php
/**
 * EventTicketAsset: all events operations
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//imports
use CrazyCake\Models\BaseResultset;

class EventTicketAssetController extends SessionController
{
    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        parent::onConstruct();

        //msg keys
        $this->MSGS = [
            "ASSIGNMENT_FAILED" => $this->trans->_("Ha ocurrido algo inesperado. Porfavor %a_open%comunícate aquí%a_close% con nuestro equipo.",
                                                    ["a_open" => '<a href="javascript:core.redirectTo(\'contact\');">', "a_close" => '</a>'])
        ];
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Listener - Event on success checkout
     * LOGIC:
     * 1) Crates userTicketsAssets records
     * 2) Substract item quantity
     * @param  int $user_id The user ID
     * @param  object $checkout The checkout object
     * @return checkout object
     */
    public function onSuccessCheckout($user_id = 0, &$checkout)
    {
        //1) asociar el usuario al evento (UserEventTicket, UserEventAsset)
        $task_assign = $this->_assignAssetsForUser($user_id, $checkout);

        if(isset($task_assign->error))
            throw new Exception($task_assign->error);

        //set user tickets IDS on checkout object
        $checkout->newObjectIds = $task_assign->newObjectIds;

        $this->logger->debug(">>".json_encode($checkout));
        //2) substract checkout items quantity
        UserCheckoutObject::substractStock($checkout->objects);

        if(empty($user_id))
            return $checkout;

        return $checkout;
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Assign assets to user ticket
     * @param int $user_id The user id
     * @param object $checkout The checkout objects
     * @return object $response
     */
    private function _assignAssetsForUser($user_id, $checkout)
    {
        //create response
        $response = new \stdClass();

        //validates checkout
        if(!isset($checkout->objects) || empty($checkout->objects)) {
            $this->logger->error("EventTicketAsset::_assignAssetsForUser -> Received empty checkout objects. Checkout: ".json_encode($checkout));
            throw new Exception($this->MSGS['ASSIGNMENT_FAILED']);
        }

        //get user ticket id
        $userTicket = UserEventTicket::findFirst("id = '".$checkout->categories[1]."' AND user_id = '$user_id'");

        if(!$userTicket) {
            $this->logger->error("EventTicketAsset::_assignAssetsForUser -> Received an invalid userTicket Checkout: ".json_encode($checkout));
            throw new Exception($this->MSGS['ASSIGNMENT_FAILED']);
        }

        //user tickets array
        $savedObjects = [];

        //1) Association between user & object
        try {

            //+begin db transaction
            $this->db->begin();

            //loop through objects
            foreach ($checkout->objects as $obj) {

                //DB SAVE ACTION: associates object
                if($obj->object_class != "EventTicketAsset")
                    continue;

                //foreach quantity creates an object and push to userTickets
                for ($i = 0; $i < $obj->quantity; $i++) {
                    array_push($savedObjects, $this->_newAssetForUserTicket($userTicket, $obj));
                }
            }

            //+commit db actions
            $this->db->commit();
        }
        catch (Exception $e) {
            //rollback action
            $this->db->rollback();
            //set and send response
            $response->error = $e->getMessage();
            return $response;
        }

        //set userTickets arrays
        $response->newObjectIds = BaseResultset::getIdsArray($savedObjects, "id");
        //var_dump($response, $savedObjects);exit;
        //return response
        return $response;
    }

    /**
     * Creates relation user-event-ticket => event-ticket-asset
     * Called by reflection
     * @param object $object The Checkout Object
     * @param object $userTicket The user ticket
     * @return ORM object
     */
    private function _newAssetForUserTicket($userTicket, $object)
    {
        //create user-event-ticket record (qr code is generated automatically)
        $props = [
            "user_event_ticket_id"  => $userTicket->id,
            "event_ticket_asset_id" => $object->object_id
        ];

        $userTicketAsset = new UserEventTicketAsset();

        if(!$userTicketAsset->save($props)) {
            throw new Exception($this->MSGS['ASSIGNMENT_FAILED']);
        }

        return $userTicketAsset;
    }
}
