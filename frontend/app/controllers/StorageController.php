<?php
/**
 * StorageController: handles file storage, including QR & PDF files
 * @todo Build a trait from this controller
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//CrazyCake Traits
use CrazyCake\Tickets\TicketManager;

class StorageController extends SessionController
{
    /* traits */
    use TicketManager;

    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        parent::onConstruct();

        //set configurations
        $this->initTicketManager([
            //resources
            "allowed_resources_types"  => ['png', 'pdf'],
            "ticket_pdf_template_view" => "invoices/mainLayout",
            "local_temp_path"          => PUBLIC_PATH."uploads/storage/",
            "image_fallback_uri"       => PUBLIC_PATH.'images/icons/icon-image-fallback.png',
            //qr settings
            "qr_settings" => [
                "background_color" => "ffffff",              //Background-color
                "frame_class"      => "QrTagFrame3",         //Frame class (QrTagFrameSquare)
                "frame_color"      => "000000",              //Frame color (85C11E)
                "dot_frame_class"  => "QrTagFrameDotSquare", //Dot frame class (QrTagFrameDotSquare, QrTagFrameDot15)
                "dot_frame_color"  => "000000",              //Dot frame color (85C11E),
                "dot_shape_class"  => "QrTagDotSquare",      //QR Dot Shape Class (QrTagDotSquare)
                "dot_shape_color"  => "000000",              //QR Dot shape color (222222)
                "dot_shape_size"   => 14,                    //QR Dot size (14)
                "embed_logo"       => PUBLIC_PATH."images/logos/logo-square-dark-small.png" //An embed image
            ],
            //functions
            "getUserTicketFunction" => function($code, $user_id) {
                //this anonymous function is used to get a ticket object
                return UserEventTicket::getByCodeAndUserId($code, $user_id);
            },
            "getObjectsForInvoiceFunction" => function($user_id, $checkout) {

                //NOTE: returns only one object type
                if(in_array("EventTicket", $checkout->objectsClasses))
                    $objects = UserEventTicket::getCollectionForUI($user_id, null, false, $checkout->newObjectIds);
                else
                    $objects = UserEventTicketAsset::getCollectionForUI($checkout->newObjectIds);
                //print_r($checkout->newObjectIds);exit;

                return $objects;
            }
        ]);
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Get Ticket, outputs a binary image
     * @param  string $code The ticket code
     */
    public function ticketAction($code)
    {
        //handle response, dispatch to auth/logout
        $this->_checkUserIsLoggedIn(true);

        //get ticket (png type as default)
        if(!$this->getTicketQR($this->user_session['id'], $code)) {
            $this->view->disable();
            //return false;
        }
    }

    /**
     * Get invoice from a given event_id
     * @param  string $ehash
     */
    public function invoiceAction($event_id_hashed = "")
    {
        //handle response, dispatch to auth/logout
        $this->_checkUserIsLoggedIn(true);

        try {
            //check param data
            if(empty($event_id_hashed))
                $this->_redirectToNotFound();

            //get event
            $user_id  = $this->user_session['id'];
            $event_id = $this->cryptify->decryptHashId($event_id_hashed);
            $event    = Event::getById($event_id);

            if(empty($event))
                throw new Exception("Event not found, event_id: ".$event_id);

            //get tickets for event
            $tickets = UserEventTicket::getCollection($user_id, $event_id);

            if(empty($tickets))
                throw new Exception("No tickets available for user: ".$user_id." & eventId: ".$event_id);

            //create the checkout object
            $checkout = (object)[
                "buy_order"      => uniqid(),
                "type"           => "action",
                "objectsClasses" => ["EventTicket"],
                "newObjectIds"   => $tickets->toIdsArray('id'),
                "event"          => $event,
                "comment"        => $this->trans->_('Entradas vigentes al día %date%.', ["date" => date('d-m-Y')])
            ];

            //get tickets as one invoice
            $result = $this->generateInvoice($user_id, $checkout, true);

            if(isset($result->error))
                throw new Exception("Failed generating OTF ticket invoices, userID: $user_id, eventId: $event_id");

            //send response
            $this->_sendFileToBuffer($result->binary, self::$MIME_TYPES['pdf']);
        }
        catch(Exception $e) {
            $this->logger->error("StorageController::invoices -> Failed requesting invoice, err: ".$e->getMessage());
            $this->_redirectToNotFound();
        }
    }
}
