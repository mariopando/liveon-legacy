<?php
/**
 * Event ticket transfer controller.
 * This class manages transfer ticket between users.
 * Files are uploaded automatically to S3 in a defined URI.
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

use CrazyCake\Models\BaseResultset;

class EventTicketTransferController extends SessionController
{
    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        parent::onConstruct();

        //msg keys
        $this->MSGS = [
            "AUTH_NEEDED" => $this->trans->_("Ingresa a tu cuenta %app_name% para activar la transferencia.", ['app_name' => '<span class="marked">'.$this->config->app->name.'</span>']),
            //errors
            "ERROR_RECEIVER_EMAIL"   => $this->trans->_("Porfavor ingresa un correo de envío válido."),
            "ERROR_TRANSFER_EXISTS"  => $this->trans->_("Ya se ha enviado una orden de transferencia a %email%.", ["email" => "{email}"]),
            "ERROR_FORM_INPUTS"      => $this->trans->_("Porfavor ingresa todos los campos requeridos."),
            "ERROR_SELF_TRANSFER"    => $this->trans->_("No puedes transferir una entrada que ya está asociada a tu cuenta."),
            "ERROR_TRANSFER_EXPIRED" => $this->trans->_("Este enlace ha caducado y ya no se encuentra disponible."),
            //transfer related
            "CANCEL_TRANSFER_SUCCESS" => $this->trans->_("Se ha cancelado esta transferencia exitosamente."),
            "TRANSFER_SENT"           => $this->trans->_("Espera que %name% acepte la transferencia de esta entrada.", ['name' => '{name}']),
            "TRANSFER_COMPLETE"       => $this->trans->_("¡Se ha transferido una entrada a tu cuenta!"),
            "INVOICE_COMMENT_RECEIVER"   => $this->trans->_('%name% te transfirió la entrada %ticket%.', ["name" => "{name}", "ticket" => "{ticket}"]),
            "INVOICE_COMMENT_TRANSFERER" => $this->trans->_('%name% aceptó la transferencia de la entrada %ticket%.', ["name" => "{name}", "ticket" => "{ticket}"])
        ];
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Ajax - creates a new ticket transfer record and notifies receiver user
     */
    public function newOrderAction()
    {
        //make sure is ajax request
        $this->_onlyAjax();
        //handle response, dispatch to auth/logout
        $this->_checkUserIsLoggedIn(true);

        //get form data
        $data = $this->_handleRequestParams([
            "@code"          => "string",
            "@receiverName"  => "string",
            "@receiverEmail" => "string"
        ]);

        try {

            if(empty($data["code"]) || empty($data["receiverName"]) || empty($data["receiverEmail"]))
                throw new Exception($this->MSGS["ERROR_FORM_INPUTS"]);

            //validates receiver email
            if(!isset($data["receiverEmail"]) || !filter_var($data['receiverEmail'], FILTER_VALIDATE_EMAIL))
                throw new Exception($this->MSGS["ERROR_RECEIVER_EMAIL"]);

            //lower case email
            $data["receiverEmail"] = strtolower($data["receiverEmail"]);
            //capitalize name
            $data["receiverName"] = mb_convert_case($data["receiverName"], MB_CASE_TITLE, 'UTF-8');

            //get user id from session
            $user_id = $this->user_session["id"];
            $user    = User::getById($user_id);
            //make sure ticket belongs to user
            $userTicket = UserEventTicket::getByCodeAndUserId($data["code"], $user_id);

            if(!$userTicket)
                $this->_sendJsonResponse(400);

            //get event
            $ticket = $userTicket->eventTicket;
            $event  = Event::getById($ticket->event_id);

            //only open events allows ticket transfers
            if(!$event || ($event->state != "open" && $event->state != "invisible"))
                $this->_sendJsonResponse(400);

            //check if a pending transfer was already created
            $transfer = UserEventTicketTransfer::getByUserTicket($user_id, $userTicket->id);
            if($transfer)
                throw new Exception(str_replace("{email}", $transfer->receiver_email, $this->MSGS["ERROR_TRANSFER_EXISTS"]));

            //save record
            $transfer = UserEventTicketTransfer::newOrder($userTicket, $data);

            if(!$transfer)
                $this->_sendJsonResponse(500);

            //set encoded URL
            $encrypted_data = $this->cryptify->encryptData(sha1($data['receiverEmail'])."#".$transfer->token);
            //send the email to receiver user
            $this->_sendMailMessage("sendMailForTicketTransfer", [
                 "user"     => $user,
                 "event"    => $event,
                 "ticket"   => $ticket,
                 "receiver" => (object)["name" => $data["receiverName"], "email" => $data["receiverEmail"]],
                 "url"      => $this->_baseUrl("event_ticket_transfer/transfer/".$encrypted_data)
             ]);

             $payload = [
                 "msg"      => str_replace("{name}", $data["receiverName"], $this->MSGS["TRANSFER_SENT"]),
                 "transfer" => $transfer->toArray(["id", "receiver_name", "receiver_email"]),
             ];
             $this->_sendJsonResponse(200, array_merge($payload, $data));
        }
        catch (Exception $e) {
            //sends an error message
            $this->_sendJsonResponse(200, $e->getMessage(), 'alert');
        }
    }

    /**
     * Handler - process a link transfer, must follow checkout flux
     * This method is triggered by the receiver user. Struct:[hash1,hash2]
     * @param string $encrypted_data The param encrypted data string
     */
    public function transferAction($encrypted_data = null)
    {
        try {
            if (is_null($encrypted_data))
                throw new Exception("sent input null encrypted_data");

            $data     = $this->cryptify->decryptData($encrypted_data, "#");
            $transfer = UserEventTicketTransfer::getByToken($data[1]);

            //no transfer found?
            if(!$transfer || $transfer->state != "pending")
                throw new Exception("No pending transfer found for: \n".json_encode($data));

            //handle logged out users
            $this->_handleLoggedOutUserForTransfer($transfer->receiver_email);

            //check that the transfer order is not processed by me
            if($this->user_session["id"] == $transfer->user_id) {
                $error_message = $this->MSGS["ERROR_SELF_TRANSFER"];
                throw new Exception("Session user_id ".$this->user_session["id"]." is the same with transfer user_id: \n".json_encode($transfer->toArray()));
            }

            //get user ID and user object
            $user_id = $this->user_session["id"];
            $user    = User::getById($user_id);

            //OK proceed to transfer
            $transfererUser   = User::getById($transfer->user_id);
            $transferedTicket = UserEventTicket::getById($transfer->user_event_ticket_id);

            $this->logger->debug("EventTicketControllers::transfer -> new request ".$transfererUser->id." [".$transferedTicket->code."] to ".$user_id);

            //validate user tickets
            if(!$transferedTicket)
                throw new Exception("weird error, user event ticket not found: \n".json_encode($transfer->toArray()));

            //get event
            $event = $transferedTicket->eventTicket->event;

            //check event state
            if(!$event || ($event->state != "open" && $event->state != "invisible"))
                throw new Exception("Event (id:".$event->id.") not found or is not opened. Transfer: \n".json_encode($transfer->toArray()));

            //1)Move S3 resources to active session user folder
            $storage = new StorageController();
            $ticket_moved = $storage->moveTicketQR($transfer->user_id, $transferedTicket->code, $user_id);

            if(!$ticket_moved)
                throw new Exception("failed moving ticket in s3 bucket: \n".json_encode($transfer->toArray()));

            //2)SAVE, Update user ticket to active session user & Transfer state
            //$this->_logDatabaseStatements();

            if(!UserEventTicket::updateOwner($transferedTicket->id, $user_id))
                throw new Exception("Error updating ticket owner, ticketID: ".$transferedTicket->id." to userID: ".$user_id);

            //transfer state
            $transfer->update(["state" => "success"]);

            $this->logger->debug("EventTicketControllers::transfer -> transfer complete [".$transfer->id."] data: ".json_encode($transferedTicket->toArray()));

            //3)Generates invoices and sends emails to both Users
            $this->_generateTransferInvoices($event, $user, $transfererUser, $transferedTicket);

            //4)Set flash message and redirect
            $this->flash->success($this->MSGS["TRANSFER_COMPLETE"]);
            $this->_redirectTo("account");
        }
        catch (Exception $e) {

            if(!isset($data)) $data = null;

            $this->logger->error("EventTicketTransferController::transferAction -> Error in transfer, data: ".json_encode($data).".\nTrace: ".$e->getMessage());
            //set error message
            $this->view->setVar("error_message", isset($error_message) ? $error_message : $this->MSGS["ERROR_TRANSFER_EXPIRED"]);
            //dispatch
            $this->dispatcher->forward(["controller" => "error", "action" => "expired"]);
        }
    }

    /**
     * Ajax - cancels a pending ticket transfer record and notifies receiver user
     */
    public function cancelTransferOrderAction()
    {
        //make sure is ajax request
        $this->_onlyAjax();
        //handle response, dispatch to auth/logout
        $this->_checkUserIsLoggedIn(true);

        //get form data
        $data = $this->_handleRequestParams([
            "transfer_id" => "int"
        ]);
        //print_r($data);exit;
        //get user ID and user object
        $user_id = $this->user_session["id"];

        try {

            //get transfer object
            $ormObject = UserEventTicketTransfer::getByUserIdAndRecordId($user_id, $data["transfer_id"]);

            if(!$ormObject)
                $this->_sendJsonResponse(400);

            //delete record
            $ormObject->delete();
            //send success response
            $this->_sendJsonResponse(200, $this->MSGS["CANCEL_TRANSFER_SUCCESS"]);
        }
        catch (Exception $e) {
            //sends an error message
            $this->_sendJsonResponse(200, $e->getMessage(), 'alert');
        }
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Generates transfers invoices
     * @param  object $event            The event ORM object
     * @param  object $user             The user ORM object
     * @param  object $transfererUser   The transferer user ORM object
     * @param  object $transferedTicket The transferer ticket ORM object
     */
    private function _generateTransferInvoices($event, $user, $transfererUser, $transferedTicket)
    {
        //get user tickets updated for event (todos hasta la fecha)
        $userTickets = UserEventTicket::getCollection($user->id, $event->id);
        //set transferer user name
        $transfererUserName = $transfererUser->first_name." ".$transfererUser->last_name;
        //get all transferedTicket properties
        $transferedTicketProps = EventTicket::getById($transferedTicket->ticket_id);

        //3)Generate invoice to active session user (receiver), author is the opposite agent.
        $receiverUserName = $user->first_name." ".$user->last_name;
        $comment = str_replace(["{name}","{ticket}"], [$transfererUserName, $transferedTicket->code], $this->MSGS["INVOICE_COMMENT_RECEIVER"]);
        $author  = (object)["type" => "transferer", "props" => $transfererUser];
        //new invoice transfer (receiver)
        $this->_newTransferInvoice($event, $user, $transferedTicketProps, $userTickets, $author, $comment);

        //4)Generate invoice to transferer user (transferer)
        $comment = str_replace(["{name}","{ticket}"], [$receiverUserName, $transferedTicket->code], $this->MSGS["INVOICE_COMMENT_TRANSFERER"]);
        $author  = (object)["type" => "receiver", "props" => $user];
        //+new invoice transfer (transferer)
        $transfererUserTickets = UserEventTicket::getCollection($transfererUser->id, $event->id);
        $this->_newTransferInvoice($event, $transfererUser, $transferedTicketProps, $transfererUserTickets, $author, $comment);
    }
    /**
     * Generates a new transfer invoice
     * @param  object $event      The event object
     * @param  object $user       The user object
     * @param  object $ticket     The transfered Ticket (EventTicket object)
     * @param  array $userTickets An ORM array of user tickets
     * @param  string $author     The user action author (receiver or transferer)
     * @param  string $comment    A comment for the invoice
     */
    private function _newTransferInvoice($event, $user, $ticket, $userTickets, $author, $comment)
    {
        $userTicketsIds = $userTickets ? BaseResultset::getIdsArray($userTickets, "id") : false;

        //creates the checkout object
        $checkout = (object)[
            "buy_order"      => uniqid(),
            "type"           => "action",
            "objectsClasses" => ["EventTicket"],
            "newObjectIds"   => $userTicketsIds,
            "event"          => $event,
            "comment"        => $comment
        ];

        //TODO: para las transferencias incluir un futuro el real-time update con push.

        //Generates transfer invoice (exclude invoice for user that transfer all his tickets)
        $invoice = null;
        if($userTicketsIds) {

            //1) Generate Invoice
            $task_invoice = (new StorageController())->generateInvoice($user->id, $checkout, true);

            //check for errors
            if(isset($task_invoice->error))
                throw new Exception($task_invoice->error);
            else
                $invoice = $task_invoice->binary;
        }

        //append data for mail message
        $checkout->author      = $author;
        $checkout->userType    = ($author->type == "receiver") ? "transferer" : "receiver";
        $checkout->eventTicket = $ticket;

        //Send email for user
        $this->_sendMailMessage("sendMailForTicketTransferSuccess", [
            "user"     => $user,
            "checkout" => $checkout,
            "invoice"  => $invoice
        ]);
    }

    /**
     * Handle logged Out user that access link transfer
     * @param strings $receiver_email The receiver email
     */
    private function _handleLoggedOutUserForTransfer($receiver_email)
    {
        //check user is logged in
        if($this->_checkUserIsLoggedIn())
            return;

        //set a flash message for non authenticated users
        $this->flash->notice($this->MSGS["AUTH_NEEDED"]);
        //if not logged In, set this URI to redirected after logIn
        $this->_setSessionRedirectionOnLoggedIn();
        //add receiverEmail for signUp field auto-completion
        $this->_addSessionObject("signup_session", $receiver_email, "email");
        //handle response, dispatch to auth/logout
        $this->_checkUserIsLoggedIn(true);
    }
}
