<?php
/**
 * SessionController: parent class for main account based controllers
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

 use CrazyCake\Account\AccountSession;

class SessionController extends CoreController
{
    /* traits */
    use AccountSession;

    /**
     * @var array
     */
    private $UI_DATA;

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Set user Session as logged in
     * @param object $user The user object is passed
     */
    protected function setUserSessionAsLoggedIn($user)
    {
        //check if user has facebook accout
        $user_fb_id = $user->userFacebook ? $user->userFacebook->id : false;

        //set session data (id & auth props are set by defult)
        return [
            "id_hashed"    => $user->id_hashed,
            "email"        => $user->email,
            "first_name"   => $user->first_name,
            "last_name"    => $user->last_name,
            "last_login"   => $user->last_login,
            "account_flag" => $user->account_flag,
            "fb_id"        => $user_fb_id
        ];
    }

    /**
     * Get logged in user session data
     * @param array $session The session array
     * @return boolean
     */
    protected function getUserSessionData($session)
    {
        //account UI data
        $ui_paths = [
            "fb_profile"       => $this->client->protocol.FacebookController::$FB_USER_IMAGE_URI,
            "local_profile"    => $this->_staticUrl("uploads/account_images/<hash>.png"),
            "fallback_profile" => $this->_staticUrl("images/icons/icon-user-fallback.png?v=1.0")
        ];

        //return unmodified session data
        if (isset($session['profile_img_path']))
            return $session;

        //set fallback as default
        $image_path = $ui_paths['fallback_profile'];

        //check if user has a facebook id
        if ($session['fb_id']) {
            $fb_img_path = str_replace("<fb_id>", $session['fb_id'], $ui_paths['fb_profile']);
            $image_path  = str_replace("type=<size>", 'width=130&height=130', $fb_img_path);
        }

        //local or cdn image path (for non-social account users)
        if (is_file($ui_paths['local_profile']))
            $image_path = str_replace("<hash>", $session["id_hashed"], $ui_paths['local_profile']);

        //set image path
        $session['profile_img_path'] = $image_path;

        //return modified session properties
        return $session;
    }
}
