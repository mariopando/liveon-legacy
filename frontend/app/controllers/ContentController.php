<?php
/**
 * ContentsController: handles Content pages
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

class ContentController extends SessionController
{
    /**
     * View - Terms
     */
    public function termsAction()
    {
        //...
    }

    /**
     * View - Help
     */
    public function faqAction()
    {
        //...
    }

    /**
    * View - Contact page
    */
   public function contactAction()
   {
       //force centered content
       $this->view->setVar("medium_content", true);

       //load js modules
       $this->_loadJsModules([
           "content" => null
       ]);
   }
}
