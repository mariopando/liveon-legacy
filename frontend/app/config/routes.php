<?php
/**
 * Phalcon App Routes files
 * 404 error page is managed by Route 404 Plugin automatically
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

return function($router) {

	//default route
	$router->add("/", [
	    "controller" => "index",
	    "action"     => "index"
	]);

	//contact
	$router->add("/contact", [
	    "controller" => "content",
	    "action"     => "contact"
	]);

	//login & register
	$router->add("/signIn", [
	    "controller" => "auth",
	    "action"     => "signIn"
	]);
	$router->add("/signUp", [
	    "controller" => "auth",
	    "action"     => "signUp"
	]);

	//events
	$router->add("/event/:params", [
	    "controller" => "event",
	    "action"     => "index",
	    "params"	 => 1
	]);

	//tickets
	$router->add("/account/tickets/:params", [
	    "controller" => "event_ticket",
	    "action"     => "tickets",
	    "params"	 => 1
	]);

	//invitations
	$router->add("/account/invitations", [
	    "controller" => "event_invitation",
	    "action"     => "invitations"
	]);
	//confirm
	$router->add("/account/invitations/confirm/:params", [
	    "controller" => "event_invitation",
	    "action"     => "confirm",
	    "params"	 => 1
	]);
};
