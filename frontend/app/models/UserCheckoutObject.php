<?php

//imports
use CrazyCake\Checkout\BaseUserCheckoutObject;

class UserCheckoutObject extends BaseUserCheckoutObject
{
    /**
     * late static binding, overrides parent who function
     * @link http://php.net/manual/en/language.oop5.late-static-bindings.php
     * @return string
     */
    public static function who()
    {
        return __CLASS__;
    }
}
