<?php

//other imports
use CrazyCake\Facebook\BaseUserFacebook;

class UserFacebookPage extends BaseUserFacebook
{
    /* extended properties */

    /**
     * @var int Facebook APP ID
     */
    public $app_id;
}
