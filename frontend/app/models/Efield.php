<?php
//imports
use Phalcon\Mvc\Model\Validator\InclusionIn;
//other imports
use CrazyCake\Phalcon\AppLoader;
use CrazyCake\Models\Base;

class Efield extends Base
{
    /* properties */

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $namespace;

    /**
     * @var string
     */
    public $small_print;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $data;

    /**
     * @var string
     */
    public $_ext;

    /* inclusion vars */

    /**
     * @static
     * @var array
     */
    static $TYPES = ['checkbox', 'open', 'select', 'phone'];

    /**
     * Initilizer
     */
    public function initialize()
    {
        //model relations
        $this->hasMany("id", "EventField", "efield_id");
    }

    /**
     * After Fetch Event
     */
    public function afterFetch()
    {
        //shared URL with API
        $this->_ext["image_url"] = AppLoader::getModuleUrl("frontend", "images/app/efield/".$this->namespace.".png", "static");
    }

    /**
     * Validation Event
     */
    public function validation()
    {
        $this->validate(new InclusionIn([
            "field"   => "types",
            "domain"  => self::$TYPES,
            "message" => 'Invalid type. Types supported: '.implode(", ", self::$TYPES)
        ]));

        //check validations
        if ($this->validationHasFailed() == true)
            return false;
    }
}
