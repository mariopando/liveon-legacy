<?php

//other imports
use CrazyCake\Tickets\BaseTicket;
use CrazyCake\Helpers\Forms;

class EventTicket extends BaseTicket
{
    /* extended properties */

    /**
     * @var int
     */
    public $event_id;

    /**
     * Initilizer
     */
    public function initialize()
    {
        parent::initialize();

        //model relations
        $this->hasOne("event_id",  "Event", "id");
    }

    /**
     * After Fetch Event
     */
    public function afterFetch()
    {
        parent::afterFetch();
        
        //format starting price
        if(isset($this->starting_price) && isset($this->currency))
            $this->_ext["st_price_formatted"] = Forms::formatPrice($this->starting_price, $this->currency);
    }

    /** ------------------------------------------ § ------------------------------------------------- **/

    /**
     * required late static binding, overrides parent who function
     * @link http://php.net/manual/en/language.oop5.late-static-bindings.php
     * @return string
     */
    public static function who() {
        return __CLASS__;
    }

    /**
     * Find Ticket by ID and Event ID
     * @static
     * @param int $ticket_id
     * @param int $event_id
     * @return EventTicket
     */
    public static function getByIdAndEventId($ticket_id, $event_id)
    {
        return self::findFirst([
            "id = ?1 AND event_id = ?2",
            "bind" => [1 => $ticket_id, 2 => $event_id]
        ]);
    }

    /**
     * Get events tickets with main info for UI render (RAW SQL)
     * @static
     * @param int $number
     * @param string $condition
     * @param int $offset
     * @param string $order Order by Field
     * @param boolean $group The group by
     * @return EventTicket
     */
    public static function getCollectionForUI($number = 1, $offset = 0, $conditions = "et.id > 0", $order = "et.price ASC", $group = true)
    {
        //exclude event name?
        if(!empty($conditions) && is_array($conditions))
            $conditions = implode(" AND ", $conditions);

        //group by?
        $group_by = $group ? "GROUP BY e.id" : "";

        //print_r($sql);exit;
        return self::getByQuery(
            "SELECT et.id, et.event_id, et.name, et.price, et.currency, et.created_at, et.state, et.small_print,
                    (SELECT MIN(price) FROM event_ticket WHERE event_id = e.id) AS starting_price
            FROM event_ticket et
            INNER JOIN event e
                ON (e.id = et.event_id)
            WHERE $conditions
            $group_by
            ORDER BY $order
            LIMIT $offset, $number",
            //bindings
            null,
            //className
            "EventTicket"
        );
    }
}
