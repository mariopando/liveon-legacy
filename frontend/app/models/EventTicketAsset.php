<?php

//other imports
use CrazyCake\Tickets\BaseTicketAsset;

class EventTicketAsset extends BaseTicketAsset
{
    /** properties **/

    /**
    * @var int
    */
    public $easset_id;

    /**
     * Initilizer
     */
    public function initialize()
    {
        //model relations
        $this->hasOne("ticket_id", "EventTicket", "id");
        $this->hasOne("easset_id", "Easset", "id");
    }

    /**
     * After Fetch Event
     */
    public function afterFetch()
    {
        parent::afterFetch();

        $this->_ext["name"] = $this->easset->name;
    }

    /** ------------------------------------------ § ------------------------------------------------- **/

    /**
     * required late static binding, overrides parent who function
     * @link http://php.net/manual/en/language.oop5.late-static-bindings.php
     * @return string
     */
    public static function who() {
        return __CLASS__;
    }

    /**
     * Get assets for event
     * @param int $event_id The event id
     * @param string $state The eventAsset state
     */
    public static function getByEventId($event_id = 0, $state = '')
    {
        //filter
        if(!empty($state))
            $state = " AND eta.state = '$state'";

        return self::getByQuery(
            "SELECT eta.easset_id, eta.price, eta.currency, eta.default_quantity,
                    ea.namespace, ea.small_print
                FROM event_ticket_asset eta
                    INNER JOIN easset ea ON (ea.id = eta.easset_id)
                    INNER JOIN event_ticket et ON (et.id = eta.ticket_id)
                WHERE et.event_id = ?
                    $state
                GROUP BY ea.id
                ORDER BY eta.id ASC",
            //binds
            [$event_id],
            //className
            "EventTicketAsset"
        );
    }

    /**
     * Get assets with object properties (RAW SQL)
     * Excludes assets owned by user
     * @static
     * @param object $ticket_id The EventTicket ID
     * @param object $user_ticket_id The UserEventTicket ID
     * @param string $order Order by Field
     * @return EventTicketAsset
     */
    public static function getCollectionForUI($ticket_id, $user_ticket_id = 0, $order = "eta.price ASC")
    {
        $sql = "SELECT eta.id, eta.ticket_id, eta.easset_id, ea.small_print,
                        eta.price, eta.currency, eta.created_at, eta.state, eta._ext,
                        (SELECT COUNT(id)
                           FROM user_event_ticket_asset ueta
                            WHERE user_event_ticket_id = ?
                                AND event_ticket_asset_id = eta.id
                        ) AS assets_owned
                FROM event_ticket_asset eta
                    INNER JOIN easset ea ON (ea.id = eta.easset_id)
                WHERE eta.ticket_id = ?
                GROUP BY eta.id
                HAVING assets_owned = 0
                ORDER BY ".$order;
        //var_dump($sql, $ticket_id, $user_ticket_id);exit;

        return self::getByQuery(
            $sql,
            //binds
            [$user_ticket_id, $ticket_id],
            //className
            "EventTicketAsset"
        );
    }

    /**
     * Parse a ORM resulset to Array. TODO: override this later with ORM solution.
     * @override
     * @param  resulset $assets The resulset
     * @return array
     */
    public static function customReduce($assets = array())
    {
        $assetsMerge = [];

        foreach ($assets as $obj) {

            $tempObj = $obj->toArray();

            $assetObj = Easset::findFirstById($tempObj["easset_id"]);

            //append props
            $tempObj["name"]        = $assetObj->name;
            $tempObj["small_print"] = $assetObj->small_print;

            //unset unwanted props
            unset($tempObj["id"], $tempObj["easset_id"], $tempObj["ticket_id"]);

            array_push($assetsMerge, $tempObj);
        }

        return $assetsMerge;
    }
}
