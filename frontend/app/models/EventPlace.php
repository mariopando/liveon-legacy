<?php

//other imports
use CrazyCake\Models\Base;

class EventPlace extends Base
{
    /* properties */

    /**
     * @var int
     */
    public $event_id;

   /**
    * @var int
    */
    public $eplace_id;

    /**
     * Initilizer
     */
    public function initialize()
    {
        //model relations
        $this->hasOne("eplace_id", "Eplace", "id");
    }
}
