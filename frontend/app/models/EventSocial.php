<?php

//other imports
use CrazyCake\Models\Base;

class EventSocial extends Base
{
    /* properties */

    /**
     * @var int
     */
    public $event_id;

   /**
    * @var int
    */
    public $required;

    /**
     * Initilizer
     */
    public function initialize()
    {
        //model relations
        $this->hasOne("event_id", "Event", "id");
    }
}
