<?php

//other imports
use CrazyCake\Facebook\BaseGraphAction;
use CrazyCake\Helpers\Dates;

class EventFacebook extends BaseGraphAction
{
    /* properties */

    /**
     * @var int
     */
    public $event_id;

    /**
     * Initilizer
     */
    public function initialize()
    {
        parent::initialize();

        //model relations
        $this->hasOne("event_id", "Event", "id");
    }
}
