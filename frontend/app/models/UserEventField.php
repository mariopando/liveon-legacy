<?php

//other imports
use CrazyCake\Models\Base;

class UserEventField extends Base
{
    /* properties */

    /**
     * @var int
     */
    public $user_id;

    /**
     * @var int
     */
    public $efield_id;

    /**
     * @var string
     */
    public $value;

    /**
     * @var string
     */
    public $created_at;

    /**
     * Initilizer
     */
    public function initialize()
    {
        //model relations
        $this->hasOne("user_id", "User", "id");
        $this->hasOne("efield_id", "Efield", "id");
    }

    /**
     * Before Validation Event [onCreate]
     */
    public function beforeValidationOnCreate()
    {
        //set date
        $this->created_at = date('Y-m-d H:i:s');
    }

    /** ----------------------------------------- § ------------------------------------------------  **/

    /**
     * Get all user efield data
     * @param int $user_id - The user ID
     */
    public static function getCollection($user_id = 0)
    {
        //print_r($sql);exit;
        $result = self::getByQuery(
            "SELECT f.name, f.namespace, f.small_print, f.type, f.data, f._ext,
                ef.priority, ef.required,
                uef.value, uef.created_at
            FROM user_event_field uef
                INNER JOIN event_field ef ON ef.efield_id = uef.efield_id
                INNER JOIN efield f ON f.id = ef.efield_id
            WHERE uef.user_id = ?
            GROUP BY f.namespace",
            //bindings
            [$user_id],
            //className
            "Efield"
        );

        if(!$result)
            return [];

        //parse data
        $objects = [];
        foreach ($result as $obj) {

            $value    = $obj->value;
            $namspace = $obj->namespace;

            $objArray  = $obj->toArray(["name", "small_print", "type", "_ext"]);
            $objArray["priority"] = $obj->priority;
            $objArray["required"] = $obj->required;
            $objArray["value"] = $value;

            $objects[$namspace] = $objArray;
        }

        return $objects;
    }

    /**
     * Get all user efield data
     * @param int $user_id - The user ID
     * @param int $efield_id - The event field ID
     */
    public static function getByUserIdAndEfieldId($user_id = 0, $efield_id = 0)
    {
        $conditions = "user_id = ?1 AND efield_id = ?2";
        $parameters = [1 => $user_id, 2 => $efield_id];

        return self::findFirst([$conditions, "bind" => $parameters]);
    }

    /**
     * Saves data user-efield data
     * @static
     * @param int $user_id The user id
     * @param int $eventFieldsData The event fields data
     * @param array $data The data to save
     * @return boolean
     */
    public static function saveData($user_id, $eventFieldsData)
    {
        if(empty($eventFieldsData))
            return false;

        foreach ($eventFieldsData as $field) {

            try {

                //check if exists
                $object = self::getByUserIdAndEfieldId($user_id, $field->efield_id);

                //update object
                if($object) {
                    $object->value = $field->value;
                    $object->update();
                }
                //new object
                else {

                    $object = new self();
                    $object->save([
                        "user_id"   => $user_id,
                        "efield_id" => $field->id,
                        "value"     => $field->value
                    ]);
                }
            }
            catch(\Exception $e) {
                return $e;
            }
        }

        return true;
    }
}
