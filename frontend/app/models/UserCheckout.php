<?php

//imports
use CrazyCake\Checkout\BaseUserCheckout;

class UserCheckout extends BaseUserCheckout
{
    /** ------------------------------------------- § ------------------------------------------------ **/

    /**
     * late static binding, overrides parent who function
     * @link http://php.net/manual/en/language.oop5.late-static-bindings.php
     * @return string
     */
    public static function who()
    {
        return __CLASS__;
    }

    /**
     * User Checkout validation for MAX objects purchased
     * Se valida que el usuario no haya comprado un MAX de entradas
     * @param  user_id $user_id The user ID
     * @param  object $checkout The checkout object
     * @param  int $max_items Max item per user number
     * @return boolean
     */
    public static function maxObjectsPurchased($user_id = 0, $checkout = null, $max_items = 1)
    {
        //call reflection method
        $method = "_getPurchased".$checkout->objectsClasses[0];

        return self::$method($user_id, $checkout, $max_items);
    }

    /** ------------------------------------------- § ------------------------------------------------ **/

    /**
     * Get event tickets purchased
     * @param  int $user_id      The user id
     * @param  object $checkout  The obejct checkout
     * @param  int $max_items    Max number items
     * @return boolean
     */
    private static function _getPurchasedEventTicket($user_id, $checkout, $max_items)
    {
        $event = Event::findFirstByNamespace($checkout->categories[0]);

        if(!$event)
            return true;

        $objects = UserEventTicket::getCollection($user_id, $event->id);

        if(!$objects)
            return false;

        //event type special cases
        if($event->type == "free" || $event->type == "invitation") {

            if($objects->count() > 1)
                return true;
        }
        else if($event->type == "paid") {

            //calculates total
            $total = $objects->count() + $checkout->totalQ;

            if($total > $max_items)
                return true;
        }

        return false;
    }

    /**
     * Get event assets purchased
     * @param  int $user_id      The user id
     * @param  object $checkout  The obejct checkout
     * @param  int $max_items    Max number items
     * @return boolean
     */
    private static function _getPurchasedEventTicketAsset($user_id, $checkout, $max_items)
    {
        $purchased = false;

        //para cada elemento en el checkout
        foreach ($checkout->objects as $obj) {

            //get obejct id
            $ticketAsset = EventTicketAsset::getById($obj->id);

            if(!$ticketAsset)
                continue;

            //get assets linked to this userTicket
            $userTicketAsset = UserEventTicketAsset::getCollection($checkout->categories[1], $ticketAsset->id);

            if($userTicketAsset) {
                //s($userTicketAsset->toArray(), $checkout->categories[1], $ticketAsset->id);exit;
                $purchased = true;
                break;
            }
        }

        return $purchased;
    }
}
