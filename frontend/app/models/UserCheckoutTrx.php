<?php
/**
 * User Checkouts Transactions
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//other imports
use CrazyCake\Checkout\BaseUserCheckoutTrx;

class UserCheckoutTrx extends BaseUserCheckoutTrx
{
    /** ------------------------------------------- § ------------------------------------------------ **/

    /**
     * late static binding, overrides parent who function
     * @link http://php.net/manual/en/language.oop5.late-static-bindings.php
     * @return string
     */
    public static function who()
    {
        return __CLASS__;
    }
}
