<?php

//other imports
use CrazyCake\Models\Base;
//imports
use Phalcon\Mvc\Model\Validator\InclusionIn;

class UserEventTicketAsset extends Base
{
    /* properties */

    /**
     * @var int
     */
    public $user_event_ticket_id;

    /**
     * @var int
     */
    public $event_ticket_asset_id;

    /**
     * @var string
     */
    public $state;

    /**
     * @var string
     */
    public $created_at;

    /**
     * @static
     * @var array
     */
    static $STATES = ['ready', 'burned'];

    /**
     * Initilizer
     */
    public function initialize()
    {
        //model relations
        $this->hasOne("user_event_ticket_id",  "UserEventTicket", "id");
        $this->hasOne("event_ticket_asset_id",  "EventTicketAsset", "id");
    }

    /**
     * Before Validation Event [onCreate]
     */
    public function beforeValidationOnCreate()
    {
        $this->state      = 'ready';
        $this->created_at = date("Y-m-d H:i:s");
    }

    /**
     * Validation
     */
    public function validation()
    {
        $this->validate( new InclusionIn([
            "field"   => "state",
            "domain"  => self::$STATES,
            "message" => 'Invalid state. States supported: '.implode(", ", self::$STATES)
         ]));

        //check validations
        if ($this->validationHasFailed() == true)
            return false;
    }

    /** ------------------------------------------ § ------------------------------------------------- **/

    /**
     * required late static binding, overrides parent who function
     * @link http://php.net/manual/en/language.oop5.late-static-bindings.php
     * @return string
     */
    public static function who() {
        return __CLASS__;
    }

    /**
     * Get user event tickets assets for user ticket
     * @param  int $user_event_ticket_id The record ID
     * @param  int $event_ticket_asset_id The record ID
     * @return mixed [boolean|object]
     */
    public static function getCollection($user_event_ticket_id = 0, $event_ticket_asset_id = 0)
    {
        if(empty($event_ticket_asset_id)) {

            return self::find([
                "user_event_ticket_id = ?1",
                "bind" => [
                    1 => $user_event_ticket_id
                ]
            ]);
        }
        else {

            return self::findFirst([
                "user_event_ticket_id = ?1 AND event_ticket_asset_id = ?2",
                "bind" => [
                    1 => $user_event_ticket_id,
                    2 => $event_ticket_asset_id
                ]
            ]);
        }
    }


    /**
     * Get UserEventTicket assets
     * @param int $user_id The user id
     * @param array $record_ids The objects record ids
     */
    public static function getCollectionForUI($record_ids = array())
    {
        //filter
        $ids_filter = "";

        //filter by ids
        if(!empty($record_ids)) {

            foreach ($record_ids as $key => $id)
                $record_ids[$key] = "id = '".(int)$id."'";

            $ids_filter = " AND (".implode(" OR ", $record_ids).") ";
        }

        $sql = "SELECT ueta.*
                FROM user_event_ticket_asset ueta
                WHERE id > 0
                    $ids_filter
                ORDER BY id ASC";
        //var_dump($sql);exit;

        return self::getByQuery($sql, null, "UserEventTicketAsset");
    }
}
