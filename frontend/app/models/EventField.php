<?php

//other imports
use CrazyCake\Models\Base;

class EventField extends Base
{
    /* properties */

    /**
     * @var int
     */
    public $event_id;

    /**
     * @var int
     */
    public $efield_id;

    /**
     * @var int
     */
    public $priority;

    /**
     * @var boolean
     */
    public $required;

    /**
     * Initilizer
     */
    public function initialize()
    {
        //model relations
        $this->hasOne("event_id", "Event", "id");
        $this->hasOne("efield_id", "Efield", "id");
    }

    /** ------------------------------------------ § ------------------------------------------------- **/

    /**
     * Get events tickets with main info for UI render (RAW SQL)
     * @static
     * @param int $event_id The event id
     * @return Efield collection
     */
    public static function getCollectionForUI($event_id = 0)
    {
        //print_r($sql);exit;
        $result = self::getByQuery(
            "SELECT f.name, f.namespace, f.small_print, f.type, f.data, f._ext,
                ef.priority, ef.required
            FROM event_field ef
                INNER JOIN efield f ON f.id = ef.efield_id
            WHERE event_id = ?
            ORDER BY ef.priority ASC",
            //bindings
            [$event_id],
            //className
            "Efield"
        );

        if(!$result)
            return [];

        $objects = [];
        foreach ($result as $obj) {

            $objArray  = $obj->toArray();
            $objArray["priority"] = $obj->priority;
            $objArray["required"] = $obj->required;

            $objects[] = $objArray;
        }

        return $objects;
    }

    /**
     * Saves post event fields
     * @param int $event_id The event id
     * @param  array  $data The POST params
     * @return array  Array with saved objects
     */
    public static function filterData($event_id, $data = array())
    {
        //phalcon DI
        $di = \Phalcon\DI::getDefault();

        $response = new \stdClass();
        $response->data = [];
        $response->error_field = false;

        foreach ($data as $key => $value) {

            $namespace = str_replace("efield_", "", $key);
            $efield    = Efield::findFirstByNamespace($namespace);

            if(!$efield) continue;

            $event_field = self::findFirst([
                "event_id = ?1 AND efield_id = ?2 ",
                "bind" => [
                    1  => $event_id,
                    2  => $efield->id,
                ]
            ]);

            if(!$event_field) continue;

            //clean string, TODO: validar aquí casos especiales de inputs?
            $value = $di->getShared('filter')->sanitize(trim($value), "string");

            //validate field
            if($event_field->required && empty($value)) {

                $response->error_field = $efield->name;
                break;
            }

            $event_field = (object)$event_field->toArray();
            //set value
            $event_field->value = $value;

            //append objects
            array_push($response->data, $event_field);
        }

        return $response;
    }
}
