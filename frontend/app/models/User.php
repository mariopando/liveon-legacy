<?php
//imports
use Phalcon\Mvc\Model\Validator\InclusionIn;
//other imports
use CrazyCake\Account\BaseUser;

class User extends BaseUser
{
    /* aditional properties */

    /**
     * optional
     * @var string rut
     */
    public $rut;

    /**
     * values in GENDERS array
     * @var string
     */
    public $gender;

    /**
     * optional
     * @var string
     */
    public $phone;

    /**
     * birthday
     * @var string
     */
    public $birthday;

    /**
     * @static
     * @var array
     */
    static $GENDERS = ['undefined', 'male', 'female'];

    /**
     * Initilizer
     */
    public function initialize()
    {
        //call parent method
        parent::initialize();

        //model relations
        $this->hasOne("id", "UserFacebook", "user_id");
    }

    /**
     * Validation
     */
    public function validation()
    {
        //call parent method
        parent::validation();

        //gender
        $this->validate(new InclusionIn([
            "field"   => "gender",
            "domain"  => self::$GENDERS,
            "message" => $this->getDI()->getShared('trans')->_('Género inválido.')
        ]));

        //check validations
        if ($this->validationHasFailed() == true)
            return false;
    }

    /**
     * Parent Base required method
     * @param  string $key
     * @return string
     */
    protected function getModelMessage($key)
    {
        //set required messages
        $array = [
            //unique record
            "email_uniqueness" => $this->getDI()->getShared('trans')->_('El correo %email% ya se encuentra registrado, ingresa con tu correo %a% aquí. %_a%',
                                  ['email' => "<strong>".$this->email."</strong>", 'a' => '<a href="'.$this->getDI()->getShared('url')->getBaseUri().'signIn">', '_a' => '</a>']),
            //email required
            "email_required" => $this->getDI()->getShared('trans')->_('Ingresa un correo válido')
        ];

        return $array[$key];
    }
    /** ------------------------------------------- § --------------------------------------------------  **/
}
