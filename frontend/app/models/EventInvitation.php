<?php

//other imports
use CrazyCake\Models\Base;
use Phalcon\Mvc\Model\Validator\InclusionIn;

class EventInvitation extends Base
{
    /* conts */
    const CODE_LENGTH = 10;

    /* props */

    /**
     * @var string
     */
    public $code;

    /**
     * @var int
     */
    public $event_ticket_id;

    /**
     * @var int
     */
    public $activation_user_id;

    /**
     * @var string
     */
    public $activation_date;


    /**
     * Initilizer
     */
    public function initialize()
    {
        //custom model relations
        $this->hasOne("activation_user_id", "User", "id");

        //set relation for object types
        $this->hasOne("event_ticket_id", "EventTicket", "id");

        //Skip attributes
        $this->skipAttributesOnCreate(["activation_user_id", "activation_date"]);
    }

    /**
     * Before Validation Event [onCreate]
     */
    public function beforeValidationOnCreate()
    {
        //set alphanumeric code
        if(is_null($this->code))
            $this->code = $this->newCode();
    }

    /**
     * Before Validation Event [onUpdate]
     */
    public function beforeValidationOnUpdate()
    {
        parent::beforeValidationOnUpdate();

        if(is_null($this->activation_date))
            $this->activation_date = date('Y-m-d H:i:s');
    }

    /** ------------------------------------------- § ------------------------------------------------ **/


    /**
     * Generates a random Code for object class
     * @return string
     */
    protected function newCode()
    {
        $code = $this->getDI()->getShared('cryptify')->newAlphanumeric(self::CODE_LENGTH);
        //unique constrait
        $exists = self::findFirstByCode($code);

        return $exists ? $this->newCode() : $code;
    }
}
