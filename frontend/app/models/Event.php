<?php

use Phalcon\Mvc\Model\Validator\InclusionIn;
//other imports
use CrazyCake\Phalcon\AppLoader;
use CrazyCake\Models\Base;
use CrazyCake\Helpers\Dates;
//libs
use Carbon\Carbon;

class Event extends Base
{
    /* properties */

    /**
     * Flag to debug if an event is running
     */
    const DEBUG_EVENT_IS_RUNNING = false;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $namespace;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $state;

    /**
     * @var string
     */
    public $start_date;

    /**
     * @var string
     */
    public $end_date;

    /**
     * @var string
     */
    public $display_date;

    /**
     * @var string
     */
    public $hour;

    /**
     * @var string
     */
    public $content_html;

     /**
     * @var string
     */
    public $official_url;

    /**
     * @var string
     */
    public $created_at;

    /**
     * Extended properties
     */
    public $_ext;

    /**
     * @static
     * @var array
     */
    static $TYPES = array('free', 'paid', 'invitation');

    /**
     * @static
     * @var array
     */
    static $STATES = array('hidden', 'closed', 'open', 'finished');

    /**
     * Initilizer
     */
    public function initialize()
    {
        //model relations
        $this->hasMany("id", "EventTicket", "event_id");
        $this->hasOne("id", "EventPlace", "event_id");
        $this->hasOne("id", "EventSocial", "event_id");
        $this->hasOne("id", "EventFacebook", "event_id");

        //Skips fields/columns on both INSERT/UPDATE operations
        $this->skipAttributes(['created_at', '_ext']);
    }

    /**
     * After Fetch Event
     */
    public function afterFetch()
    {
        //extend properties, shared URLs with API
        $id_hashed     = $this->getDI()->getShared('cryptify')->encryptHashId($this->id);
        $resources_uri = AppLoader::getModuleUrl("frontend", "images/app/event/$id_hashed/", "static");

        $this->_ext = [
            "id_hashed"        => $id_hashed,
            "landing_url"      => AppLoader::getModuleUrl("frontend", "event/".$this->namespace),
            "image_url_square" => $resources_uri."event-square.jpg",
            "image_url_card"   => $resources_uri."event-card.jpg",
            "image_url_poster" => $resources_uri."event-poster.jpg",
            "image_url_large"  => $resources_uri."event-large.jpg"
        ];

        //restrict model for API
        if(MODULE_NAME == "api")
            return;

        //format event date?
        if(!isset($this->start_date))
            return;

        $event_date = date_parse($this->start_date);
        //set event day with all date info
        $this->_ext["start_hour"] = (new \DateTime($this->start_date))->format("H:i");
        $this->_ext["end_hour"]   = (new \DateTime($this->end_date))->format("H:i");
        $this->_ext["day_short"]  = $event_date['day']."-".Dates::getTranslatedMonthName($event_date['month'], true);
    }

    /**
     * Validation Event
     */
    public function validation()
    {
        //types
        $this->validate(new InclusionIn([
            "field"   => "type",
            "domain"  => self::$TYPES,
            "message" => 'Invalid type. Types supported: '.implode(", ", self::$TYPES)
        ]));

        //states
        $this->validate(new InclusionIn([
            "field"   => "state",
            "domain"  => self::$STATES,
            "message" => 'Invalid state. States supported: '.implode(", ", self::$STATES)
        ]));

        //check validations
        if ($this->validationHasFailed() == true)
            return false;
    }
    /** ------------------------------------------ § ------------------------------------------------- **/

    /**
     * Check if current event is runnning at current time
     * @return boolean
     */
    public function isRunning() {

        try {
            //use server datetime
            $now = new Carbon();
            //$now = Carbon::createFromDate(2015, 11, 01, 'America/Santiago');
            $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $this->start_date);
            $end_date   = Carbon::createFromFormat('Y-m-d H:i:s', $this->end_date);
            //s($now->toDateTimeString());exit;

            //consider one hour early from date
            $start_date->subHours(1);

            //check if now is between this dates
            return self::DEBUG_EVENT_IS_RUNNING ? true : $now->between($start_date, $end_date);
        }
        catch(Exception $e) {
            throw new Exception("Event::isRunning -> error: ".$e->getMessage());
            return false;
        }
    }
}
