<?php

//other imports
use CrazyCake\Models\Base;

class UserSocialAction extends Base
{
    /* properties */

    /**
     * @var int
     */
    public $user_id;

    /**
     * @var int
     */
    public $event_id;

    /**
     * @var string
     */
    public $action;

    /**
     * @var string
     */
    public $api;

    /**
     * @var sting [enabled, disabled]
     */
    public $state;

    /**
     * @var string
     */
    public $api_object_id;

    /**
     * @var string
     */
    public $created_at;

    /**
     * @static
     * @var array
     */
    static $ACTIONS = ['checkin', 'story', 'photo'];

    /**
     * @static
     * @var array
     */
    static $APIS = ['facebook', 'instagram'];

    /**
     * Initilizer
     */
    public function initialize()
    {
        //custom model relations
        $this->hasOne("user_id", "User", "id");
        $this->hasOne("event_id", "Event", "id");
    }

    /**
     * Before Validation Event [onCreate]
     */
    public function beforeValidationOnCreate()
    {
        $this->created_at = date('Y-m-d H:i:s');
    }

    /** ------------------------------------------- § ------------------------------------------------ **/

    /**
     * Saves a User Event Action
     * @param int $user_id
     * @param int $event_id
     * @param string $action
     * @param string $api
     * @param string $object_id
     * @param string $state [success or failed]
     * @return object
     */
    public static function saveAction($user_id, $event_id, $action, $api, $object_id, $state = 'success')
    {
        //save action
        $event_action = new self();

        $data = [
            "user_id"       => $user_id,
            "event_id"      => $event_id,
            "action"        => $action,
            "api"           => $api,
            "api_object_id" => $object_id,
            "state"         => $state
        ];

        //var_dump($data, $response);exit;
        $event_action->save($data);

        return $event_action ? $event_action->toArray() : null;
    }
}
