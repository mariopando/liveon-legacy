<?php

use Phalcon\Mvc\Model\Validator\Regex;
//other imports
use CrazyCake\Models\Base;

class Eplace extends Base
{
    /* properties */

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $caption;

    /**
     * @var float
     */
    public $latitude;

    /**
     * @var float
     */
    public $longitude;

    /**
     * @var string
     */
    public $url;

    /**
     * Validation Event
     */
    public function validation()
    {
        //coordinates latitude
        $this->validate(new Regex([
            'field'   => 'latitude',
            'pattern' => '/([0-9.-]+).+?([0-9.-]+)/',
            "message" => 'invalid latitude value'
        ]));

        //coordinates longitude
        $this->validate(new Regex([
            'field'   => 'longitude',
            'pattern' => '/([0-9.-]+).+?([0-9.-]+)/',
            "message" => 'invalid longitude value'
        ]));

        //check validations
        if ($this->validationHasFailed() == true)
            return false;
    }
}
