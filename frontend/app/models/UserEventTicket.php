<?php

//other imports
use CrazyCake\Tickets\BaseUserTicket;

class UserEventTicket extends BaseUserTicket
{
    /* conts */
    public static $QR_CODE_LEGTH = 24;

    /**
     * Initilizer
     */
    public function initialize()
    {
        parent::initialize();
        //custom model relations
        $this->hasOne("ticket_id", "EventTicket", "id");
    }

    /**
     * Before Validation Event [onCreate]
     */
    public function beforeValidationOnCreate()
    {
        parent::beforeValidationOnCreate();
        //saves QR & ticket code automatically...
    }

    /**
     * After Fetch Event
     */
    public function afterFetch()
    {
        parent::afterFetch();

        //extend properties
        $this->_ext["code_url"] = $this->getDI()->getShared('url')->getBaseUri()."storage/ticket/".$this->code;

        $assets_owned = UserEventTicketAsset::getCollection($this->id);
        $this->_ext["assets_owned_count"] = 0;

        //append ticket state
        if(isset($this->eventTicket))
            $this->_ext["state"] = isset($this->eventTicket->state) ? $this->eventTicket->state : null;

        //append owned assets
        if($assets_owned) {

            $this->_ext["assets_owned"] = [];

            foreach ($assets_owned as $assets)
                $this->_ext["assets_owned"][] = $assets->eventTicketAsset ? $assets->eventTicketAsset->toArray(["_ext"]) : [];

            $this->_ext["assets_owned_count"] = $assets_owned->count();
        }

        $assets_count = EventTicketAsset::getCollectionForUI($this->ticket_id);
        $this->_ext["assets_count"] = $assets_count ? $assets_count->count() : 0;
    }

    /**
     * Record Not Saved Event
     */
    public function notSave()
    {
        //log error
        $this->getDI()->get('logger')->error("UserEventTicket -> Error Insertion User EventTicket data. userId -> ".$this->user_id.", ticket_id -> ".$this->ticket_id);
    }

    /** ------------------------------------------- § ------------------------------------------------ **/

    /**
     * Get user ticket by ticket_id
     * @static
     * @param int $user_id The user ID
     * @param int $event_id The event id (optional)
     * @param array $record_ids The record Ids (optional)
     * @return UserEventTicket
     */
    public static function getCollection($user_id, $event_id = null, $record_ids = array())
    {
        //event filter
        $event_filter = is_null($event_id) ? "" : " AND et.event_id = '".(int)$event_id."'";
        //filter by ids
        $ids_filter = "";

        if(!empty($record_ids)) {

            foreach ($record_ids as $key => $id)
                $record_ids[$key] = "uet.id = '".(int)$id."'";

            $ids_filter = implode(" OR ", $record_ids);
            $ids_filter = " AND (".$ids_filter.") ";
        }

        return self::getByQuery(
            "SELECT uet.*, et.event_id
             FROM user_event_ticket uet
             INNER JOIN event_ticket et
                ON (et.id = uet.ticket_id)
             WHERE uet.user_id = ?
                 $event_filter
                 $ids_filter
             ORDER BY uet.created_at ASC
            ",
            [$user_id],
            "UserEventTicket"
        );
    }

    /**
     * Get tickets for given user for UI render. Can filter by event_id (RAW SQL)
     * TODO: phalconIssue -> toArray resulset function don't calls afterFetch,
     * @param int $user_id The user id
     * @param int $event_id An event id (optional)
     * @param boolean $group_by Group elements (optional)
     * @param array $record_ids An array of UserEventTicket IDs (optional)
     * @param string $order_by The order by filter
     * @return UserEventTicket
     */
    public static function getCollectionForUI($user_id, $event_id = null, $group_by = true, $record_ids = array(), $order_by = "")
    {
        //event filter
        $event_filter = is_null($event_id) ? "" : " AND et.event_id = '".(int)$event_id."'";
        //group by filter
        $group_by = $group_by ? " GROUP BY et.event_id " : "";
        //group by event_ticket filter (auto)
        $ids_filter = "";

        //filter by ids
        if(!empty($record_ids)) {

            foreach ($record_ids as $key => $id)
                $record_ids[$key] = "uet.id = '".(int)$id."'";

            $ids_filter = " AND (".implode(" OR ", $record_ids).") ";
        }

        //set order by param
        if(empty($order_by))
            $order_by = "uet.created_at ASC";

        $sql = "SELECT uet.id, uet.code, uet.ticket_id
                FROM user_event_ticket uet
                INNER JOIN event_ticket et
                    ON (et.id = uet.ticket_id)
                INNER JOIN event e
                    ON (e.id = et.event_id)
                WHERE uet.user_id = ?
                    $event_filter
                    $ids_filter
                $group_by
                ORDER BY $order_by
                "; //LIMIT 1
        //var_dump($sql);exit;

        return self::getByQuery($sql, [$user_id], "UserEventTicket");
    }

    /**
     * Get user tickets that event has social required property
     * @param  integer $user_id - The user ID
     */
    public static function getCollectionWithSocialRequired($user_id = 0)
    {
        $sql = "SELECT uet.code, et.name
                FROM user_event_ticket uet
                INNER JOIN event_ticket et
                    ON (et.id = uet.ticket_id)
                INNER JOIN event e
                    ON (e.id = et.event_id)
                INNER JOIN event_social es
                        ON (e.id = es.event_id AND es.required = 1)
                WHERE uet.user_id = ?
                    AND e.state = 'open'
                ORDER BY uet.id ASC";
        //var_dump($sql);exit;

        return self::getByQuery($sql, [$user_id], "UserEventTicket");
    }

    /**
     * Get user tickets IDS that belong to an event that is running
     * @static
     * @param int $user_id - The user ID
     * @return array
     */
    public static function getIdsFromEventRunning($user_id = 0)
    {
        $user_tickets = self::getCollection($user_id);

        if(!$user_tickets)
            return false;

        $running_tickets = [];

        foreach ($user_tickets as $ticket) {
            //get event
            $event = $ticket->eventTicket->event;

            if($event->isRunning())
                $running_tickets[] = $ticket->id;
        }

        return $running_tickets;
    }

    /**
     * Get total unassigned user event tickets for a given event
     * @static
     * @param int $user_id The user ID
     * @param int $event_id The event id
     * @return UserEventTicket
     */
    public static function countUnassigned($user_id = 0, $event_id = 0)
    {
        $result = self::getByQuery(
            "SELECT uet.code
             FROM user_event_ticket uet
             INNER JOIN event_ticket et
                ON (et.id = uet.ticket_id)
             LEFT JOIN event_ticket_asset eta
                ON (eta.ticket_id = uet.ticket_id)
             WHERE uet.user_id = ?
                 AND et.event_id = ?
                 AND eta.ticket_id IS NOT NULL
                 AND et.state != 'closed'
             GROUP BY uet.code
             ORDER BY uet.id ASC
            ",
            [$user_id, $event_id],
            "UserEventTicket"
        );
        //var_dump($result->toArray());exit;

        return $result ? $result->count() : 0;
    }

    /**
     * Update user ticket owner
     * @param  int $record_id - The record ID
     * @param  int $user_id - The user ID
     */
    public static function updateOwner($record_id = 0, $user_id = 0)
    {
        return self::executePhql(
            "UPDATE UserEventTicket
                SET user_id = ?0
                WHERE id = ?1
            ",
            [$user_id, $record_id]
        );
    }

    /** ------------------------------------------- § ------------------------------------------------ **/
}
