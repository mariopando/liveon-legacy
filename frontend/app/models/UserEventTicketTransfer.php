<?php

//other imports
use CrazyCake\Models\Base;

class UserEventTicketTransfer extends Base
{
    /**
     * @var int
     */
    public $user_id;

    /**
     * @var string
     */
    public $receiver_name;

    /**
     * @var string
     */
    public $receiver_email;

    /**
     * @var int
     */
    public $user_event_ticket_id;

    /**
     * @var string
     */
    public $token;

    /**
     * @var string
     */
    public $state;

    /**
     * @var string
     */
    public $created_at;

    /**
     * @static
     * @var array
     */
    static $STATES = ['pending', 'success'];

    /**
     * Initilizer
     */
    public function initialize()
    {
        //custom model relations
        $this->hasOne("user_event_ticket_id", "UserEventTicket", "id");

        //Skips fields/columns on both INSERT/UPDATE operations
        $this->skipAttributes(['created_at']);
    }

    /**
     * Before Validation Event [onCreate]
     */
    public function beforeValidationOnCreate()
    {
        //creates token
        if(is_null($this->token))
            $this->token = sha1($this->user_id.$this->receiver_email.microtime());

        //default state
        if(is_null($this->state))
            $this->state = self::$STATES[0];
    }

    /** ------------------------------------------- § ------------------------------------------------ **/

    /**
     * Get transfer by token
     * @static
     * @param string $token The token
     * @return UserEventTicketTransfer
     */
    public static function getByToken($token)
    {
        $transfer = self::findFirstByToken($token);

        return $transfer;
    }


    /**
     * Get transfer by userId and recordId
     * @static
     * @param int $user_id   The user id
     * @param int $record_id The record id
     * @return UserEventTicketTransfer
     */
     public static function getByUserIdAndRecordId($user_id = 0, $record_id = 0, $state = "pending")
     {
         $object = self::findFirst([
             "user_id = ?1 AND id = ?2 AND state = ?3",
             "bind" => [
                 1 => $user_id,
                 2 => $record_id,
                 3 => $state,
             ]
         ]);

         return $object;
     }

    /**
     * Get transfer by user_id and user_event_ticket ID
     * @static
     * @param int $user_id The user ID
     * @param int $user_ticket_id The UserEventTicket record ID
     * @return UserEventTicketTransfer
     */
    public static function getByUserTicket($user_id = 0, $user_ticket_id = 0, $state = "pending")
    {
        $object = self::findFirst([
            "user_id = ?1 AND user_event_ticket_id = ?2 AND state = ?3",
            "bind" => [
                1 => $user_id,
                2 => $user_ticket_id,
                3 => $state,
            ]
        ]);

        return $object;
    }

    /**
     * Get transfer by token
     * @static
     * @param object $userTicket The UserEventTicket object
     * @param array $data The receiver data
     * @return UserEventTicketTransfer
     */
    public static function newOrder($userTicket, $data = array())
    {
        $transfer = new UserEventTicketTransfer();
        $transfer->user_id              = $userTicket->user_id;
        $transfer->user_event_ticket_id = $userTicket->id;
        $transfer->receiver_name        = $data["receiverName"];
        $transfer->receiver_email       = $data["receiverEmail"];

        //save it
        if(!$transfer->save())
            return false;

        return $transfer;
    }

    /**
     * Set Ticket transfer records for UI JS
     * @static
     * @param object $objects The UserEventTicket resultArray
     * @return void
     */
    public static function setDataForUserTicketsJs(&$objects, $state = "pending")
    {
        if(empty($objects))
            return;

        //loop through objects
        foreach ($objects as &$object) {

            //get object_id by code (is for UI result is unset)
            $objectOrm = UserEventTicket::getByCodeAndUserId($object["code"], 0);

            $transfer = self::findFirstByUserEventTicketId($objectOrm->id);

            if(!$transfer || $transfer->state != $state)
                continue;

            //unset unwanted props
            $transfer = (object)$transfer->toArray(["id", "receiver_name", "receiver_email"]);

            //append properties
            $object["transfer"] = $transfer;
        }
    }

    /** ------------------------------------------- § ------------------------------------------------ **/
}
