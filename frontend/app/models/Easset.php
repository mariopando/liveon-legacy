<?php

//imports
use CrazyCake\Phalcon\AppLoader;
use CrazyCake\Models\Base;

class Easset extends Base
{
    /* properties */

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $namespace;

    /**
     * Extended properties
     */
    public $_ext;

    /**
     * Initilizer
     */
    public function initialize()
    {
        //model relations
        $this->hasMany("id", "EventTicketAsset", "easset_id");

        //Skips fields/columns on both INSERT/UPDATE operations
        $this->skipAttributes(['_ext']);
    }

    /**
     * After Fetch Event
     */
    public function afterFetch()
    {
        //shared URL with API
        $this->_ext["image_url"] = AppLoader::getModuleUrl("frontend", "images/app/easset/".$this->namespace.".png", "static");
    }
}
