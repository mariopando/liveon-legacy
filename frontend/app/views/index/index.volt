<div class="sections">
	<div class="page">
		{# Header TopBar #}
		{{ partial("templates/header") }}

		{% if main_ticket is defined %}
			<section id="app-first-content" class="app-middle-content intro text-center" data-fit="24">
				<div>
					<h1>{{ main_ticket.event.name }}</h1>
					<h2>{{ main_ticket.event.caption~" / "~main_ticket.event.display_date }}</h2>

					<form action="{{ url('event/'~main_ticket.event.namespace) }}" method="get">
						<button class="app-btn-transparent">{{ trans._('Ver más') }}</button>
					</form>

				</div>
			</section>
		{% endif %}
		{# Section 2 #}
		<div class="events-cards-wrapper">
			{# Event #}
			{% if tickets is defined %}
				{# set cache fragment #}
				{#{% cache "index_tickets" 3600 %}#}
				{#<h2 class="subtitle text-center">&nbsp;</h2>#}
				<section class="events">
					<div class="row">
						{% set cols_number = tickets|length == 2 ? 'large-6' : 'large-4' %}
						{% for ticket in tickets %}
						{# custom column number #}
							<div class="columns app-event-card-holder">
								<div class="app-event-card {{ ticket.event.state != 'open' ? 'inactive' : 'active' }}">
									<a class="absolute" href="{{ url('event/'~ticket.event.namespace) }}">&nbsp;</a>
									<img src="{{ ticket.event._ext['image_url_card'] }}" data-retina alt="" />
									{#<div>#}
										{#<span>{{ ticket.event.name }}</span>#}
										{#<span>{{ ticket.event.caption }}</span>#}
										{#<span class="place">{{ ticket.event.eventPlace.eplace.name }}</span>#}
										{#<ul>#}
											{#<li><span class="title">{{ trans._('Día') }}</span> <span>{{ ticket.event._ext['day_short'] }}</span></li>#}
											{#<li><span class="title">{{ trans._('Hora') }}</span> <span>{{ ticket.event._ext["start_hour"] }}</span></li>#}
											{#<li><span class="title">{{ trans._('Desde') }}</span> <span>{{ ticket._ext['st_price_formatted'] }}</span></li>#}
										{#</ul>#}
									{#</div>#}
								</div>
							</div>
							{# complete another row #}
						{% endfor %}
					</div>
				</section>
				{#{% endcache %}#}
			{% endif %}
		</div>

		{# video #}
		{#<div class="video show-for-large">#}
			{#<div>
				<div class="app-embed-container">
					{#<iframe src="http://player.vimeo.com/video/119264084?color=85C11E&amp;title=0&amp;portrait=0" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>#}
					{#<iframe width="853" height="480" src="https://www.youtube.com/embed/DNkae1OYITM" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div>#}
	</div>
	<div class="page menu">
		<div class="row collapse">
			<div class="columns small-3">
				<div class="menu-link bracelet">
					<a href="javascript:void(0);">
						<span class="icon"></span>
						<h2>Pulsera</h2>
						<h3>Motor de la plataforma</h3>
					</a>
					<div class="content">
						<span class="small-12 content-close"><img src="{{ static_url('images/icons/close-icon.png') }}" /></span>
						<img class="small-12 content-image" src="{{ static_url('images/content/bracelet-pic.png') }}" />
						<div class="small-12 content-info">
							<div class="column small-6">
								<h3>Pulsera</h3>
								<p>Tu muñeca se transforma en un vehículo digital conectado a la red. En un medio de intercambio fácil y sin papel.</p>
							</div>
							<div class="column small-6">
								<img src="{{ static_url('images/icons/water-big-icon.png') }}" />
								<img src="{{ static_url('images/icons/creditcard-big-icon.png') }}" />
								<img src="{{ static_url('images/icons/cloud-big-icon.png') }}" />
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="columns small-3">
				<div class="menu-link access">
					<a href="javascript:void(0);">
						<span class="icon"></span>
						<h2>Acceso</h2>
						<h3>Portal de transición</h3>
					</a>

					<div class="content">
						<span class="small-12 content-close"><img src="{{ static_url('images/icons/close-icon.png') }}" /></span>
						<img class="small-12 content-image" src="{{ static_url('images/content/access-pic.png') }}" />
						<div class="small-12 content-info">
							<div class="column small-6">
								<h3>Acceso</h3>
								<p>Portal para definir la frontera, agilizar el tráfico y personalizar la experiencia.</p>
							</div>
							<div class="column small-6">
								<img src="{{ static_url('images/icons/doublecheck-big-icon.png') }}" />
								<img src="{{ static_url('images/icons/gps-big-icon.png') }}" />
								<img src="{{ static_url('images/icons/headphones-big-icon.png') }}" />
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="columns small-3">
				<div class="menu-link exchange">
					<a href="javascript:void(0);">
						<span class="icon"></span>
						<h2>Intercambio</h2>
						<h3>Eslabón de conexion</h3>
					</a>
					<div class="content">
						<span class="small-12 content-close"><img src="{{ static_url('images/icons/close-icon.png') }}" /></span>
						<img class="small-12 content-image" src="{{ static_url('images/content/exchange-pic.png') }}" />
						<div class="small-12 content-info">
							<div class="column small-6">
								<h3>Intercambio</h3>
								<p>Estación diseñada para generar lazos, digitalizar la compra y facilitar el intercambio.</p>
							</div>
							<div class="column small-6">
								<img src="{{ static_url('images/icons/icon-garbage-big.png') }}" />
								<img src="{{ static_url('images/icons/icon-exchange2-big.png') }}" />
								<img src="{{ static_url('images/icons/icon-shakehands-big.png') }}" />
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="columns small-3">
				<div class="menu-link social">
					<a href="javascript:void(0);">
						<span class="icon"></span>
						<h2>Social</h2>
						<h3>Estación interactiva</h3>
					</a>
					<div class="content">
						<span class="small-12 content-close"><img src="{{ static_url('images/icons/close-icon.png') }}" /></span>
						<img class="small-12 content-image" src="{{ static_url('images/content/social-pic.png') }}" />
						<div class="small-12 content-info">
							<div class="column small-6">
								<h3>Social</h3>
								<p>Portal interactivo para aumentar tu experiencia. Puente al mundo virtual en tiempo real.</p>
							</div>
							<div class="column small-6">
								<img src="{{ static_url('images/icons/icon-picture-big.png') }}" />
								<img src="{{ static_url('images/icons/icon-chat-big.png') }}" />
								<img src="{{ static_url('images/icons/icon-play-big.png') }}" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>