{# New pass View #}
<div id="vue-passRecovery" class="app-password-wrapper app-block app-form recaptcha-size">
	{# New pass form #}
   	<form data-validate v-on:submit="saveNewPassword" action="javascript:void(0);">

        <h6 class="subheader text-center">{{ trans._('Crea una nueva contraseña.') }}</h6>

        <div class="row">
            <div class="columns small-12">
                <input name="pass" type="password" size="50" maxlength="20" autocomplete="off" placeholder="{{ trans._('Crea tu contraseña') }}"
                    data-fv-required="stringLength: { min: 8, max: 20, message: '{{ trans._('Al menos 8 caracteres.') }}' }"
                    data-fv-message="{{ trans._('Al menos 8 caracteres.') }}" />
            </div>
        </div>
        <div class="row">
            <div class="columns small-12">
                <input name="edata" type="hidden" value="{{ edata }}" />
        		<button type="submit" class="app-btn">
                    <img src="{{ static_url('images/icons/icon-app-small.png') }}" data-retina alt="" />
                    <span>{{ trans._('Guardar') }}</span>
                </button>
            </div>
		</div>
    </form>

</div>
