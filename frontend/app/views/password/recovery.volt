{# Recovery pass View #}
<div id="vue-passRecovery" class="app-password-wrapper app-block app-form recaptcha-size">

    <h6 class="subheader text-center">{{ trans._('Recupera tu contraseña.') }}</h6>

	{# recovery pass form #}
   	<form data-validate data-fv-live="disabled" v-on:submit="sendRecoveryInstructions" action="javascript:void(0);">
        <div class="row">
            <div class="columns small-12">
    		    <input name="email" type="email" size="50" maxlength="255" autocomplete="on" placeholder="{{ trans._('Correo electrónico') }}"
                       data-fv-required data-fv-message="{{ trans._('Ingresa un correo electrónico válido.') }}" />
    	    </div>
        </div>
         {# reCaptcha #}
         <div class="row small-collapse">
             <div class="columns small-12">
                 <div class="recaptcha">
                     <small>{{ trans._('Cargando Recaptcha...') }}</small>
                     <div id="app-recaptcha"></div>
                     <input name="reCaptchaValue" type="hidden"
                            data-fv-excluded="false" data-fv-required="numeric : {}" data-fv-message="{{ trans._('Completa el reCaptcha.') }}" />
                 </div>
            </div>
        </div>
        {# submit button #}
        <div class="row">
           <div class="columns small-12">
                <button type="submit" class="app-btn">
                    <img src="{{ static_url('images/icons/icon-app-small.png') }}" data-retina alt="" />
                    <span>{{ trans._('Enviar') }}</span>
                </button>
            </div>
        </div>
    </form>

</div>
