{# Activation code form view #}
<div id="app-first-content" class="app-event-invitation-wrapper app-checkout-wrapper app-form">

	{# Breadcrumbs #}
	<ul class="breadcrumbs title hide-for-large">
		<li><span>{{ trans._('Canje') }}</span></li>
	</ul>

	<div class="row medium-collapse">
		<div class="medium-7 columns hide-for-small-only">
			<img src="{{ static_url('images/bgs/bg-exchange-lanscape.jpg') }}" class="exchange" data-retina alt="" />
		</div>
		<div class="small-12 medium-5 columns text-center">
			{# Code form #}
			<div id="vue-eventInvitation" class="invitation-form">

				<p class="instructions text-center">
					{{ trans._('Ingresa tu código') }}
				</p>

				{# code form #}
				<form data-validate v-on:submit="checkInvitationCode" action="javascript:void(0);">
					{# code and text inputs #}
					<div class="row">
			            <div class="columns small-12">
							<input name="invitationCode" class="code" type="text" size="20" maxlength="14" placeholder="{{ trans._('Código de Invitación') }}"
								   data-fv-required="stringLength: { min: 3, max: 14, message: '{{ trans._('Ingresa tu código.') }}' }"
								   data-fv-message="{{ trans._('Ingresa tu código.') }}" />
						</div>
					</div>
					{# submit button #}
					<div class="row">
			            <div class="columns small-12">
							<button type="submit" class="app-btn">
								<img src="{{ static_url('images/icons/icon-app-small.png') }}" data-retina alt="" />
								<span>{{ trans._('Continuar') }}</span>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
