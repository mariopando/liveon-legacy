{# Invitation code confirmation form view #}
<div id="app-first-content" class="app-event-section">

	<div id="vue-eventInvitation" class="app-event-invitation-wrapper app-event-wrapper app-form">
        {# event head #}
        {{ partial("event/headerDetails") }}
        {# event body #}
        <div class="opaque event-body large-text-justify">
			{# prevents box division bug #}
            <div class="app-line-spacer">&nbsp;</div>

			{# ticket name #}
			<div class="ticket-category">
				<h4 class="app-section-title">{{ ticket.name }}</h4>
				<p>{{ ticket.small_print }}</p>
			</div>

			{# activation form #}
			<form id="event-invitation" data-validate action="javascript:void(0);">

				{# NOTE: HARDCODED - SELECTOR [ASSETS] #}
				{#<div v-if="event.namespace == 'club_hipico_2016_01'">
					<div class="ticket-resume">
						<h4 class="app-section-title">{{ trans._('Extras') }}</h4>
						<p>
							&raquo; {{ trans._("Créditos restantes: %num%",
										["num" : '<span v-text="getCreditsLeft" v-bind:style="{ color: creditsColor }"></span>']) }}
						</p>

						<eassets v-ref:easset v-for="asset in eassets" :asset="asset">
						</eassets>
					</div>
				</div>#}

				{# render efields if any #}
				<div class="app-events-fields app-form text-center" v-cloak v-if="efields">
					<h5>{{ trans._('Los siguientes campos son obligatorios para este evento:') }}</h5>
					{# binds a efield row #}
					<efields v-for="field in efields" :field="field">
					</efields>
				</div>

				{# invoice email #}
				<div class="app-invoice-email-box app-form">

					<div class="row text-center">
						<div class="columns small-12">

						{% if event.type == "free" or event.type == "invitation" %}
							<span>{{ trans._('Enviar mi entrada a:') }}</span>
						{% else %}
							<span>{{ trans._('Enviar mi entrada y comprobante a:') }}</span>
						{% endif %}

							<input name="invoiceEmail" type="text" size="50" maxlength="255" autocomplete="on"
							   	   value="{{ invoiceEmail }}" placeholder="{{ trans._('Correo electrónico') }}"
							   	   data-fv-required data-fv-message="{{ trans._('Ingresa un correo electrónico válido.') }}" />
						   </div>
					</div>

				</div>
				<div class="app-line-spacer">&nbsp;</div>

				{# submit button #}
		        <div class="button-wrapper text-center">
					<div class="row text-center small-collapse">
						<div class="columns small-12">

							{# invitation code #}
							<input name="invitationCode" type="hidden" value="{{ invitationCode }}"/>
							{# ticket required element (checkout) #}
							<input name="{{ 'checkout_EventTicket_'~ticket._ext['id_hashed'] }}" type="hidden" value="1"/>

							{# button #}
							{% set eventSocial = event.eventSocial %}
							{% if eventSocial is not empty and eventSocial.required %}
								<button class="app-btn-fb styled wide" disabled="disabled" data-action="login" data-fb-loaded="{{ trans._('Confirmar con Facebook') }}">
									<img src="{{ static_url('images/icons/icon-fb-small.png') }}" data-retina alt="" />
									<span>{{ trans._('Cargando facebook...') }}</span>
								</button>
								<div class="terms text-center">
									{{ trans._('Confirmando este evento estás aceptando publicaciones en tu muro. %a_open%ver más%a_close%',
										["a_open" :'<a href="'~url('content/terms#before-facebook')~'" target="_blank">[', "a_close" : "]</a>"]) }}
								</div>
							{% else %}
								<button v-on:click="activationConfirm" v-bind:disabled="getCreditsLeft < 0" class="app-btn">
					                <img src="{{ static_url('images/icons/icon-app-small.png') }}" data-retina alt="" />
					                <span>{{ trans._('Confirmar') }}</span>
					            </button>
							{% endif %}
						</div>
					</div>
				</div>

			</form>
		</div>

	</div>
</div>

{# Components #}
{{ partial("components/eventField") }}
{{ partial("components/eventAsset") }}
