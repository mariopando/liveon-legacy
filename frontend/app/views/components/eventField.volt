{# Efield Component #}

<script type="text/html" id="vue-template-event-field">

	{# eventField #}
	<div>
		<div class="name-wrapper">
			<img src="" alt="" class="field-icon" v-bind:src="field._ext.image_url" data-retina />
			<span v-text="field.name"></span>
		</div>
		<div class="row">
			<div v-if="field.type == 'phone'" class="columns small-12">
				<span class="phone-prefix">+569</span>
				<input type="text" maxlength="8" v-bind:name="field.nameAttr" v-model="field.value"
					   data-fv-required="integer: {}" data-fv-message="{{ trans._('Ingresa un número válido.') }}" />

				{# small print #}
				<p class="small-print" v-html="small_print"></p>
			</div>
		</div>
	</div>

</script>
