{# Easset Component #}

<script type="text/html" id="vue-template-event-asset">

	{# eventAsset #}
	<div class="row checkout-objects small-collapse medium-uncollapse text-center">
		<div class="small-7 large-4 columns text-left">
			<span class="name" v-text="asset.name">&nbsp;</span>
			<div> <a href="javascript:void(0);" class="app-see-more" v-on:click="asset.seeMore = !asset.seeMore">{{ '['~trans._('Ver más')~']'}}</a> </div>
			<div class="app-small-print-wapper">
				<div v-html="asset.small_print" v-show="asset.seeMore"></div>
			</div>
		</div>
		<!-- <div class="small-2 medium-3 columns">
			<span class="price" v-text="asset._ext.price_formatted">&nbsp;</span>
		</div> -->
		<div class="small-5 large-8 columns small-text-right large-text-left">
			<select v-if="asset.state == 'open'" v-bind:name="asset.nameAttr" v-model="asset.selectedQ" class="small-text-for-small">
				<option v-for="option in $parent.creditsAmountsOpts"
						v-text="option"></option>
			</select>
			<p v-if="asset.state == 'soldout'" class="small-text-for-small" style="color:#FCC83A;">{{ trans._("Agotado") }}</p>
			<p v-if="asset.state == 'closed'" class="small-text-for-small" style="color:#FCC83A;">{{ trans._("No disponible") }}</p>
		</div>
	</div>

</script>
