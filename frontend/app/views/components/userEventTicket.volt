{# User Tickets Component #}

<script type="text/html" id="vue-template-user-event-ticket">

	{# Ticket Row #}
    <div v-bind:id="rowId">
    	<div class="row collapse styled ticket-row">
    		<div class="small-12 medium-6 columns text-left">

    			<div class="transfer-status" v-html="transferStatus"></div>
                <div>
                    <a href="javascript:void(0);" class="app-see-more" v-show="obj._ext.assets_owned_count > 0" v-on:click="seeMore = !seeMore">
                        {{ trans._('[Extras]') }}
                    </a>
                </div>
                <div class="app-small-print-wapper">
                    <div v-if="seeMore">
                        {# loop through assets #}
                        <div v-for="subObj in obj._ext.assets_owned" class="assets-owned">
                            <span v-text="'+ ' + subObj._ext.name"></span>
                        </div>
                    </div>
                </div>

    		</div>
    		<div class="small-12 medium-6 columns text-center medium-text-right controls-wrapper">

    			<button v-on:click="getTicket" class="app-btn-small extended">
    				{{ trans._('Ver') }}
    			</button>
                {# only shows if has assets available #}
                <button v-on:click="goToExtras" v-bind:disabled="extrasButtonDisabled" class="app-btn-small extended rightest">
    				{{ trans._('Extras') }}
    			</button>
                {# form action button, view model handles it #}
    			<button v-on:click="toggleForm" v-bind:disabled="transferButtonDisabled" v-text="buttonToggleText" class="app-btn-small last">
    				{{ trans._('Transferir') }}
    			</button>

    		</div>
    	</div>

    	<div v-show="displayForm" class="form-transfer-wrapper app-form" >
    		<div class="row">
    			<div class="small-12 medium-6 large-7 small-collapse large-uncollapse columns small-text-left large-text-center">

    				<h5>{{ trans._('Transfiere esta entrada a quien quieras.') }}</h5>

    				<div class="instructions">
    					<p>
    						{{ trans._('Ingresa el nombre y correo electrónico de la persona que quieras que reciba esta entrada.') }}
    					</p>
    				</div>
    			</div>
    			<div class="small-12 medium-6 large-5 columns transfer-form-col">
    				{# transfer form #}
    				<div>
    					<form data-validate v-on:submit="$root.newTransferOrder($event, obj.index)">
    						{# code and text inputs #}
                            <div class="row app-no-padding">
                                <div class="columns small-12">
        							<input name="receiverName" type="text" size="20" maxlength="60" placeholder="{{ trans._('Nombre') }}"
                                           data-fv-required="stringLength: { min: 3, max: 60, message: '{{ trans._('Ingresa el nombre.') }}' }"
                                           data-fv-message="{{ trans._('Ingresa el nombre.') }}" />
                                </div>
                            </div>
                            <div class="row app-no-padding">
                                <div class="columns small-12">
        							<input name="receiverEmail" type="email" size="20" maxlength="255" autocomplete="on" placeholder="{{ trans._('Correo electrónico') }}"
                                           data-fv-required data-fv-message="{{ trans._('Ingresa un correo electrónico válido.') }}" />
                                </div>
    						</div>
    						{# submit button #}
                            <div class="row">
                                <div class="columns small-12">
        							<input name="code" type="hidden" v-model="obj.code" />
        							<button type="submit" class="app-btn">
        								<img src="{{ static_url('images/icons/icon-app-small.png') }}" data-retina alt="" />
        								<span>{{ trans._('Transferir') }}</span>
        							</button>
                                </div>
    						</div>
    					</form>
    				</div>
    				{# agreement (shared) #}
    				<div>
    					<p class="agreement text-center">{{ trans._('Al hacer click en Transferir, aceptas los %a_open%términos%a_close%
    						de transferencia.', ["a_open" : '<a href="'~url('content/terms#transfers')~'">', "a_close" : "</a>"]) }}
    					</p>
    				</div>
    			</div>
    		</div>
    	</div>

    	<!-- Ticket Modal -->
    	{# Ticket modal for Show Button #}
        <div v-bind:id="modalId" class="ticket-modal-wrapper reveal app-block modal-size" data-reveal>
            <div class="text-center">
    			<h4 class="text-center" v-text="obj.name"></h4>
    			<img src="{{ static_url('images/icons/icon-image-fallback.png') }}" alt="" v-bind:src="obj._ext.code_url"  />
    			<h3 class="text-center" v-text="obj.code"></h3>
            </div>
            {# Close modal #}
            <button class="close-button" data-close aria-label="{{ trans._('Cerrar') }}" type="button">
                <span aria-hidden="true">&times;</span>
             </button>
        </div>
    <!-- Row Closer -->
    </div>

</script>
