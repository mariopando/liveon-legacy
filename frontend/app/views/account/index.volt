<div class="tickets">

	{# Breadcrumbs #}
	<ul class="breadcrumbs title hide-for-large">
		<li><span>{{ trans._('Entradas') }}</span></li>
	</ul>

	<div class="tickets-blocks-holder">
	{# holder #}
	{% if my_tickets is defined %}

		{# vars #}
		{% set items_per_row = 3  %}
		{% set items_number = my_tickets|length %}
		{% set cols = items_number > items_per_row ? items_per_row : items_number %}

		{# loop #}
		{% for ticket in my_tickets %}
			{# data must be splitted in three #}
			{% if loop.first %}
	<!--open--><div class="row small-collapse">
			{% endif %}

			{% set ticket_event = ticket.eventTicket.event %}
			{% set tickets_url  = url('account/tickets/'~ticket_event.namespace) %}

				{# create a event-card column, todo: fix position to more than 4 events #}
				<div class="small-12 large-{{ 12/cols }} columns">
				<div>
					{# class vars #}
					{% set state_class  = (ticket_event.state != 'open' and ticket_event.state != 'invisible') ? 'inactive' : 'active' %}
					{% set single_class = (items_number > 1 and cols == 1) ? 'single': 'multiple' %}
					{% set paired_class = (items_number == 2) ? 'paired-'~loop.index : 'not-paired' %}

					<div class="app-event-card ticket text-center {{ state_class~' '~single_class~' '~paired_class }}">
			 			<a class="absolute" href="{{ tickets_url }}">&nbsp;</a>
						<img src="{{ ticket_event._ext['image_url_card'] }}" data-retina alt="" />
						<div>
							<span>{{ ticket_event.name }}</span>
							<span>{{ ticket_event.caption }}</span>
							<span class="place">{{ ticket_event.eventPlace.eplace.name }}</span>
							<ul>
								<li><span class="title">{{ trans._('Día') }}</span> <span>{{ ticket_event._ext["day_short"] }}</span></li>
								<li><span class="title">{{ trans._('Hora') }}</span> <span>{{ ticket_event._ext["start_hour"] }}</span></li>
								<li>
									<form action="{{ tickets_url }}" method="get">
										<button>{{ trans._('Ver') }}</button>
									</form>
								</li>
							</ul>
						</div>
					</div>
				</div>
				</div>
			{# close block if is still opened #}
			{% if loop.last %}
	<!--last--></div>
			{% elseif loop.index % items_per_row == 0 %}
	<!--open--></div> <div class="row small-collapse">
				{% set cols = items_per_row %}
			{% endif %}
		{% endfor %}
	{% else %}
		{#<p class="text-center">{{ trans._('Hola %name%, aún no tienes una entrada para un evento.', ['name' : '<strong>'~user_data['first_name']~'</strong>']) }}</p>#}
		<p class="text-center">{{ trans._('Haz click %a_open%aquí%a_close% para participar del mundo.',
								['name' : '<strong>'~user_data['first_name']~'</strong>', 'a_open' : '<a href="'~url()~'">', 'a_close' : '</a>']) }}
		</p>
	{% endif %}
	</div>
</div>
