<div id="app-first-content" class="app-event-section">

	<div class="app-event-wrapper">
        {# event head details #}
        {{ partial("event/headerDetails") }}

        {# my tickets #}
        <div id="vue-userEventTicket" class="opaque event-body large-text-justify">
			{# prevents box division bug #}
            <div class="app-line-spacer">&nbsp;</div>

			{# ticket list #}
			<div class="tickets-list" v-cloak>

				<div v-for="category in ticketCategories" class="ticket-category table">

					{# title #}
					<h4 v-text="category.name" class="app-section-title"></h4>

					{# category tickets #}
					<div v-for="ticket in category.tickets">

						<ticket
							:row-id="'ticket-row-' + ticket.index"
							:modal-id="'ticket-modal-' + ticket.index"
							:obj="ticket">
						</ticket>

					</div>
				</div>
			</div>

			{# back & downlaod buttons #}
			{# for large screens #}
			<div class="show-for-large">
				<div class="row">
					<div class="large-4 columns">
						<div class="app-btn-wrapper">
							 <form action="{{ url('account') }}" method="get">
								<button class="app-btn gray">
									<img src="{{ static_url('images/icons/icon-back.png') }}" data-retina alt="" />
									<span>{{ trans._('Volver a Mis Entradas') }}</span>
								</button>
							</form>
						</div>
					</div>
					<div class="large-4 columns">
						&nbsp;
					</div>
					<div class="large-4 columns">
						<div class="app-btn-wrapper">
							<button class="app-btn" v-on:click="getInvoice('{{ invoice_url }}', false)">
								<img src="{{ static_url('images/icons/icon-download-small.png') }}" data-retina alt="" />
								<span>{{ trans._('Descargar Entradas') }}</span>
							</button>
						</div>
					</div>
				</div>
			</div>
			{# for small and medium screens #}
			<div class="btn-blocks-wrapper hide-for-large">
				<div class="app-btn-wrapper">
					<button class="app-btn" v-on:click="getInvoice('{{ invoice_url }}', false)">
						<img src="{{ static_url('images/icons/icon-download-small.png') }}" data-retina alt="" />
						<span>{{ trans._('Descargar Entradas') }}</span>
					</button>
				</div>
				<div class="app-btn-wrapper">
					<form action="{{ url('account') }}" method="get">
					   <button class="app-btn gray">
						   <img src="{{ static_url('images/icons/icon-back.png') }}" data-retina alt="" />
						   <span>{{ trans._('Volver atrás') }}</span>
					   </button>
				   </form>
				</div>
			</div>

		</div>
	</div>
</div>

{# Components #}
{{ partial("components/userEventTicket") }}
