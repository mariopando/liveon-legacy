<div class="my-account text-center">

	{# Breadcrumbs #}
	<ul class="breadcrumbs title hide-for-large">
		<li><span>{{ trans._('Perfil') }}</span></li>
	</ul>

	{# personal data #}
	<div class="profile-wrapper">

		{# socials #}
		<div id="vue-eventSocial" class="socials">
			<div class="facebook-label">
				<img src="{{ static_url('images/icons/icon-fb-bg-white.png') }}" data-retina alt="" />
				<span>{{ trans._('Vinculación con Facebook') }}</span>
			</div>
			<div class="row facebook" v-bind:class="[facebookSwitchState == 1 ? 'active' : '', facebookSwitchState == 2 ? 'loading' : '']">
				<div class="columns small-8 text-left">
					<div v-text="facebookSwitchText" class="sync-text"></div>
				</div>
				<div class="columns small-4 text-right">
					<div class="switch medium radius">
						{# facebook hidden button #}
						<button class="app-btn-fb hide" data-action="login"></button>
						{# handler input switch #}
						<button class="app-switch-handler" v-on:click="facebookSwitchClicked" v-bind:disabled="facebookSwitchDisabled">&nbsp;</button>
						{# switch #}
						<input id="app-facebook-switch" class="switch-input" type="checkbox" disabled />
						<label class="switch-paddle" for="app-facebook-switch"></label>
					</div>
				</div>
			</div>
		</div>

		{# separator #}
		<hr class="empty" />

		{# profile form #}
		<form id="vue-account" data-validate v-on:submit="updateProfile" action="javascript:void(0);">
			{# inputs #}
			<div class="row">
	            <div class="columns small-12">
					<label class="text-left">{{ trans._('Correo electrónico') }}</label>
	            	<input name="email" type="text" size="50" maxlength="255" placeholder="{{ user_data['email'] }}" disabled="disabled" />
				</div>
	        </div>

			<div class="row">
	            <div class="columns small-12">
					<label class="text-left">{{ trans._('Nombre') }}</label>
	            	<input name="first_name" type="text" size="50" maxlength="60" value="{{ user_data['first_name'] }}"
						   data-fv-required="stringLength: { min: 3, max: 60, message: '{{ trans._('Ingresa tu nombre.') }}' }"
						   data-fv-message="{{ trans._('Ingresa tu nombre.') }}" />
				</div>
	        </div>

			<div class="row">
	            <div class="columns small-12">
	        		<label class="text-left">{{ trans._('Apellido') }}</label>
	            	<input name="last_name" type="text" size="50" maxlength="60" value="{{ user_data['last_name'] }}"
						   data-fv-required="stringLength: { min: 3, max: 60, message: '{{ trans._('Ingresa tu apellido.') }}' }"
						   data-fv-message="{{ trans._('Ingresa tu apellido.') }}" />
				</div>
	        </div>

			<div class="row">
	            <div class="columns small-12">
	        		<label class="text-left">{{ trans._('Contraseña actual') }}</label>
            		<input name="current_pass" type="password" size="50" maxlength="20" autocomplete="off" placeholder="********" />
        		</div>
			</div>

			<div class="row">
	            <div class="columns small-12">
	        		<label class="text-left">{{ trans._('Nueva Contraseña') }}</label>
            		<input name="pass" type="password" size="50" maxlength="20" autocomplete="off" />
        		</div>
			</div>

			{# submit button #}
        	<div class="button-wrapper">
				<div class="row">
					<div class="columns small-12">
		            	<button type="submit" class="app-btn">
		                	<img src="{{ static_url('images/icons/icon-app-small.png') }}" data-retina alt="" />
		                	<span>{{ trans._('Guardar') }}</span>
		            	</button>
					</div>
				</div>
            </div>
		</form>

	</div>

</div>
