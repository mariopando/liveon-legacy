
{# index checkout layout #}
<div id="app-first-content" class="app-checkout-wrapper app-form">
	{# Checkout Form #}
	<div id="vue-checkout">
		<h3 class="text-center">{{ event.name }}</h3>

	    <form method="post" data-validate v-on:submit="sendCheckout" v-bind:action="formAction" action="javascript:void(0);">

			<p class="title text-center">{{ event.caption }} </p>
			<p class="subtitle text-center"> {{ event.display_date }} </p>

			{# ticket description if layout applies #}
			{% if in_array('EventTicketAsset', objectsClasses) %}
				<div class="ticket-resume">
					<h4 class="app-section-title">{{ user_ticket.eventTicket.name}}</h4>
				</div>
			{% endif %}
			{# checkout objects #}
			<div class="row head text-center">
				<div class="small-6 medium-7 columns text-left">
					{% if in_array('EventTicket', objectsClasses) %}
						<span>{{ trans._('Ticket') }}</span>
					{% else %}
						<span>{{ trans._('Item') }}</span>
					{% endif %}
				</div>
				<div class="small-3 medium-3 columns">
					<span>{{ trans._('Valor') }}</span>
				</div>
				<div class="small-3 medium-2 columns">
					<span class="show-for-medium">{{ trans._('Cantidad') }}</span>
					<span class="show-for-small-only">{{ trans._('Cant.') }}</span>
				</div>
			</div>

			{# renders data through view model #}
			<div v-cloak>
				<div v-for="obj in objects" class="row checkout-objects small-collapse medium-uncollapse text-center">
					<div class="small-7 medium-7 columns text-left">
						<span class="name" v-text="obj.name">&nbsp;</span>
						<div> <a href="javascript:void(0);" class="app-see-more" v-on:click="obj.seeMore = !obj.seeMore">{{ '['~trans._('Ver más')~']'}}</a> </div>
						<div class="app-small-print-wapper">
							<div v-html="obj.small_print" v-show="obj.seeMore"></div>
						</div>
					</div>
					<div class="small-2 medium-3 columns">
						<span class="price" v-text="obj._ext.price_formatted">&nbsp;</span>
					</div>
					<div class="small-3 medium-2 columns">
						<select v-if="obj.state == 'open'" v-bind:name="obj.nameAttr" v-model="obj.selectedQ" class="small-text-for-small">
							<option v-for="option in amountOptions"
									v-text="option"></option>
	                    </select>
						<p v-if="obj.state == 'soldout'" class="small-text-for-small" style="color:red;">{{ trans._('Agotada') }}</p>
						<p v-if="obj.state == 'closed'" class="small-text-for-small" style="color:#FCC83A;">{{ trans._('Sólo en Boletería') }}</p>
			        </div>
				</div>
			</div>

			{# amount #}
			<div class="row footer small-collapse large-uncollapse text-center">
				<div class="small-6 medium-7 columns">
					&nbsp;
				</div>
				<div class="small-3 medium-3 columns small-text-center medium-text-right small-text-for-small">
					<span>{{ trans._('Total:') }}</span>
				</div>
				<div class="small-3 medium-2 columns small-text-center small-text-for-small">
					<span>$</span> <span v-text="amountFormatted"></span>
				</div>
			</div>

			{# invoice email #}
			<div class="app-invoice-email-box">

				<div class="row text-center">
					<div class="columns small-12">
						{% if in_array('EventTicketAsset', objectsClasses) %}
							<span>{{ trans._('Enviar mi comprobante a:') }}</span>
						{% else %}
							<span>{{ trans._('Enviar mis entradas y comprobante a:') }}</span>
						{% endif %}
						<input name="invoiceEmail" type="text" size="50" maxlength="255" autocomplete="on"
							   value="{{ invoiceEmail }}" placeholder="{{ trans._('Correo electrónico') }}"
							   data-fv-required data-fv-message="{{ trans._('Ingresa un correo electrónico válido.') }}" />
					</div>
				</div>
			</div>

			{# payment method #}
			{% if event.type != "free" %}
				<!--<div class="row head">
					<div class="small-12 columns">
						<span>{{ trans._('Métodos de pago') }}</span>
					</div>
				</div>-->
				<div class="gateways text-center">
					<img src="{{ static_url('images/icons/icon-wp-plus.png') }}" data-retina class="wp-icon" alt="WebPay Plus" />
				</div>

			{% endif %}

	        {# submit button #}
	        <div class="button-wrapper text-center">

				{# common inputs #}
				{% for name,value in checkoutInputs %}
					<input type="hidden" name="{{ name }}" {{ value is empty ? 'v-model="'~(name|lower)~'"' : 'value="'~value~'"' }} />
				{% endfor %}

				{# webpay inputs (key bindings are lower-cased) #}
				{% if webpayInputs is defined %}
					{% for name,value in webpayInputs %}
						<input type="hidden" name="{{ name }}" {{ value is empty ? 'v-model="'~(name|lower)~'"' : 'value="'~value~'"' }} />
					{% endfor %}
				{% endif %}

				{# button #}
				<div class="row text-center">
					<div class="columns small-12">
						<button type="submit" class="app-btn" v-bind:disabled="checkoutDisabled">
			                <img src="{{ static_url('images/icons/icon-app-small.png') }}" data-retina alt="" />
			                <span>{{ event.type != "free" ? trans._('Pagar') : trans._('Siguiente') }}</span>
			            </button>
					</div>
				</div>

				<div class="terms text-center">
					{{ trans._('Antes de realizar tu compra no olvides leer nuestros %a_open%términos de compra%a_close%.',
						["a_open" :'<a href="'~url('content/terms#condiciones-compra')~'" target="_blank">', "a_close" : "</a>"]) }}
				</div>

	        </div>
	    </form>

	</div>
</div>
