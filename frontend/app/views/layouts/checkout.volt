{#  Checkout Layout
	==============================================
#}

{# set bg cover as dynamic style #}
{% set container_style = event is defined ? "background: #111 url('"~event._ext['image_url_poster']~"') no-repeat center center fixed;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;" : "background: #111;" %}

{# Header TopBar #}
{{ partial("templates/header") }}

{# Contents Wrapper #}
<div class="app-account-wrapper app-container-wrapper" style="{{ container_style }}">
    {# content #}
    <div class="app-container app-account-content">
		{{ get_content() }}
	</div>
</div>
