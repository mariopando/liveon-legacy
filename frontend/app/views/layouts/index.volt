{#  Index Parent Layout
	==============================================
#}

{# Index Wrapper #}
<div class="app-index-wrapper app-container-wrapper">
	{#Video Background#}
	<video id="bg-video"></video>
	{# content #}
	{{ get_content() }}
</div>
