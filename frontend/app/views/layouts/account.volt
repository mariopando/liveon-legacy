{#  Account Parent Layout
	==============================================
#}

{# set bg cover as dynamic style #}
{% set container_style = event is defined ? "background: #111 url('"~event._ext['image_url_poster']~"') no-repeat center center fixed;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;" : "" %}

{# Header TopBar #}
{{ partial("templates/header", ['dark_theme': true]) }}

{# Account Wrapper #}
<div class="app-account-wrapper app-container-wrapper" style="{{ container_style }}">
    {# content #}
    <div class="app-container app-account-content">
        {{ get_content() }}
    </div>

</div>
