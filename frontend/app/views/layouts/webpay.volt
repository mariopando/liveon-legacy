{#  Webpay Parent Layout
	=====================
#}

{# Header TopBar #}
{{ partial("templates/header") }}

{# Contents Wrapper #}
<div class="app-contents-wrapper app-container-wrapper app-checkout-closer">
    {# content #}
    <div class="{{ medium_content is defined ? 'app-container' : '' }}">
        <div class="content">
        	<div id="app-first-content" data-fit="-14">
    			{{ get_content() }}
    		</div>
    	</div>
    </div>
</div>
