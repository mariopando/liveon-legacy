{# Header TopBar #}
{{ partial("templates/header") }}

{# Test Wrapper #}
<div class="app-contents-wrapper app-container-wrapper">
    {# content #}
    <div class="{{ medium_content is defined ? 'app-container' : '' }}">
        <div class="content">
        	<div id="app-first-content" data-fit="-14">

				{# TEST Content #}
				<p>
					LiveOn Testing
				</p>

    		</div>
    	</div>
    </div>
</div>
