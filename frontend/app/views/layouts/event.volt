{#  Event Layout
	==============================================
#}

{# set bg cover as dynamic style #}
{% set container_style = event is defined ? "background: #111 url('"~event._ext['image_url_poster']~"') no-repeat center center fixed;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;" : "background: #111;" %}

{# Header TopBar #}
{{ partial("templates/header") }}

{# Event Wrapper #}
<div class="app-container-wrapper" style="{{ container_style }}">
    {# content #}
    <div class="app-container">
		{{ get_content() }}
	</div>
</div>
