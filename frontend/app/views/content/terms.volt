{# Terms View #}
<div>
	<div>
		<h2 class="margin-top-zero">{{ trans._('Términos y condiciones de uso') }}</h2>
	</div>

	<h3>{{ trans._("Condiciones de Uso") }}</h3>
	<p>
	 {{ trans._("Revise las condiciones y políticas expresadas en los siguientes párrafos
	 asociadas a los servicios prestados por LiveOn Technologies SpA.") }}
	</p>

	<p>
	{{ trans._("Las presentes condiciones regulan el uso del sitio Web %a_open%www.liveon.cl%a_close%,
	 en adelante sitio LiveOn, perteneciente a la sociedad LiveOn Technologies SpA RUT: 76.432.282-7, con domicilio en
	 calle Huérfanos 786 oficina 1203, comuna y ciudad de Santiago, Chile.",
	["a_open" : '<a href="'~url()~'">', "a_close" : "</a>"]) }}
	</p>

	<p>
	{{ trans._("El uso de este sitio está sujeto a las condiciones y términos que se detallan (en adelante las %i_open%“Condiciones de Uso”%i_close%),
	 por lo tanto, el Usuario se compromete a aceptarlas y velar por su cumplimiento. Asimismo, el Usuario se compromete a aceptar que las condiciones
	 de uso podrán ser modificadas o adaptadas a cambios tanto legislativos, prácticas generales de la industria, así como a políticas de la empresa.
	 Se entenderá que las modificaciones producirán efectos desde en	el momento en que estén publicadas en el sitio Web.
	 Si usted visita este sitio o es Usuario, se entiende que adquiere conocimiento de las condiciones de uso, aceptándolas.
	 Si no está de acuerdo con estas condiciones de uso, no utilice este sitio.", ["i_open" : "<i>", "i_close" : "</i>"]) }}
	</p>

	<p>
	{{ trans._("El Usuario se compromete a hacer uso del sitio LiveOn sólo para fines personales; tales como consultar información,
	 interacciones con la plataforma y acciones de compra.") }}
	</p>

	<p>
	{{ trans._("Queda prohibida la descarga, modificación o alteración, en cualquier forma, de los contenidos publicados en el sitio.") }}
	</p>
	<p>
	 {{ trans._("El contenido y software de este sitio es propiedad de LiveOn Technologies SpA y está protegido bajo las leyes internacionales y nacionales del derecho de autor. El Usuario se abstendrá
	 de hacer uso del sitio con fines o efectos ilícitos, lesivos de los derechos e intereses de terceros, o que de cualquier forma puedan dañar, inutilizar, sobrecargar, deteriorar o impedir
	 la normal utilización de este sitio, los equipos informáticos o los documentos, archivos y toda clase de contenidos almacenados en cualquier equipo informático de LiveOn.") }}
	</p>
	<p>
	{{ trans._("La violación de las condiciones de uso del sitio implicará la cancelación de su cuenta, anulación de compras y se aplicarán las acciones legales que la empresa estime conveniente.") }}
	</p>


	<h3>{{ trans._("Uso permitido y Condiciones de Venta") }}</h3>
	<p>
	{{ trans._("La venta de Entradas o Tickets con códigos electrónicos para eventos está regulada por la Ley.") }}
	<br>
	{{ trans._("LiveOn puede solicitar información personal del Usuario y éste tiene el deber de completar y actualizar, en el caso que se requiera,
	la información para	 la correcta comunicación entre éste y la empresa. Esta información sólo será accesible para LiveOn Technologies SpA
	quien protegerá la privacidad de sus datos.") }}
	<br>
	{{ trans._("Al comprar una entrada en el sitio LiveOn, el Usuario acepta las condiciones establecidas, y manifiesta su total
	conformidad con las políticas definidas por la empresa.") }}
	<br>
	{{ trans._("Si no podemos verificar o comprobar la autenticidad de cualquier información que el Usuario proporcione durante cualquier proceso de inscripción,
	verificación de autenticidad, compra, pago, o cualquier otro proceso no mencionado aquí, o si no podemos verificar o autorizar la tarjeta de crédito o información
	de cuenta bancaria, entonces se le prohibirá el uso del sitio al Usuario.") }}
	<br>
	{{ trans._("La cuenta creada por el Usuario es de carácter personal y no deberá ser facilitada a otra persona.") }}
	<br>
	{{ trans._("LiveOn ha establecido mecanismos de identificación para el Usuario mediante una contraseña secreta, conocida sólo por el titular de la cuenta.
	Por lo antes expuesto, la plataforma LiveOn asume que es el titular quien ingresa mediante esta validación de identidad.") }}
	<br>
	{{ trans._("En caso de facilitarla a otra persona, será el titular de la cuenta quien deberá asumir las responsabilidades por malos usos o faltas a las políticas y
	condiciones aquí expuestas. Toda la información que facilite el Usuario deberá ser veraz. Por ello el Usuario garantiza la autenticidad de todos aquellos
	datos que comunique. De igual forma, será responsabilidad del Usuario mantener toda la información facilitada a LiveOn Technologies SpA permanentemente actualizada
	de forma que responda, en cada momento, a la situación real del Usuario. En todo caso, el Usuario será el único responsable de las manifestaciones falsas o inexactas
	que realice y de los perjuicios que cause a LiveOn Technologies SpA o a terceros por la información que facilite.") }}
	</p>


	{# ID used for direct link (see below) #}
	<h3>{{ trans._("Condiciones de Compra") }}</h3>
	<p>
	{{ trans._("La venta de entradas u otros productos a través del sitio LiveOn, está limitada a usuarios que se hayan registrado en la plataforma.
	Las entradas son vendidas por LiveOn como agente o en nombre de un Organizador (productor, club, teatro, estadio o universidad u otro tipo de institución).
	El Organizador es el responsable del servicio o evento a realizarse, sujeto a las condiciones de venta que el mismo establezca.") }}
	<br/>
	{{ trans._("El precio final a pagar por cada ticket u otro producto comprado mediante LiveOn, estará conformado de la siguiente manera:") }}
	</p>

	<ul>
		<li>
			{{ trans._("El precio base del ticket o producto.") }}
		</li>
		<li>
			{{ trans._("El cargo por servicio por parte de la empresa para el evento demandado.") }}
		</li>
	</ul>

	<p id="condiciones-compra">
	{{ trans._("Antes de confirmar una compra, el Usuario deberá revisar el detalle de su orden desplegado en pantalla.
	No existe opción de cambio, reembolsos ni devoluciones, excepto en los casos descritos en este
	mismo documento de Términos y Condiciones.") }}
	<br/>
	{{ trans._("La transacción quedará sujeta a la verificación por parte de la empresa emisora de la tarjeta u
	otro medio de pago elegido por el Usuario. No se considerará como completa una transacción mientras
	LiveOn no reciba la autorización de la institución financiera respectiva.") }}
	<br/>
	{{ trans._("En caso que LiveOn no reciba dicha confirmación, se anulará la compra. La posterior revisión y
	aprobación del cargo (en caso que se haya realizado la transacción y no se haya recibido la confirmación),
	quedará sujeta a la disponibilidad de entradas o productos según corresponda a cada evento.") }}
	<br/>
	{{ trans._("La empresa podrá subcontratar, parcial o totalmente, el servicio de cobranza a sus usuarios
	previa firma de un contrato de servicios que asegure la privacidad de los datos y entregue una mejor experiencia
	de compra a sus usuarios.") }}
	</p>


	<h3>{{ trans._("Devoluciones y Cambios de Compras") }}</h3>

	<h4>a) {{ trans._("Devolución Voluntaria.") }}</h4>

	<p>
	{{ trans._("La aceptación o rechazo de una devolución por decisión voluntaria del cliente corresponde exclusivamente al
	 productor del espectáculo y LiveOn no tiene injerencia alguna en esa determinación. El cliente deberá comunicar su deseo de
	 devolver una entrada a través del correo %a_open%"~app.emails.support~"%a_close%
	 hasta 72 horas antes del evento. LiveOn remitirá la solicitud al productor y comunicará su decisión al cliente.",
	 ["a_open" : '<a href="mailto:'~app.emails.support~'" target="_blank">', "a_close" : "</a>"]) }}
	<br/>
	{{ trans._("De ser aprobada la devolución por parte del Productor, el reembolso será gestionado dentro de 5 a 10 días hábiles.
	El método de devolución dependerá exclusivamente de cómo el Usuario haya pagado su entrada. Si el pago fue realizado por medio de
	Tarjetas de Crédito, se le realizará la reversa del cobro vía Transbank, en cambio si fue a través de Débito, se le reembolsará
	mediante transferencia electrónica o cheque cuyo retiro deberá hacer en dependencias de LiveOn. El reembolso se limitará al valor
	del ticket y no se contempla la devolución de los gastos por servicio.") }}
	</p>

	<h4>b) {{ trans._("Devolución por Cancelación del espectáculo.") }}</h4>

	<p>
	 {{ trans._("Se realizará a partir del quinto día hábil después de comunicada oficialmente la cancelación. El método de devolución
	 dependerá exclusivamente de cómo el Usuario haya pagado su entrada. Si el pago fue realizado por medio de Tarjetas de Crédito, se le realizará
	 la reversa del cobro vía Transbank, en cambio si fue a través de Débito, se le reembolsará mediante transferencia electrónica o cheque cuyo
	 retiro deberá hacer en dependencias de LiveOn. El reembolso se limitará al valor del ticket y no se contempla la devolución de los gastos por servicio.") }}
 	</p>

	<h4>c) {{ trans._("Cambio de Entradas.") }}</h4>

	<p>
	 {{ trans._("Ante la decisión del cliente de cambiar su entrada por otra de precio igual o superior a la que compró,
	 deberá comunicarse con el área de Soporte de LiveOn a través de la plataforma de atención sólo hasta 72 horas antes del evento.
	 Una vez recibida la información, el cambio será efectuado dentro de 2 días hábiles siempre que el espectáculo en cuestión tenga
	 disponibilidad de cupo para realizar el cambio. El cliente, en este caso, deberá pagar las diferencias de precio que ocurran,
	 incluyendo diferencias en los valores de cargo por servicio.") }}
	 <br/>
	 {{ trans._("Ante la decisión del cliente de cambiar su entrada por otra de precio menor a la que compró, éste
	 deberá comunicar su deseo de cambiar su entrada a través del correo %a_open%"~app.emails.support~"%a_close%
 	 hasta cinco días hábiles antes del evento. LiveOn remitirá la solicitud al productor quien evaluará la factibilidad
	 de cupo para realizar el cambio dentro de 3 días hábiles. El método de devolución del saldo de dinero dependerá
	 exclusivamente de cómo el Usuario haya pagado su entrada. Si el pago fue realizado por medio de Tarjetas de Crédito,
	 se le realizará la reversa del cobro vía Transbank, en cambio si fue a través de Débito, se le reembolsará mediante
	 transferencia electrónica o cheque cuyo retiro deberá hacer en dependencias de LiveOn. El reembolso se limitará al
	 valor del ticket y no se contempla la devolución de los cargos por servicio.",
 	 ["a_open" : '<a href="mailto:'~app.emails.support~'" target="_blank">', "a_close" : "</a>"]) }}
    </p>


	<h4>d) {{ trans._("Transferencia de Entradas.") }}</h4>
	<p>
		{{ trans._("Un cliente puede transferir una o varias entradas. Este proceso se realiza en la sección
		“Mis Entradas”. Una vez transferida la entrada, ésta dejará de pertenecer al comprador original, y
		estará disponible solo para el receptor de la entrada.") }}
		<br/>
		{{ trans._("La trasferencia de entradas es absoluta responsabilidad del comprador,
		e implica un acuerdo tácito entre comprador y receptor.") }}
	</p>

	<h4>e) {{ trans._("Asignación de Entradas") }}</h4>
	<p>
		{{ trans._("En el caso de querer asignar la entrada a una persona menor de 18 años,
		se solicitará información descriptiva básica como nombre, edad y género, para fines
		de seguridad y restricción de acceso a zonas para mayores de edad.") }}
	</p>

	{# Contents #}
	<h3>{{ trans._('Contenido de Terceros') }}</h3>
	<p>
	 {{ trans._("El sitio podrá contar con enlaces a sitios de terceros sobre los cuales LiveOn no tiene injerencia y por lo tanto
	 no garantiza, representa o asegura que	el contenido de éstos sea exacto, legal y/o inofensivo. Al enlazarse a otros sitios, el
	 Usuario libera de responsabilidades a LiveOn por posibles daños que puedan generarse. Sin perjuicio de lo anterior, LiveOn siempre
	 velará por la seguridad y la exactitud de los contenidos de los enlaces de terceros publicados en su sitio.") }}
	</p>


	<h3>{{ trans._('Políticas de Derecho de Autor') }}</h3>
	<p>
	 {{ trans._("LiveOn Technologies SpA protegerá los derechos de autor y por ende, se verificará la información subida para intentar no infringir este derecho.
	 En el caso que se divulgue contenido sin el previo consentimiento del autor, éste será eliminado de la página tras ser notificados por el dueño de estos derechos
	 o sus agentes, tras recibir una orden judicial o por admisión del Usuario. En la eventualidad que el Usuario vulnere algunas de las disposiciones establecidas en
	 este documento, LiveOn Technologies SpA se reserva el derecho a la cancelación de la cuenta y la prohibición de ingresar en el futuro.") }}
	</p>


	{# Privacy #}
	<h3>{{ trans._('Políticas de Privacidad') }}</h3>
	<p>
	{{ trans._("En virtud de la Ley N°19.628 sobre Protección de la Vida Privada, la empresa respeta el deber de proteger los datos de carácter
	privado y personales de los usuarios, no dando acceso a ellos al público a menos que el propio Usuario los de a conocer, por medio del sitio Web
	bajo su consentimiento, no pudiendo hacer responsable a la empresa por la información que el mismo entregue.") }}
	</p>

	<p>
	{{ trans._("La empresa se compromete a cuidar la seguridad de los datos personales tomando todas las medidas necesarias para esto, a fin de evitar la pérdida,
	mal uso o cualquier apropiación indebida de estos mismos. Sin embargo, el Usuario deberá velar también por su seguridad y no entregar datos que considere privados
	o información de carácter personal que no desea revelar.") }}
	</p>

	<p id="before-facebook">
	{{ trans._("La responsabilidad en el uso de este sitio por parte de menores de edad, recaerá única y exclusivamente en los padres o representantes
	legales de los mismos. El único motivo que hará revelar la información de los datos personales del Usuario será una orden judicial, a fin de defender los intereses
	de la empresa, usuarios y terceros.	Con respecto a la entrega de información privada, es importante destacar que bajo obligación de confidencialidad, la empresa
	puede comunicar los datos personales a otros usuarios si un Usuario consiente en que se cedan sus Datos Personales, sólo en el ambiente de plataforma social. Asimismo,
	no será responsable la empresa de aquellos datos personales que un Usuario entregue a sitios Web que no son de propiedad de la empresa. Tampoco lo será respecto
	de la información que voluntariamente facilite el Usuario al comunicarse con otras personas dentro del sitio Web y menos fuera de éste.") }}
	</p>


	{# facebook privacy #}
	<h3>{{ trans._('Privacidad de Facebook') }}</h3>
	<p>
	{{ trans._("Esta Aplicación podrá solicitar permisos de Facebook que le permitan rescatar el perfil del Usuario y publicar historias en su muro de Facebook.
	Los datos obtenidos son de uso exclusivo para esta aplicación y jamás serán compartidos con terceros. El manejo de estos datos cumplen con los estándares
	de seguridad y las políticas definidas por Facebook.") }}
	</p>


	{# user rules #}
	<h3>{{ trans._('Obligaciones del Usuario') }}</h3>
	<p>{{ trans._("Además de las obligaciones contenidas en estas Condiciones de Uso, el Usuario se obliga a lo siguiente:") }}</p>

	<ul class="circle">
		<li>{{ trans._("No violar las disposiciones de la Ley N° 19.223 sobre Delitos Informáticos, ni de la Ley N°19.628 sobre Protección
			de la Vida Privada, así como de ninguna otra normativa legal.") }}</li>
		<li>{{ trans._("No utilizar este sitio Web con fines delictivos, como perpetrar amenazas electrónicas, ocasionar fraudes
			de diverso orden, producir o almacenar pornografía infantil, realizar suplantación de identidad, utilizar
			este sitio para injuriar o calumniar, entre otros.") }}</li>
		<li>{{ trans._("Usar de forma diligente el Nombre de Usuario y las contraseñas, información que tiene el carácter de
			confidencial y personal. El Usuario se hará responsable de todos los daños y perjuicios, ocasionados por la mala o
			negligente utilización de su Nombre de Usuario o contraseña, ya sea por el mismo o por parte de un tercero. Ante la usurpación
			de Nombre de Usuario y/o clave, es deber del Usuario dar inmediato aviso a la empresa.") }}</li>
		<li>{{ trans._("Hacer uso del sitio Web en conformidad a la ley, a las Condiciones de Uso y respetando la moral, las buenas costumbres y el orden público.") }}</li>
		<li>{{ trans._("No obtener para beneficio propio ni proporcionar a terceros, directorios de usuarios ni ninguna información acerca de otros usuarios,
			incluidas sus direcciones de correo electrónico.") }}</li>
	</ul>


	<h3>{{ trans._("Terminación") }}</h3>
	<p>{{ trans._("Nos reservamos el derecho a denegar, suspender o cancelar el servicio de usuarios que contravengan los Términos y Condiciones, sin previo aviso.") }}</p>


	<h3>{{ trans._("Fallas en el sistema") }}</h3>
	<p>
	{{ trans._("La empresa no se hará responsable por daños, perjuicios o pérdidas de un Usuario, causados por fallas en el sistema, en el servidor o en Internet.
	Tampoco se hará responsable por virus que contaminen los equipos de los usuarios como consecuencia del acceso, uso del sitio o debido a cualquier transferencia de
	datos, archivos, imágenes, textos, audios u otros. No se le podrá imputar responsabilidad a la empresa debido a fallas técnicas que no sean de su responsabilidad y
	que como consecuencia no permitan prestar con continuidad el servicio.") }}
	</p>


	<h3>{{ trans._("Ausencia de derechos del Usuario") }}</h3>
	<p>{{ trans._("Ser Usuario del sitio Web, jamás implicará ser titular de derechos de la empresa; no se entenderán concedidas licencias,
		autorizaciones ni cesión de derechos a los usuarios.") }}</p>


	<h3>{{ trans._("Ley aplicable y solución de controversias") }}</h3>
	<p>
	{{ trans._("Las condiciones de uso se rigen por las leyes chilenas. Toda controversia que se suscite entre un Usuario y la Empresa, se someterá a la jurisdicción de los
	Tribunales de Justicia de Chile, con asiento en la ciudad y Comuna de Santiago.") }}
	</p>

	<p class="version text-right"> {{ trans._("Última modificación: Septiembre del 2015") }}</p>

</div>
