{# Contact #}
<div class="contact app-form">
	<div id="vue-content">

		<h5 class="text-center">{{ trans._('Contáctanos a través de este formulario') }}</h5>

		<form data-validate v-on:submit="sendContact" action="javascript:void(0);">
			<div class="row">
	            <div class="columns small-12">
					<label>{{ trans._('Correo electrónico') }}</label>
					<input name="email" type="email" size="50" maxlength="255" placeholder="{{ trans._('Tu correo') }}" autocomplete="on"
					 	   data-fv-required data-fv-message="{{ trans._('Ingresa un correo electrónico válido.') }}" />
				</div>
    		</div>
			<div class="row">
	            <div class="columns small-12">
					<label>{{ trans._('Nombre') }}</label>
		            <input name="name" type="text" size="50" maxlength="60" placeholder="{{ trans._('Tu nombre') }}"
						   data-fv-required="stringLength: { min: 3, max: 60, message: '{{ trans._('Ingresa tu nombre.') }}' }"
						   data-fv-message="{{ trans._('Ingresa tu nombre.') }}" />
				</div>
	        </div>
			<div class="row">
	            <div class="columns small-12">
					<label>{{ trans._('Mensaje') }}</label>
					<textarea name="message" placeholder="{{ trans._('Escribe tu mensaje') }}" maxlength="200"
							  data-fv-required="stringLength: { min: 3, max: 200, message: '{{ trans._('Escribe tu mensaje.') }}' }"
							  data-fv-message="{{ trans._('Escribe tu mensaje.') }}" ></textarea>
				</div>
			</div>
			<div class="row">
	            <div class="columns small-12 text-center medium-text-right">
					{# submit button #}
					<button type="submit" class="app-btn-transparent gray">{{ trans._('Enviar') }}</button>
				</div>
			</div>
		</form>

	</div>
</div>
