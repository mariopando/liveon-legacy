{# FAQ View #}
<div class="text-left">
	<h2 class="margin-top-zero">{{ trans._('Preguntas Frecuentes') }}</h2>

	<h3>{{ trans._('¿CÓMO OBTENGO MIS ENTRADAS?') }}</h3>
	<p>
		{{ trans._('Al inscribirte en el evento recibirás un correo electrónico con tu entrada en formato PDF.') }}
		<br/>
		{{ trans._('Además puedes acceder a tus entradas a través de tu cuenta para imprimir o descargar.') }}
	</p>

	<h3>{{ trans._('¿CÓMO DEBO PRESENTAR MI ENTRADA?') }}</h3>
	<p>{{ trans._('Mientras sea legible, puedes presentarla de manera digital (en tu celular) o impresa.') }}</p>

	<h3>{{ trans._('¿QUÉ ES UNA PULSERA LIVEON?') }}</h3>
	<p>{{ trans._('Es una pulsera que te permite interactuar digitalmente al interior de un evento.') }}</p>

	<h3>{{ trans._('¿CÓMO CONSIGO MI PULSERA LIVEON?') }}</h3>
	<p>{{ trans._('Te la entregaremos en la entrada, después de haber presentado tu ticket digital.') }}</p>

	<h3>{{ trans._('¿PUEDO COMPARTIR MI PULSERA LIVEON CON OTRA PERSONA?') }}</h3>
	<p>{{ trans._('No. Cada pulsera se asocia con una sola persona.') }}</p>

	<h3>{{ trans._('¿SI PIERDO MI PULSERA LIVEON SE PUEDE RECUPERAR?') }}</h3>
	<p>{{ trans._('¡Por supuesto! Si se pierde tu pulsera, puedes pedir una nueva en zonas destinadas para este fin.') }}</p>

	<h3>{{ trans._('TODAVÍA TENGO ALGUNAS DUDAS') }}</h3>
	<p>
		{{ trans._('¡No te preocupes! Cuando asistas a un evento, el equipo LiveOn te ayudará con cualquier inquietud que tengas.') }}
		<br/>
		{{ trans._('También puedes %a_open1%contactarnos%a_close% a través de nuestro formulario web o a nuestro correo %a_open2%'~app.emails.support~'%a_close%.',
			['a_open1' : '<a href="'~url("contact")~'">',
			 'a_open2' : '<a href="mailto:'~app.emails.support~'">',
			 "a_close" : "</a>" ]) }}
	</p>

</div>
