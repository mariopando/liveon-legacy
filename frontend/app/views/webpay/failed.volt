
{# failed transaction #}
{% if webpay['TBK_ORDEN_COMPRA'] is defined %}
    <h2>{{ trans._("Transacción Rechazada") }}</h2>
    <p>{{ trans._("Tu transacción no ha podido ser procesada, por favor vuelve a intentarlo.") }}</p>
    <p>{{ trans._("N° orden de compra: %buy_order%", ["buy_order" : '<span class="marked">'~webpay['TBK_ORDEN_COMPRA']~'</span>']) }}</p>
{% else %}
    <h2>{{ trans._("Orden de Compra Rechazada") }}</h2>
    <p>{{ trans._("La orden de compra no ha podido ser procesada.") }}</p>
{% endif %}

<p>{{ trans._("Las posibles causas de este rechazo son:") }}</p>

{% if webpay['TBK_ORDEN_COMPRA'] is defined %}
{# transaction exceptions #}
<ul>
    <li>
        {{ trans._("Error en el ingreso de los datos de tu tarjeta de crédito o débito (fecha y/o código de seguridad).") }}
    </li>
    <li>
        {{ trans._("Tu tarjeta de crédito o débito no cuenta con el cupo necesario para cancelar la compra.") }}
    </li>
    <li>
        {{ trans._("Tarjeta aún no está habilitada en el sistema financiero.") }}
    </li>
</ul>
{% else %}
{# expired checkouts exceptions #}
<ul>
    <li>
        {{ trans._("No se ha procesado una orden de compra válida o esta ya fue procesada.") }}
    </li>
    <li>
        {{ trans._("La transacción de compra excedió el tiempo de operación.") }}
    </li>
    <li>
        {{ trans._("La transacción realizada fue rechazada por Transbank.") }}
    </li>
    <li>
        {{ trans._("Ocurrió un problema inesperado con Transbank.") }}
    </li>
</ul>
{% endif %}

<p>
    {{ trans._("Ante cualquier problema no dudes en %a_open%contactarnos%a_close%,
     o envíanos un correo a %m_open%"~app.emails.support~"%a_close%, te responderemos a la brevedad.",
    ['a_open' : '<a href="'~url('contact')~'" target="_blank">', 'a_close':'</a>',
     'm_open' : '<a href="mailto:'~app.emails.support~'">']) }}
</p>

<div class="text-center closer-link">
    {% if checkoutUri is defined %}
        <a href="{{ url(checkoutUri) }}">{{ trans._("Volver atrás") }}</a>
    {% else %}
        <a href="{{ url() }}">{{ trans._("Volver atrás") }}</a>
    {% endif %}
</div>
