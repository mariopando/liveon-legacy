<h1 class="app-print-only">
    {{ app.name }}
</h1>

<h2>{{ trans._("Pago realizado con éxito") }}</h2>

<p class="succes-message">
    {{ trans._("Tu transacción ha sido procesada exitosamente.%break%Te estamos enviando un correo con esta información.", ["break":"<br/>"]) }}
</p>

<div class="checkout-metadata text-left">

    <ul>
        <li><b>{{ trans._("Nombre") }}:</b> <span>{{ user_data['first_name'] ~ " " ~ user_data['last_name'] }} </span></li>
        <li><b>{{ trans._("Comercio") }}:</b> <span> LiveOn Technologies </span></li>
        <li><b>{{ trans._("URL del Comercio") }}:</b> <a href="https://www.liveon.cl"> https://www.liveon.cl </a></li>
        <li><b>{{ trans._("Orden de compra") }}:</b> <span class="marked"> {{ webpay['TBK_ORDEN_COMPRA'] }} </span></li>
        <li><b>{{ trans._("N° de transacción") }}:</b> <span class="marked"> {{ webpay['TBK_ID_TRANSACCION'] }} </span></li>
        <li><b>{{ trans._("Tipo de transacción") }}:</b> <span> {{ trans._("Venta") }} </span></li>
        <li><b>{{ trans._("Fecha transacción") }}:</b> <span> {{ webpay['TBK_DATE_FORMATO'] }} </span></li>
        <li><b>{{ trans._("Tipo de pago") }}:</b> <span> {{ webpay['TBK_TIPO_PAGO'][1] }} </span></li>
        <li><b>{{ trans._("Tipo de cuotas") }}:</b> <span> {{ webpay['TBK_TIPO_PAGO'][2] }} </span></li>
        <li><b>{{ trans._("N° cuotas") }}:</b> <span> {{ webpay['TBK_TIPO_PAGO'][3] }} </span></li> {# webpay['TBK_NUMERO_CUOTAS'] #}
        <li><b>{{ trans._("N° de tarjeta") }}:</b> XXXXXXXXXXXX - <span> {{ webpay['TBK_FINAL_NUMERO_TARJETA'] }} </span></li>
        <li><b>{{ trans._("Monto compra") }}:</b> <span> {{ webpay['TBK_MONTO_FORMATO'] }}</span></li>
    </ul>

</div>

<div class="checkout-details">

    <h2>{{ trans._('Detalle de la compra') }}</h2>

    {# Event details #}
    {% if checkout.event is defined %}
        {% set event_place = checkout.event.eventPlace.eplace %}
        <div class="checkout-event">
            <ul class="text-left">
                <li>
                    <b>{{ trans._('Evento') }}:</b>
                    <span>{{ checkout.event.name }}, <span>{{ checkout.event.caption }}</span>.</span>
                </li>
                <li>
                    <b>{{ trans._('Fecha referencial del evento') }}:</b>
                    <span>{{ checkout.event.display_date }}, {{ trans._('%time% hrs.', ['time' : checkout.event._ext["start_hour"] ]) }}</span>
                </li>
                <li class="last">
                    <b>{{ trans._('Lugar') }}:</b>
                    <span>{{ event_place.name ~ ", " ~ event_place.caption }}</span>
                </li>
            </ul>
        </div>
    {% endif %}

    {% if checkout.objects is defined %}

        <div class="checkout-objects" >
            <table class="checkout-table">
                <thead>
                    <tr>
                        <th>{{ trans._('Compra') }}</th>
                        <th class="text-right">{{ trans._('Cantidad') }}</th>
                        <th class="text-right">{{ trans._('Precio') }}</th>
                        <th class="text-right">{{ trans._('Total') }}</th>
                    </tr>
                </thead>
                <tbody>
                {% for object in checkout.objects %}
                    {# @TODO: Split EventAssets items #}
                    <tr>
                        <td>{{ object.name }}</td>
                        <td class="text-right">{{ object.quantity }}</td>
                        <td class="text-right">{{ object.formattedPrice }}</td>
                        <td class="text-right">{{ object.formattedTotal }}</td>
                    </tr>
                {% endfor %}
                    <tr class="marked">
                        <td>{{ trans._('Total') }}</td>
                        <td></td>
                        <td></td>
                        <td class="text-right">{{ webpay['TBK_MONTO_FORMATO'] }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

    {% endif %}

</div>

<div class="terms">
    {{ trans._('Los reembolsos y devoluciones de compra están sujetos a nuestros %a_open%términos y condiciones.%a_close%',
                ["a_open" :'<a href="'~url('content/terms#condiciones-compra')~'" target="_blank">', "a_close" : "</a>"]) }}
    <br/>
    {{ trans._('En caso de dudas o controversias, porfavor contactarnos a %a_open%'~app.emails.support~'%a_close%.',
        ["a_open" : '<a href="mailto:'~app.emails.support~'" target="_blank">', "a_close" : "</a>"]) }}
</div>

<div class="closer-link">
    <div class="row">
        <div class="small-12 large-4 columns show-for-large">
            <a href="javascript:window.print();" class="app-btn">
                <img src="{{ static_url('images/icons/icon-app-small.png') }}" data-retina alt="" />
                <span>{{ trans._('Imprimir') }}</span>
            </a>
        </div>
        <div class="small-12 large-8 columns small-text-center large-text-left">
            <a href="{{ url('account') }}" class="app-btn">
                <img src="{{ static_url('images/icons/icon-app-small.png') }}" data-retina alt="" />
                <span>{{ trans._('Mis entradas') }}</span>
            </a>
        </div>
    </div>
</div>
