{# SignUp View #}
<div id="vue-auth" class="app-sign-up-wrapper app-form">

    {# Facebook Login/Register #}
    <div class="text-center">
        {# fb-button #}
        <button class="app-btn-fb styled" data-action="login" disabled="disabled" data-fb-loaded="{{ trans._('Regístrate con Facebook') }}">
            <img src="{{ static_url('images/icons/icon-fb-small.png') }}" data-retina alt="" />
            <span>{{ trans._('Cargando facebook...') }}</span>
        </button>

        {# agreement #}
        <p class="agreement text-center">{{ trans._('Al hacer click en Regístrate, aceptas los %a_open%términos%a_close% de
            %app_name%.', ["a_open" : '<a href="'~url('content/terms')~'">', "a_close" : "</a>",
            "app_name" : '<span class="marked">'~app.name~'</span>']) }}
        </p>
    </div>

    {# Line spacer #}
    <hr class="{{ client.lang }}" />

    <h6 class="subheader text-center">{{ trans._('Regístrate con tu correo.') }}</h6>

    {# Register Form #}
    <form data-validate v-on:submit="registerUserByEmail" action="javascript:void(0);">
        <div class="row">
            <div class="columns small-12">
                <input name="email" type="email" size="50" maxlength="255" autocomplete="off" placeholder="{{ trans._('Correo electrónico') }} "
                       value="{{ signup_session['email'] is defined ? signup_session['email'] : '' }}"
                       data-fv-required data-fv-message="{{ trans._('Ingresa un correo electrónico válido.') }}" />
            </div>
        </div>

        <div class="row">
            <div class="columns small-12">
                <input name="pass" type="password" size="50" maxlength="20" autocomplete="off" placeholder="{{ trans._('Crea tu contraseña') }}"
                       data-fv-required="stringLength: { min: 8, message: '{{ trans._('Al menos 8 caracteres.') }}' }"
                       data-fv-message="{{ trans._('Al menos 8 caracteres.') }}" />
            </div>
        </div>

        <div class="row">
            <div class="columns small-12">
                <input name="first_name" type="text" size="50" maxlength="60" placeholder="{{ trans._('Nombre') }}"
                      data-fv-required="stringLength: { min: 3, max: 60, message: '{{ trans._('Ingresa tu nombre.') }}' }"
                      data-fv-message="{{ trans._('Ingresa tu nombre.') }}" />
            </div>
        </div>

        <div class="row">
            <div class="columns small-12">
                <input name="last_name" type="text" size="50" maxlength="60" placeholder="{{ trans._('Apellido') }}"
                       data-fv-required="stringLength: { min: 3, max: 60, message: '{{ trans._('Ingresa tu apellido.') }}' }"
                       data-fv-message="{{ trans._('Ingresa tu apellido.') }}" />
            </div>
        </div>

        {# birthday #}
        <div class="row">
            {# loading phrase #}
            <p class="text-center" v-if="$loadingState"><i>{{ trans._('cargando...') }}</i></p>
            <birthday-selector></birthday-selector>
        </div>

        {# gender #}
        <div class="row">
            <div class="columns small-12">
                <ul class="menu expanded" style="padding-bottom: 8px;">
                    <li class="text-right">
                        <input type="radio" value="male" name="gender" id="gender-m"
                               data-fv-required data-fv-message="{{ trans._('Ingresa tu género.') }}"
                               data-row-invalid="columns" />
                        <label class="cursor-pointer" for="gender-m">{{ trans._('Hombre') }}</label>
                    </li>
                    <li class="text-left">
                        <input type="radio" value="female" name="gender" id="gender-f"
                               data-fv-required data-fv-message="{{ trans._('Ingresa tu género.') }}"
                               data-row-invalid="columns" />
                        <label class="cursor-pointer" for="gender-f">{{ trans._('Mujer') }}</label>
                    </li>
                </ul>
            </div>
        </div>

        {# submit button #}
        <div class="row">
            <div class="columns small-12">
                <button type="submit" class="app-btn">
                    <img src="{{ static_url('images/icons/icon-app-small.png') }}" data-retina alt="" />
                    <span>{{ trans._('Regístrate') }}</span>
                </button>

                {# agreement #}
                <p class="agreement text-center">
                    {{ trans._('Al hacer click en Regístrate, aceptas los %a_open%términos%a_close% de
                                %app_name%.', ["a_open" : '<a href="'~url('content/terms')~'">', "a_close" : "</a>",
                                "app_name" : '<span class="marked">'~app.name~'</span>']) }}
                </p>
            </div>
        </div>
    </form>

    {# Account links #}
     <div class="links text-center">
        <h6 class="subheader margin-top">
            <span>{{ trans._('¿Ya eres usuario de %app_name%?', ['app_name': '<span class="marked">'~app.name~'</span>']) }}</span>
            <a href="{{ url('signIn') }}">{{ trans._('Ingresa') }}</a>
        </h6>
    </div>

</div>

{# Components #}
{{ partial("components/birthdaySelector") }}
