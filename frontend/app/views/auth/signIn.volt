{# SignIn View #}
<div id="vue-auth" class="app-sign-in-wrapper app-form">

    {# Facebook Login/Register #}
    <div class="text-center">
        {# fb button #}
        <button class="app-btn-fb styled" data-action="login" disabled="disabled" data-fb-loaded="{{ trans._('Ingresa con Facebook') }}">
            <img src="{{ static_url('images/icons/icon-fb-small.png') }}" data-retina alt="" />
            <span>{{ trans._('Cargando facebook...') }}</span>
        </button>
    </div>

    {# Line spacer #}
    <hr class="{{ client.lang }}" />

    <h6 class="subheader text-center">{{ trans._('Ingresa con tu correo.') }}</h6>

    {# Login Form #}
    <form data-validate v-on:submit="loginUserByEmail" action="javascript:void(0);">
        <div class="row">
            <div class="columns small-12">
                <input name="email" type="email" size="50" maxlength="255" v-model="loginInputEmail" autocomplete="on" placeholder="{{ trans._('Correo electrónico') }}"
                       data-fv-required data-fv-message="{{ trans._('Ingresa un correo electrónico válido.') }}" />
            </div>
        </div>

        <div class="row">
            <div class="columns small-12">
                <input name="pass" type="password" size="50" maxlength="20" autocomplete="off" placeholder="{{ trans._('Contraseña') }}"
                       data-fv-required data-fv-message="{{ trans._('Ingresa tu contraseña.') }}" />
            </div>
        </div>

        {# submit button #}
        <div class="row">
            <div class="columns small-12">
                <button type="submit" class="app-btn">
                    <img src="{{ static_url('images/icons/icon-app-small.png') }}" data-retina alt="" />
                    <span>{{ trans._('Ingresa') }}</span>
                </button>
            </div>
        </div>
    </form>

    {# Account links #}
    <div class="links text-center">
        <h6 class="subheader">
            <a href="{{ url('password/recovery') }}" class="forgot-account">{{ trans._('¿Olvidaste tu contraseña?') }}</a>
        </h6>
        <h6 class="subheader margin-top">
            <span>{{ trans._('¿Aún no tienes tu cuenta?') }}</span>
            <a href="{{ url('signUp') }}">{{ trans._('Regístrate en %app_name%', ['app_name': app.name]) }}</a>
        </h6>
    </div>

    {# Send Activation Mail Modal Form #}
    <div id="app-modal-account-activation" class="app-block modal-size reveal" data-reveal>
        <div>
            <h2 class="text-center">{{ trans._('Activa tu cuenta') }}</h2>
            <p>{{ trans._('Completa el reCaptcha para reenviarte el correo de activación a %email%.', ['email': '<strong v-text="loginInputEmail"></strong>']) }}</p>

            {# Recaptcha Form #}
            <form data-validate data-fv-live="disabled" v-on:submit="resendActivationMailMessage">
                <div class="row small-collapse">
                    <div class="columns small-12">
                        <div class="recaptcha padding">
                            <small>{{ trans._('Cargando Recaptcha...') }}</small>
                            <div id="app-recaptcha"></div>
                            <input name="reCaptchaValue" type="hidden"
                                   data-fv-excluded="false" data-fv-required="numeric : {}"
                                   data-fv-message="{{ trans._('Completa el reCaptcha.') }}" />
                        </div>
                    </div>
                </div>
                {# submit button #}
                <div class="row">
                    <input name="email" type="hidden" v-model="loginInputEmail" />
                    <div class="columns small-12">
                        <button type="submit" class="app-btn">
                            <img src="{{ static_url('images/icons/icon-app-small.png') }}" data-retina alt="" />
                            <span>{{ trans._('Enviar') }}</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        {# Close modal #}
        <button class="close-button" data-close aria-label="{{ trans._('Cerrar') }}" type="button">
            <span aria-hidden="true">&times;</span>
         </button>
    </div>

</div>
