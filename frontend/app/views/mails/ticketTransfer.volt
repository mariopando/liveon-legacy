{{ partial("mails/mailHeader") }}
		<table class="twelve columns">
			<tr>
			<td>
				<h1>{{ trans._('Hola %name%,', ['name' : '<strong>'~data_user.first_name~'</strong>']) }}</h1>
				<p class="lead">
					{{ trans._('Alguien quiere transferirte una entrada.') }}
				</p>
				<img src="{{ static_url('images/mailing/bg-transfer.jpg') }}" alt="" />
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<table class="row callout">
	<tr>
	<td class="wrapper last">
		<table class="twelve columns">
			<tr>
			<td class="panel">
				{{ trans._('Hola %name1%, %name2% te está transfiriendo una entrada "%ticket_name%" para el evento %event_name% a través de LiveOn, haz click en el siguiente botón para hacer efectiva la transferencia.',
									['name1' : '<strong>'~data_receiver.name~'</strong>',
									 'name2' : '<i>'~data_user.first_name~' '~data_user.last_name~'</i>',
									 'ticket_name' : '<strong>'~data_ticket.name~'</strong>',
									 'event_name' : '<strong>'~data_event.name~'</strong>', 'br' : '<br/>']) }}
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<table class="row">
	<tr>
	<td class="wrapper last">
		<table class="five columns">
			<tr>
			<td>
				<table class="button">
					<tr>
					<td>
						<a href="{{ data_url }}" target="_blank" class="button" mc:disable-tracking> {{ trans._('Aceptar Transferencia') }} </a>
					</td>
					</tr>
				</table>
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
{{ partial("mails/mailFooter") }}
