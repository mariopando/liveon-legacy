{{ partial("mails/mailHeader") }}
		<table class="twelve columns">
			<tr>
			<td>
				<h1>{{ trans._('Hola %name%,', ['name' : '<strong>'~data_user.first_name~'</strong>']) }}</h1>
				<p class="lead">
					{{ trans._('Recupera tu contraseña %app_name%.', ['app_name' : '<strong>'~appName~'</strong>']) }}
				</p>
				<img src="{{ static_url('images/mailing/bg-pass-recovery.jpg') }}" alt="" />
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<table class="row callout">
	<tr>
	<td class="wrapper last">
		<table class="twelve columns">
			<tr>
			<td class="panel">
				{{ trans._('¿Quieres cambiar tu contraseña %app_name%? Si no es así, porfavor ignora este correo.
								%br%Para cambiar tu contraseña haz click en el siguiente botón.',
					['app_name' : '<strong>'~appName~'</strong>', 'br' : '<br/>']) }}
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<table class="row">
	<tr>
	<td class="wrapper last">
		<table class="three columns">
			<tr>
			<td>
				<table class="button">
					<tr>
					<td>
						<a href="{{ data_url }}" target="_blank" class="button" mc:disable-tracking> {{ trans._('Siguiente') }} </a>
					</td>
					</tr>
				</table>
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<table class="row">
	<tr>
	<td class="wrapper last">
		<table class="twelve columns">
			<tr>
			<td>
				<i>{{ trans._('Este enlace te llevará a un sitio seguro donde podrás cambiar tu contraseña.%br%
								El enlace estará disponible sólo por %num% horas.',
									["num" : 24 * (data_token_expiration is defined ? data_token_expiration : 1),
									'br' : '<br/>']) }}</i>
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
{{ partial("mails/mailFooter") }}
