{# Asset checkout template #}
{{ partial("mails/mailHeader") }}
		<table class="twelve columns">
			<tr>
			<td>
				<h1>{{ trans._('Hola %name%,', ['name' : '<strong>'~data_user.first_name~'</strong>']) }}</h1>
				<p class="lead">
					{{ trans._('Gracias por comprar en %app_name%.', ['app_name' : '<strong>'~appName~'</strong>']) }}
				</p>
				<img src="{{ static_url('images/mailing/bg-checkout.jpg') }}" alt="" />
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<table class="row callout">
	<tr>
	<td class="wrapper last">
		<table class="twelve columns">
			<tr>
			<td class="panel success">
				{{ trans._('Tu compra se ha realizado con éxito.
								Todas tus compras están vinculadas a tu pulsera que te entregaremos en el evento.',
								['strong_open' : '<strong>', 'strong_close' : '</strong>']) }}
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<table class="row">
	<tr>
	<td class="wrapper last">
		<table class="three columns">
			<tr>
			<td>
				<table class="button">
					<tr>
					<td>
						<a href="{{ url('signIn') }}" target="_blank" class="button" mc:disable-tracking> {{ trans._('Mi cuenta') }} </a>
					</td>
					</tr>
				</table>
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
{{ partial("mails/mailFooter") }}
