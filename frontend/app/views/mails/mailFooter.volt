				<table class="row">
					<tr>
					<td class="wrapper last">&nbsp;</td>
					<td class="expander"></td>
					</tr>
				</table>
				<table class="row">
					<tr>
					<td class="wrapper last border-top">
						<table class="twelve columns">
							<tr>
							<td align="center">
							<center>
								<small><i>{{ trans._('Este correo ha sido enviado a %email%.%br% Si tienes un problema escríbenos a %support_email%.',
												[
													'email' : '<strong>'~data_email~'</strong>',
													'br' : '<br/>', 'support_email' : '<a class="gray" href="mailto:'~supportEmail~'" mc:disable-tracking><b>'~supportEmail~'</b></a>'
												])
								}}</i></small>
							</center>
							</td>
							<td class="expander"></td>
							</tr>
						</table>
					</td>
					</tr>
				</table>
				<table class="row">
					<tr>
					<td class="wrapper last">
						<table class="twelve columns">
							<tr>
							<td align="center">
							<center>
								<p style="text-align:center;"><a href="{{ url() }}"><span class="app-name">{{ appName }}</span> {{ date('Y') }}</a></p>
							</center>
							</td>
							<td class="expander"></td>
							</tr>
						</table>
					</td>
					</tr>
				</table>
			</td>
			</tr>
		</table>
	</center>
	</td>
	</tr>
</table>

</body>
</html>
