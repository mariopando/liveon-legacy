{# Dummy data for testing #}
{% set agent 	   = data_checkout is defined ? data_checkout.userType : "receiver" %}
{% set ticket_name = data_checkout.eventTicket is defined ? data_checkout.eventTicket.name : "LiveOn Access" %}
{% set author_name = data_checkout.author is defined ? data_checkout.author.props.first_name~' '~data_checkout.author.props.last_name : "CrazyCaker" %}

{# Ticket transfer success #}
{# Asset checkout template #}
{{ partial("mails/mailHeader") }}
		<table class="twelve columns">
			<tr>
			<td>
				<h1>{{ trans._('Hola %name%,', ['name' : '<strong>'~data_user.first_name~'</strong>']) }}</h1>
				<p class="lead">
					{{ trans._('Transferencia de entrada completa.', ['app_name' : '<strong>'~appName~'</strong>']) }}
				</p>
				<img src="{{ static_url('images/mailing/bg-transfer.jpg') }}" alt="" />
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<table class="row callout">
	<tr>
	<td class="wrapper last">
		<table class="twelve columns">
			<tr>
			<td class="panel success">
				{% if agent == "receiver" %}
					{{ trans._('Hola %name%, se completó exitosamente la transferencia de la entrada "%ticket_name%" a tu cuenta, para el evento %event_name%.', [
										 'name' : '<strong>'~data_user.first_name~'</strong>',
										 'ticket_name' : '<strong>'~ticket_name~'</strong>',
										 'event_name' : '<strong>'~data_event.name~'</strong>'
									]) }}
				{% else %}
					{{ trans._('Hola %name1%, %name2% ha aceptado la transferencia de la entrada "%ticket_name%", para el evento %event_name%.', [
										'name1' : '<strong>'~data_user.first_name~'</strong>',
										'name2' : '<i>'~author_name~'</i>',
										'ticket_name' : '<strong>'~ticket_name~'</strong>',
										'event_name' : '<strong>'~data_event.name~'</strong>'
									]) }}
				{% endif %}

			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<table class="row">
	<tr>
	<td class="wrapper last">
		<table class="three columns">
			<tr>
			<td>
				<table class="button">
					<tr>
					<td>
						<a href="{{ url('signIn') }}" target="_blank" class="button" mc:disable-tracking> {{ trans._('Mi cuenta') }} </a>
					</td>
					</tr>
				</table>
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
{{ partial("mails/mailFooter") }}
