{{ partial("mails/mailHeader") }}
		<table class="twelve columns">
			<tr>
			<td>
				<h1>{{ trans._('Hola %name%,', ['name' : '<strong>'~data_user.first_name~'</strong>']) }}</h1>
				<p class="lead">
					{{ trans._('Activa tu cuenta %app_name%.', ['app_name' : '<strong>'~appName~'</strong>']) }}
				</p>
				<img src="{{ static_url('images/mailing/bg-activation.jpg') }}" alt="" />
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<table class="row callout">
	<tr>
	<td class="wrapper last">
		<table class="twelve columns">
			<tr>
			<td class="panel">
				{{ trans._('Haz click en el siguiente botón para activar tu cuenta %app_name%.', ['app_name' : appName]) }}
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<table class="row">
	<tr>
	<td class="wrapper last">
		<table class="four columns">
			<tr>
			<td>
				<table class="button">
					<tr>
					<td>
						<a href="{{ data_url }}" target="_blank" class="button" mc:disable-tracking> {{ trans._('Activar Cuenta') }} </a>
					</td>
					</tr>
				</table>
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
{{ partial("mails/mailFooter") }}
