{{ partial("mails/mailHeader") }}
		<table class="twelve columns">
			<tr>
			<td>
				<h1>{{ trans._('Hola %name%,', ['name' : '<strong>'~data_user.first_name~'</strong>']) }}</h1>
			</td>
			</tr>
			<tr>
			<td>
				<p>{{ trans._('Has desvinculado tu cuenta %app_name% de Facebook.', ['app_name' : appName]) }}</p>
				{% if data_tickets is defined and data_tickets %}
					<p>{{ trans._('Las siguientes entradas requieren vinculación con Facebook: ') }}</p>
					<table class="twelve columns">
					{% for ticket in data_tickets %}
						<tr>
							<td>&bull; {{ ticket.name }} - <span>{{ ticket.code}}</span></td>
						</tr>
					{% endfor %}
					</table>
				{% endif %}
			</td>
			<td class="expander"></td>
			</tr>
			<tr>
			<td class="wrapper last">
				<img src="{{ static_url('images/mailing/bg-pass-recovery.jpg') }}" alt="" />
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<table class="row callout">
	<tr>
	<td class="wrapper last">
		<table class="twelve columns">
			<tr>
			<td class="panel">
				{{ trans._('Haz click en el siguiente botón para vincular nuevamente tu cuenta %app_name% a Facebook.', ['app_name' : appName]) }}
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<table class="row">
	<tr>
	<td class="wrapper last">
		<table class="four columns">
			<tr>
			<td>
				<table class="button facebook">
					<tr>
					<td>
						<a href="{{ data_url }}" target="_blank" class="button" mc:disable-tracking> {{ trans._('Vincular Facebook') }} </a>
					</td>
					</tr>
				</table>
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
{{ partial("mails/mailFooter") }}
