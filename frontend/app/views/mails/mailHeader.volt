<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width" />
	<meta charset="utf-8" />
</head>
<body>
{# hero layout #}
<table class="body">
	<tr>
	<td class="center" align="center" valign="top">
	<center>
		<table class="row header">
			<tr>
			<td class="center" align="center">
			<center>
				<table class="container">
					<tr>
					<td class="wrapper last">
						<table class="twelve columns">
							<tr>
							<td class="six sub-columns">
								<a href="{{ url() }}" target="_blank">
									<img width="200" height="50" src="{{ static_url('images/mailing/logo.png') }}" alt="{{ appName }}" />
								</a>
							</td>
							<td class="six sub-columns last" align="right" style="text-align:right; vertical-align:middle;">
								<span class="template-label"><a href="{{ url('signIn') }}">{{ trans._('Ingresar') }}</a></span>
							</td>
							<td class="expander"></td>
							</tr>
						</table>
					</td>
					</tr>
				</table>
			</center>
			</td>
			</tr>
		</table>
		<br>
		<table class="container">
			<tr>
			<td>
			{# content #}
			<table class="row">
				<tr>
				<td class="wrapper last">
