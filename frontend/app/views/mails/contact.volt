<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<!-- styles -->
	<style type="text/css" media="screen">
		body { background:#fff; color:#000; padding: 10px; font-size: 14px; }
		a  { color: blue; text-decoration: none; }
		h4 { padding: 10px 3px; margin:5px; border-bottom: 1px solid #ccc; font-size: 14px; }
		div { margin: 30px 0; }
	</style>
</head>
<body>
	<p><strong>Nombre:</strong> <span>{{ data_name }}</span></p>
	<p><strong>Email:</strong> <span>{{ data_email }}</span></p>

	<h4>Mensaje</h4>
	<p>{{ data_message|nl2br }}</p>
	<!-- Reply To -->
	<div>
		<a href="mailto:{{ data_email }}?subject=RE:Contacto">
		</a>
	</div>
</body>
</html>
