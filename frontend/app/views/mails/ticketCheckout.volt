{# Ticket checkout template #}
{% set event_place = data_event.eventPlace.eplace %}

{{ partial("mails/mailHeader") }}
		<table class="twelve columns">
			<tr>
			<td>
				<h1>{{ trans._('Hola %name%,', ['name' : '<strong>'~data_user.first_name~'</strong>']) }}</h1>
				<p class="lead">
					{% if data_event.type == "paid" %}
						{{ trans._('Gracias por comprar en %app_name%.', ['app_name' : '<strong>'~appName~'</strong>']) }}
					{% else %}
						{{ trans._('Tienes una nueva entrada en tu cuenta %app_name%.', ['app_name' : '<strong>'~appName~'</strong>']) }}
					{% endif %}
				</p>
				<img src="{{ data_event._ext['image_url_large'] }}" alt="" />
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<table class="row callout">
	<tr>
	<td class="wrapper last">

		<table class="event" cellspacing="5">
			<tr>
			<td width="34">
				<img src="{{ static_url('images/mailing/icon-calendar.png') }}" alt="{{ trans._('Día:') }}" width="25" />
			</td>
			<td>
				<span>{{ data_event.display_date }}</span>
			</td>
			</tr>
			<tr>
			<td width="34">
				<img src="{{ static_url('images/mailing/icon-clock.png') }}" alt="{{ trans._('Hora:') }}" width="25" />
			</td>
			<td>
				<span>{{ trans._('%time% hrs.', ['time' : data_event._ext["start_hour"] ]) }}</span>
			</td>
			</tr>
			<tr>
			<td width="34">
				<img src="{{ static_url('images/mailing/icon-place.png') }}" alt="{{ trans._('Lugar:') }}" width="25" />
			</td>
			<td>
				<span class="name">{{ event_place.name }}</span>
				<br/>
				<span class="caption gray"><a href="{{ event_place.url }}" target="_blank" mc:disable-tracking>{{ event_place.caption }}</a></span>
			</td>
			</tr>
		</table>

	</td>
	</tr>
</table>
<table class="row">
	<tr>
	<td class="wrapper last">
		<table class="four columns">
			<tr>
			<td>
				<table class="button notice">
					<tr>
					<td>
						<a href="{{ data_event.official_url }}" target="_blank" mc:disable-tracking> {{ trans._('Más Info') }} </a>
					</td>
					</tr>
				</table>
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<table class="row">
	<tr>
	<td class="wrapper last">
		<table class="twelve columns">
			<tr>
			<td class="panel">

				{{ trans._('Tu entrada se encuentra en el archivo %open_strong%PDF%close_strong% adjunto.',
								['open_strong' : '<strong>', 'close_strong' : '</strong>']) }}
				<br/>
				{{ trans._('Presenta tu entrada digital impresa o en tu móvil en los puntos de acceso disponibles en el evento.') }}
				{{ trans._('Las entradas son únicas y su venta o re-venta está prohibida.') }}

			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<table class="row">
	<tr>
	<td class="wrapper last">
		<table class="four columns">
			<tr>
			<td>
				<table class="button">
					<tr>
					<td>
						<a href="{{ url('signIn') }}" target="_blank" mc:disable-tracking> {{ trans._('Ir a mi cuenta') }} </a>
					</td>
					</tr>
				</table>
			</td>
			<td class="expander"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
{{ partial("mails/mailFooter") }}
