{# Event Header Details, requires an event object #}
<div class="row small-collapse opaque head united">
    {# 1st row #}
    <div class="poster-wrapper large-8 columns large-text-left medium-text-center">
        <img src="{{ event._ext['image_url_large'] }}" data-retina class="large show-for-large" alt="" />
    </div>
    {# 2st row #}
    <div class="super-row small-12 large-4 columns text-center">
        <div class="row small-collapse">
            {# for small screens #}
            <div class="top-img-wrapper hide-for-large">
                <img src="{{ event._ext['image_url_large'] }}" data-retina alt="" />
            </div>
            <div class="small-12 columns text-center">
                <div class="info-wrapper">
                    <h4 class="app-section-title text-center">{{ event.name }} <span>{{ event.caption }}</span></h4>
                    {# head box #}
                    <div class="head-box">
                        <ul class="text-left">
                            <li>
                                <img src="{{ static_url('images/icons/icon-calendar.png') }}" data-retina alt="" />
                                <span class="small-text-for-small">{{ event.display_date }}</span>
                            </li>
                            <li>
                                <img src="{{ static_url('images/icons/icon-clock.png') }}" data-retina alt="" />
                                <span class="small-text-for-small">{{ trans._('%time% hrs.', ['time' : event._ext["start_hour"] ]) }}</span>
                            </li>
                            <li class="last">
                                <img src="{{ static_url('images/icons/icon-place.png') }}" data-retina alt="" />
                                <span class="small-text-for-small">{{ event.eventPlace.eplace.name }}</span>
                            </li>
                            <li class="last-caption">
                                <span class="caption">{{ event.eventPlace.eplace.caption }}</span>
                            </li>
                        </ul>
                    </div>
                    {# Mini-control large screen (for medium and small this block is replicated below)  #}
                    <div class="sub-menu show-for-large">
                        <div class="row large-collapse large-centered text-center">
                            {% set place_url = event.eventPlace.eplace.url ? event.eventPlace.eplace.url : 'javascript:void(0);' %}
                            {% set place_style = event.eventPlace.eplace.url ? 'active' : 'disabled' %}

                            <div class="columns large-4 {{ place_style }}">
                                <a href="{{ place_url }}" target="_blank">
                                    <img src="{{ static_url('images/icons/icon-place-small.png') }}" data-retina alt="" />
                                    <span>{{ trans._('mapa') }}</span>
                                </a>
                            </div>
                            <div class="columns large-4">
                                <a href="{{ event.official_url is empty ? '#' : event.official_url }}" target="_blank">
                                    <img src="{{ static_url('images/icons/icon-info.png') }}" data-retina alt="" />
                                    <span>{{ trans._('Info') }}</span>
                                </a>
                            </div>
                            <div class="columns large-4 last">
                                <a href="javascript:void(0);" class="app-btn-fb" data-action="share-url" data-url="{{ event.official_url is empty ? '#' : event.official_url }}" >
                                    <img src="{{ static_url('images/icons/icon-fb-bg-white.png') }}" data-retina alt="" />
                                    <span>{{ trans._('share') }}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{# empty space for clearfix effect #}
<div class="opaque" style="height:2px;">
    &nbsp;
</div>
{# Mini-controls Small Screen #}
<div class="opaque divider-wrapper hide-for-large">
    <div class="sub-menu divider">
        <div class="row small-up-3 text-center">
            <div class="column">
                <a href="{{ event.official_url }}" target="_blank">
                    <img src="{{ static_url('images/icons/icon-place-small.png') }}" data-retina alt="" />
                    <span class="hide-for-small-only">{{ trans._('mapa') }}</span>
                </a>
            </div>
            <div class="column">
                <a href="{{ event.official_url is empty ? '#' : event.official_url }}" target="_blank">
                    <img src="{{ static_url('images/icons/icon-info.png') }}" data-retina alt="" />
                    <span class="hide-for-small-only">{{ trans._('Info') }}</span>
                </a>
            </div>
            <div class="column">
                <a href="javascript:void(0);" class="app-btn-fb" data-action="share-url" data-url="{{ event.official_url is empty ? '#' : event.official_url }}" >
                    <img src="{{ static_url('images/icons/icon-fb-bg-white.png') }}" data-retina alt="" />
                    <span class="hide-for-small-only">{{ trans._('share') }}</span>
                </a>
            </div>
        </div>
    </div>
</div>
