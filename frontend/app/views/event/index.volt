{# event layout #}
<div id="app-first-content" class="app-event-section">

    <div class="app-event-wrapper">
        {# event head details #}
        {{ partial("event/headerDetails") }}
        {# event body #}
        <div class="opaque event-body large-text-justify">

            {# event description #}
            <div class="details">
                <div class="rich-content">
                    {{ event.content_html }}
                </div>
            </div>

            {# event prices #}
            <div class="brief-wrapper">

                {# submit button: different actions for event types. #}
                {% if event.type == "free" or event.type == "invitation" %}
                    <div class="{{ 'event-'~event.state }}">
                        {# check if user already has this ticket #}
                        {% if user_owns_ticket is defined and  user_owns_ticket %}
                            <div class="button-wrapper">
                                <form action="{{ url('account/tickets/'~event.namespace) }}" method="get">
                                    <button class="app-btn">
                                        <img src="{{ static_url('images/icons/icon-app-small.png') }}" data-retina alt="" />
                                        <span>{{ trans._('Ver mi entrada') }}</span>
                                    </button>
                                </form>
                                <div class="terms text-center">
                                    {{ trans._('Ya estás subscrito a este evento') }}
                                </div>                                
                            </div>
                        {% else %}
                            {#<p class="price text-center">{{ event.type == "free" ? trans._('Esta entrada es liberada') : trans._('Sólo Invitación') }}</p>#}
                            <div class="button-wrapper">

                                {% set formUrl = event.type == "free" ? url('checkout/tickets/'~event.namespace) : url('account/invitations') %}

                                {% if event.state == 'open' or event.state == 'invisible' %}
                                    <form action="{{ formUrl }}" method="get">
                                        <button class="app-btn">
                                            <img src="{{ static_url('images/icons/icon-app-small.png') }}" data-retina alt="" />
                                            <span>{{ trans._('Obtener entrada') }}</span>
                                        </button>
                                    </form>
                                {% elseif event.state == 'closed' %}
                                    <p class="subtext text-center">{{ trans._('Evento cerrado') }}</p>
                                {% elseif event.state == 'finished' %}
                                    <p class="subtext text-center">{{ trans._('Evento finalizado') }}</p>
                                {% endif %}

                            </div>
                        {% endif %}
                    </div>
                {# paid event type #}
                {% else %}

                    <h4 class="app-section-title"> {{ trans._('Valor de las entradas') }} </h4>
                    {# head box #}
                    <div class="{{ 'event-'~event.state }}">
                        <div class="table">
                            {# tickets loop #}
                            {% for object in tickets %}
                                <div class="row collapse {{ object.state }} {{ loop.index0 == 0 ? '' : 'styled' }}">
                                    <div class="icon-wrapper small-2 medium-2 large-1 columns text-left ">
                                        <span class="icon"><i class="circle-{{ loop.index }}">&nbsp;</i></span>
                                    </div>
                                    <div class="small-7 medium-7 large-8 columns text-left">
                                        <span class="name">{{ object.name }}</span>
                                    </div>
                                    <div class="small-3 medium-3 large-3 columns text-right">
                                        <span class="value">{{ object._ext['price_formatted'] }}</span>
                                    </div>
                                </div>
                            {% endfor %}

                        </div>
                        {# button buy ticket #}
                        <div class="button-wrapper">
                            {# check if event is still available #}
                            {% if event.state == 'open' or event.state == 'invisible' %}
                                <form action="{{ url('checkout/tickets/'~event.namespace) }}" method="get">
                                    <button class="app-btn">
                                        <img src="{{ static_url('images/icons/icon-app-small.png') }}" data-retina alt="" />
                                        <span>{{ trans._('Comprar entrada') }}</span>
                                    </button>
                                </form>

                                {# disclaimer #}
                                {% if event.type == "paid" %}
                                    <div class="terms text-center">
                                        {{ trans._('Venta por sistema incluye recargo por servicio.') }}
                                    </div>
                                {% endif %}

                            {% elseif event.state == 'closed' %}
                                <p class="subtext text-center">{{ trans._('Evento cerrado') }}</p>
                            {% elseif event.state == 'finished' %}
                                <p class="subtext text-center">{{ trans._('Evento finalizado') }}</p>
                            {% endif %}
                        </div>
                    </div>

                {% endif %}
            </div>

        </div>

    </div>
</div>
