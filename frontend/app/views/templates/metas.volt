{# Facebook Sharing Metas #}
<meta property="fb:app_id" content="{{ app.facebook.appID }}" />
<meta property="og:site_name" content="{{ app.name }}" />
{#<meta property="fb:admins" content="100001770616943"/>#}
<meta property="og:title" content="{{ app.name }}" />
<meta property="og:url" content="{{ url() }}" />
<meta property="og:type" content="website"/>
<meta property="og:image" content="{{ static_url('images/bgs/bg-liveon-fb-share.jpg') }}" />
<meta property="og:description" content="{{ trans._('En el borde de la singularidad, un puente digital hacia un mundo humanizado.') }}" />
