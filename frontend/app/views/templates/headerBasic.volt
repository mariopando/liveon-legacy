{#  Header Basic Layout (login, register, password)
	=================================================
#}

{# Logo #}
<div class="logo text-center">
	<a href="{{ url() }}">
		<img src="{{ static_url('images/logos/logo-ls-dark.png') }}" data-retina alt="" />
	</a>
</div>
