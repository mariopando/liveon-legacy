{# Footer, render a fallback for legacy browsers #}
{% if client.isLegacy %}
	<div class="app-footer-legacy text-center">
		<p>&copy; {{ date('Y') }} <span>{{ app.name }}</span> </p>
	</div>
{% else %}
	<footer id="footer">
		{# default footer #}
		<ul class="hide-for-small-only">
			<li><a href="{{ url('content/faq') }}">{{ trans._('Preguntas frecuentes') }}</a></li>
			<li class="dot">&bull;</li>
			<li><a href="{{ url('content/terms') }}">{{ trans._('Términos y condiciones') }}</a></li>
			<li class="dot">&bull;</li>
			<li><a href="{{ url('contact') }}">{{ trans._('Contacto') }}</a></li>
			<li class="dot">&bull;</li>
			<li><span>&copy; {{ date('Y') }}</span> <a href="{{ url() }}" class="marked">Chile</a></li>
		</ul>
		{# small screen footer #}
		<div class="show-for-small-only text-centered stack">
			<div><a href="{{ url('content/faq') }}">{{ trans._('Preguntas frecuentes') }}</a></div>
			<div><a href="{{ url('content/terms') }}">{{ trans._('Términos y condiciones') }}</a></div>
			<div><a href="{{ url('contact') }}">{{ trans._('Contacto') }}</a></div>
			<div><span>&copy; {{ date('Y') }}</span> <a href="{{ url() }}" class="marked">Chile</a></div>
		</div>
	</footer>
{% endif %}
