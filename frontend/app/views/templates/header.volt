{# HEADER TOP BAR #}
<header id="header" class="{% if dark_theme is defined %}{{ "dark" }}{% endif %}">
	<nav>
		<div class="app-left">
			<a href="{{ url() }}">
				<img src="{{ static_url('images/logos/white-logo.png') }}" data-retina alt="" />
			</a>
			{# Logged in header menu #}
			{% if current_view is defined and user_data is defined and user_data %}
			{#{% set current_view = current_view is defined ? current_view : "" %}#}
			<div class="row submenu small-up-3 show-for-large text-center">
				<div class="column">
					<a href="{{ url('account') }}" class="{{ current_view == 'tickets' ? 'active' : 'default' }}">{{ trans._('ENTRADAS') }}</a>
				</div>
				<div class="column text-right">
					<a href="{{ url('account/invitations') }}" class="{{ current_view == 'invitations' ? 'active' : 'default' }}">{{ trans._('INVITACIONES') }}</a>
				</div>
				<div class="column">
					<a href="{{ url('account/profile') }}" class="{{ current_view == 'profile' ? 'active' : 'default' }}">{{ trans._('PERFIL') }}</a>
				</div>
			</div>
			{% endif %}
		</div><!--
	 --><div class="app-right text-right">
 		{# Logged in user info #}
 		{% if user_data is defined and user_data %}
	 		{#<div class="logged-in">#}
	 			{#<a id="app-header-profile" href="javascript:void(0);">#}
					{#<span class="app-user-name hide-for-small-only">{{ user_data['first_name']~" "~user_data['last_name'] }}</span>#}
                    {#<span class="icon-menu-wrapper">#}
						{#<img src="{{ static_url('images/icons/icon-menu.png') }}" data-retina alt="" />#}
                    {#</span>#}
	 			{#</a>#}
	 		{#</div>#}
	 	{# Logged out, sign in-up links #}
	 	{% else %}
			{#<div class="logged-out">#}
				{#<a href="{{ url('signUp') }}" class="sign-up">#}
					{#<img src="{{ static_url('images/icons/icon-signup.png') }}" class="hide-for-small-only" data-retina alt="" />#}
					{#<span class="hide-for-small-only">{{ trans._('regístrate') }}</span>#}
				{#</a>#}
				{#<a href="{{ url('signIn') }}"  class="sign-in">#}
					{#<img src="{{ static_url('images/icons/icon-signin.png') }}" data-retina alt="" />#}
					{#<span class="hide-for-small-only">{{ trans._('ingresar') }}</span>#}
				{#</a>#}
			{#</div>#}
 		{% endif %}
			<a href="#" id="menu_shortcut">
				<img src="{{ static_url('images/icons/icon-hamburguer.png') }}" data-retina alt="" />
			</a>
		</div>
	</nav>
</header>

{# SideBar menu for logged in #}
{% if user_data is defined and user_data %}
	<div id="app-header-menu">
		<div class="menu-content">
			<div class="user-info">
				<img src="{{ static_url('images/icons/icon-close-dark.png') }}" data-retina class="close" alt="" />
				{% if user_data['fb_id'] is defined and user_data['fb_id'] %}
					<span class="app-social-img">
						<img src="{{ user_data['profile_img_path'] }}" alt=""/>
					</span>
				{% else %}
					<span class="app-social-img no-loading">
						<img src="{{ user_data['profile_img_path'] }}" data-retina alt=""/>
					</span>
				{% endif %}
				<div class="text-center">
					<div class="app-user-name">{{ user_data['first_name']~" "~user_data['last_name'] }}</div>
				</div>

			</div>
			<ul class="menu vertical">
				<li>
					<a href="{{ url('account') }}" class="tickets"><span>{{ trans._('Entradas') }}</span><i>&nbsp;</i></a>
				</li>
				<li>
					<a href="{{ url('account/invitations') }}" class="invitations"><span>{{ trans._('Invitaciones') }}</span><i>&nbsp;</i></a>
				</li>
				<li>
		    		<a href="{{ url('account/profile') }}" class="profile"><span>{{ trans._('Perfil') }}</span><i>&nbsp;</i></a>
		    	</li>
				<li>
					<a href="{{ url('content/faq') }}" class="help"><span>{{ trans._('Ayuda') }}</span><i>&nbsp;</i></a>
				</li>
		    	<li>
		    		<a href="{{ url('auth/logout') }}" class="logout"><span>{{ trans._('Salir') }}</span><i>&nbsp;</i></a>
		    	</li>
		    </ul>
		</div>
	</div>
{% endif %}
