{# Invoice PDF Action template (child view of mainLayout template) #}

<h1 class="section">{{ trans._('Comprobante') }}</h1>

{# main details #}
<ul>
    <li><span class="bold">{{ trans._('Nombre') }}:</span> {{ data_user.first_name~" "~data_user.last_name }}</li>

    {# Buy order #}
    {% if data_checkout.buy_order is defined %}
        <li><span class="bold">{{ trans._('Documento') }}:</span> {{ data_checkout.buy_order }}</li>
    {% endif %}

    {# Event #}
    {% if data_checkout.event is defined %}
        <li><span class="bold">{{ trans._('Evento') }}:</span> {{ data_checkout.event.name }}, {{ data_checkout.event.caption }}</li>
    {% endif %}

    {# Action comments #}
    {% if data_checkout.comment is defined %}
        <li><span class="bold">{{ trans._('Observaciones') }}:</span> {{ data_checkout.comment }}</li>
    {% endif %}
</ul>
