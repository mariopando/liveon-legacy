{# Payment Type Header Layout #}

<h1 class="section">{{ trans._('Comprobante') }}</h1>

<ul>
    <li><span class="bold">{{ trans._('Nombre') }}:</span> {{ data_user.first_name~" "~data_user.last_name }}</li>
    <li><span class="bold">{{ trans._('Orden de Compra') }}:</span> {{ data_checkout.buy_order }}</li>
    <li><span class="bold">{{ trans._('Monto (CLP)') }}:</span> {{ data_checkout.amountFormatted }}</li>

    {# Checkout comments #}
    {% if data_checkout.comment is defined %}
        <li><span class="bold">{{ trans._('Observaciones') }}:</span> {{ data_checkout.comment }}</li>
    {% endif %}
</ul>
