{# Event Ticket PDF Checkout template (child view of mainLayout template) #}
<div class="tickets">

    <h1 class="section">{{ data_objects|length > 1 ? trans._('Entradas') : trans._('Entrada') }}</h1>

    {% for ticket in data_objects %}

        {# set event object #}
        {% set event = ticket.eventTicket.event %}

        {% set margin_top  = (loop.index % 2 == 0 and loop.index0 != 0) ? '60px' : '40px' %}
        {% set break_class = (loop.index0 == 0 and data_objects|length > 1) ? 'break-after' : ((loop.index % 2 == 0 or loop.last) ? '' : 'break-after') %}

        <div class="ticket-wrapper {{ break_class }}">
            <div class="ticket" style="margin-top: {{ margin_top }}">
                <div class="header">
                    <div class="inline-block size40">
                        <img src="{{ static_url('images/logos/logo-ls-bw.png') }}" alt="{{ app.name }}" />
                    </div><!--
                 --><div class="inline-block size60 text-right">
                        <span class="inline-block digital">{{ trans._('Entrada Digital') }}</span><span class="inline-block code"><i>{{ ticket.code }}</i></span>
                    </div>
                </div>
                <div class="info">
                    <div class="inline-block half">
                        <h1>{{ event.name }}</h1>
                        <h2 class="margin-bottom">{{ event.caption }}</h2>

                        <h1>{{ ticket.eventTicket.name }}</h1>

                        <ul>
                            <li>{{ ticket.eventTicket.small_print }} </li>
                            <li>Hora: {{ trans._('%time% hrs.', ['time' : event._ext["start_hour"] ]) }}</li>
                            <li>Lugar: {{ event.eventPlace.eplace.name }} <span>{{ event.eventPlace.eplace.caption }}</span></li>
                        </ul>

                    </div><!--
                 --><div class="inline-block half">
                        <img src="{{ data_storage is defined ? data_storage~ticket.code~'.png' : ticket._ext['code_url'] }}" alt="{{ trans._('Código QR') }}" class="inline-block qr-code" />
                        {#<span>{{ data_storage is defined ? data_storage~ticket.code~'.png' : data_storage~ticket.code~'.png' }}</span>#}
                    </div>
                </div>
                <div class="footer">
                    <div>
                        <img src="{{ static_url('images/icons/icon-alert-small.png') }}" alt="" />
                        {{ trans._('Está entrada es única. No se reemplazará en caso de pérdida o robo.') }}
                        <br/>
                        {{ trans._('La venta o re-venta de entradas emitidas está prohibida.') }}
                    </div>
                </div>
            </div>
        </div>
    {% endfor %}

</div>
