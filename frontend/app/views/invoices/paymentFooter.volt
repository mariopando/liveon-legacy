{# Payment Type Footer Layout #}

<div>

    <h1 class="section margin-top">{{ trans._('Detalle de Compra') }}</h1>

    {# Transaction Trx, cames as an object #}
    {% if data_checkout.trx is defined %}
        <ul>
            <li><span class="bold">{{ trans._('N° de transacción') }}:</span> {{ data_checkout.trx.trx_id }}</li>
            <li><span class="bold">{{ trans._('Método de pago') }}:</span> {{ trans._('WebPay Plus') }}</li>
            <li><span class="bold">{{ trans._('Tarjeta') }}:</span> XXXXXXXXXXXX-{{ data_checkout.trx.card_last_digits }}</li>
            <li><span class="bold">{{ trans._('Fecha de la transacción') }}:</span> {{ data_checkout.trx._ext['date_formatted'] }}</li>
        </ul>
    {% endif %}

    {# Event details #}
    {% if data_checkout.event is defined %}
        <div style="margin-top: 40px;">
            <ul class="text-left">
                <li>
                    <strong> {{ data_checkout.event.name }}, {{ data_checkout.event.caption }} </strong>
                </li>
            </ul>
        </div>
    {% endif %}

    {# Table checkout details #}
    {% if data_checkout.objects is defined and data_checkout.objects|length > 0 %}
        <table>
            <thead>
                <tr>
                    <th class="text-left">{{ trans._('Producto') }}</th>
                    <th class="text-right">{{ trans._('Cantidad') }}</th>
                    <th class="text-right">{{ trans._('Precio') }}</th>
                    <th class="text-right">{{ trans._('Total') }}</th>
                </tr>
            </thead>
            <tbody>
            {# values #}
            {% for object in data_checkout.objects %}
                <tr>
                    <td class="large">{{ object.name }}</td>
                    <td class="text-right">{{ object.quantity }}</td>
                    <td class="text-right">{{ object.formattedPrice }}</td>
                    <td class="text-right">{{ object.formattedTotal }}</td>
                </tr>
            {% endfor %}
                <tr class="marked">
                    <td class="bold">{{ trans._('Total (CLP)') }}</td>
                    <td></td>
                    <td></td>
                    <td class="text-right">{{ data_checkout.amountFormatted }}</td>
                </tr>
            </tbody>
        </table>
    {% endif %}

</div>
