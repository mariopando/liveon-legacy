<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>{{ app.name }}</title>
	<!-- styles -->
	{{ partial("invoices/style.volt") }}
</head>
<body>
	{# Main wrapper #}
	<div id="wrapper">
		{# Header #}
		<header>
			<div class="inline-block half">
				<img src="{{ static_url('images/logos/logo-ls-bw.png') }}" alt="{{ app.name }}" />
			</div><!--
		 --><div class="inline-block half text-right">
				<span class="date bold">{{ data_date }}</span>
			</div>
		</header>

		{# Invoice Content #}
		<div class="invoice">

			{# load invoice header layout #}
			<div class="header">

				{{ partial("invoices/"~data_checkout.type~"Header") }}

				<div style="margin-top: 12px;"><i>{{ trans._('En caso de dudas o controversias, porfavor contáctanos a %a_open%'~app.emails.support~'%a_close%.',
						["a_open" : '<a href="mailto:'~app.emails.support~'" target="_blank" class="black">', "a_close" : "</a>"]) }}</i>
				</div>
			</div>
			
			{# EventTicket #}
			{% if data_objects is defined and data_objects|length > 0 and in_array('EventTicket', data_checkout.objectsClasses) %}

				{# load tickets #}
				{{ partial("invoices/eventTicket") }}

				{# NOTE: Instructions, this must be on website #}
				{#<h1 class="section margin-top">{{ trans._('¿Cómo usar tu entrada?') }}</h1>
				<div>
					<p>
						{{ trans._('Presenta tu entrada digital impresa o en tu móvil en los puntos de acceso disponibles en el evento.') }}
						<br/>
						{{ trans._('Las entradas son únicas y su venta o re-venta está prohibida.') }}
						{{ trans._('No se reemplazarán en caso de pérdida o robo.') }}
						<br/>

					</p>
				</div>#}

			{# EventTicketAsset #}
			{% elseif data_objects is defined and data_objects|length > 0 and in_array('EventTicketAsset', data_checkout.objectsClasses) %}

				{# Load payment footer layout (no special cases) #}
				<div class="footer">
					{{ partial("invoices/paymentFooter") }}
				</div>

				{# NOTE: Instructions, this must be on website #}
				{#<h1 class="section margin-top">{{ trans._('¿Cómo usar tus compras?') }}</h1>
				<div>
					<p>
						{{ trans._('Todas tus compras están vinculadas a la pulsera que te entregaremos en el evento.') }}
					</p>
				</div>#}

			{% endif %}

		</div>
	</div>
</body>
</html>
