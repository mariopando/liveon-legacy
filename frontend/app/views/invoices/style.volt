<style>

 /*general */
* { margin: 0; padding: 0; border: 0; outline: 0 none; }
html { height: 100%; }
body { width: 600px; padding: 50px; background: #fff; font-family: Helvetica, Helvetica Neue, Arial, sans-serif; font-size: 16px; color: #000; }
p { line-height: 1.6em; padding: 2px 10px; }
a { color: #85C11E; text-decoration: none; }
a:hover { color: #666; }

/* common classes */
.inline-block { display: inline-block; vertical-align: top; }
.half { width:50%; }
.size60 { width:60%; }
.size40 { width:40%; }
.text-left { text-align: left; }
.text-center { text-align: center; }
.text-right { text-align: right; }
.no-margin { margin: 0; }
.bold { font-weight: bold; }
.section { margin-bottom: 20px; padding-bottom: 4px; border-bottom: 1px dashed #ccc; }
.alert { margin-top: 20px; }
.break-after { page-break-after: always; }
.break-before { page-break-before: always; }
.black { color:#000; font-weight: bold; }

/* contents */
#wrapper { width: inherit; margin: 0 auto; }
header { margin-bottom: 30px; }
header span.date{ position:relative; top: 3px; }
h1 { font-size: 18px; line-height: 26px; }
h1.margin-top { margin-top: 40px; }

/* invoice content */
div.invoice { font-size: 14px; }
div.invoice ul { list-style: none; }
div.invoice ul li { padding: 4px 0; }
div.invoice table { margin-top: 20px; border: 1px solid #efefef; }
div.invoice table tr { padding: 10px; }
div.invoice table th { padding: 10px; }
div.invoice table td { padding: 10px; }
div.invoice table td.large { width: 351px; }
div.invoice i { font-size: 12px; }

/* tickets  */
div.tickets { padding-top:40px; }
div.ticket { width: inherit; margin: 0 auto; overflow-x: hidden; border: 1px solid #000; border-radius: 10px; }
div.ticket div.header { padding: 10px 26px 8px 26px; border-bottom: 1px solid #000; }
div.ticket div.header span { padding-top: 4px; font-size: 18px; font-weight: bold; }
div.ticket div.header span.code { margin:4px 0 0 20px; padding:3px; background:#000; border:1px solid #000; border-radius: 4px;
                                  color: #fff; font-weight: normal; font-size: 12px; text-align: center; }
div.ticket div.header span.code i { font-style: normal; }
div.ticket div.header span.digital { text-transform: uppercase; }
div.ticket div.info { padding: 32px 32px 20px 32px; }
div.ticket div.info h1 { text-transform: uppercase; }
div.ticket div.info h2 { font-size: 18px; line-height: 22px; text-transform: uppercase; }
div.ticket div.info .margin-bottom { margin-bottom: 24px; }
div.ticket div.info img.qr-code { width: 268px; height: 268px; margin:0 auto; padding:1px; border:1px dashed #ddd; }
div.ticket div.info ul { list-style-type: none; margin: 24px 14px 0 0; }
div.ticket div.info ul li { padding: 0 0 10px 3px; font-size: 16px; font-weight: bold; }
div.ticket div.info ul li span { display: block; padding-top: 4px; font-size: 11px; font-weight: normal; color: #222;}
div.ticket div.footer { padding: 0 20px 16px 20px; font-size: 12px; }
div.ticket div.footer > div { position: relative; padding-left: 34px; }
div.ticket div.footer img { position: absolute; top:0; left: 0; }
div.ticket div.footer span { display: block; }

</style>
