<?php
/**
 * Checkout helper trait.
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//CrazyCake Traits
use CrazyCake\Helpers\Dates;
use CrazyCake\Helpers\Forms;

trait CheckoutHelper
{
    /**
     * Renders invoice for debugging
     */
    public function renderInvoiceAction($event_namespace = false)
    {
        if(APP_ENVIRONMENT == 'production')
            $this->_redirectToNotFound();

        //handle response, dispatch to auth/logout
        $this->_checkUserIsLoggedIn(true);

        //get session id
        $checkout = UserCheckout::getLast($this->user_session["id"], 'success');

        if(!$checkout)
            die("Render: No checkouts available for userID: ".$this->user_session["id"]);

        //reduce checkout object
        $checkout = $checkout->reduce();

        //set checkout extended properties
        $checkout->type            = 'payment'; //or action
        $checkout->comment         = "Renderización de testing de este comprobante.";
        $checkout->objects         = UserCheckoutObject::getCollection($checkout->buy_order);
        $checkout->objectsClasses  = [$checkout->objects[0]->object_class];
        $checkout->amountFormatted = Forms::formatPrice($checkout->amount, $checkout->currency);
        $checkout->categories      = $event_namespace ? $event_namespace : ["comic_con_chile"];
        $checkout->event           = Event::findFirstByNamespace($checkout->categories[0]);

        //get trx
        $trx = UserCheckoutTrx::findFirstByBuyOrder($checkout->buy_order);
        //simulates that trx cames as a JSON decoded object
        $checkout->trx = $trx ? $trx->reduce() : null;

        //get tickets
        $tickets = UserEventTicket::getCollectionForUI($this->user_session["id"], $checkout->event->id, false);

        //set PDF settings
        $pdf_settings = [
            "app"           => $this->config->app,
            "data_date"     => Dates::getTranslatedCurrentDate(),
            "data_user"     => User::getById($this->user_session["id"]),
            "data_objects"  => $tickets,
            "data_checkout" => $checkout
        ];

        $html_raw = $this->simpleView->render("invoices/mainLayout", $pdf_settings);
        //render view
        die($html_raw);
    }
}
