<?php
/**
 * Phinx Database Migration file
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 * @link http://docs.phinx.org/
 */

//load composer dependencies
require "packages/composer/vendor/autoload.php";

//set environment vars
define("PROJECT_PATH", __DIR__."/");

$envfile = PROJECT_PATH.".env";
//load dot env vars
$dotenv = new \Dotenv\Dotenv(PROJECT_PATH);

//load environment file
if(is_file($envfile))
    $dotenv->load();

//set app environment name
$app_environment = getenv('APP_ENV');

//return configuration
return [
    "paths" => [
        "migrations" => PROJECT_PATH."db/migrations/"
    ],
    "environments" => [
        "default_migration_table" => "phinxlog",
        "default_database"        => "$app_environment",
        "migration_base_class"    => "DbMigration",
        "$app_environment"        => [
            "adapter" => "mysql",
            "charset" => "utf8",
            "port"    => "3306",
            "host"    => getenv('DB_HOST'),
            "name"    => getenv('DB_NAME'),
            "user"    => getenv('DB_USER'),
            "pass"    => getenv('DB_PASS')
        ]
    ]
];
