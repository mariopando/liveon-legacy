#! /bin/bash
# bower post-install script, copies tool files into project directories.

# interrupt if error raises
set -e

# Current path (tools folder)
CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SCRIPT_NAME=`basename "$0"`

# WebApp paths
CCBOWER_PATH=$CURRENT_PATH"/../"
CCBOWER_PATH="$( cd "$CCBOWER_PATH" && pwd )"

PROJECT_PATH=$CURRENT_PATH"/../../../../"
PROJECT_PATH="$( cd "$PROJECT_PATH" && pwd )"

BACKEND_PATH=$PROJECT_PATH"/backend/"
FRONTEND_PATH=$PROJECT_PATH"/frontend/"
TOOLS_PATH=$PROJECT_PATH"/.tools/"

MAIN_TOOL_FILE="_app"

NOW=$(date +"%d-%m-%Y %R")

echo -e "\033[96mProject Dir: "$PROJECT_PATH" \033[0m"
echo -e "\033[94mCopying packages files... \033[0m"

#creates folder
mkdir -p $TOOLS_PATH

# copy tool files
find $CURRENT_PATH -maxdepth 1 -mindepth 1 -type f -print0 | while read -d $'\0' FILE; do

	#get file props
	FILENAME=$(basename "$FILE")
	EXT="${FILENAME##*.}"
	FILENAME="${FILENAME%.*}"

	echo -e "\033[96mCopying file $FILENAME.$EXT ... \033[0m"
	# exclude files?
	if [ "$FILENAME.$EXT" = "$SCRIPT_NAME" ]; then
        continue
    elif [ "$FILENAME" = "$MAIN_TOOL_FILE" ]; then
        cp $FILE "$PROJECT_PATH/"
    else
		cp $FILE "$TOOLS_PATH/"
	fi

done

#copy volt files
if [ -d $BACKEND_PATH"app/views/" ]; then
	echo -e "\033[96mCopying backend volt files ... \033[0m"
	cp -r $CCBOWER_PATH"/volt/index.volt" $BACKEND_PATH"app/views/index.volt"
fi

if [ -d $FRONTEND_PATH"app/views/" ]; then
	echo -e "\033[96mCopying frontend volt files ... \033[0m"
	cp $CCBOWER_PATH"/volt/index.volt" $FRONTEND_PATH"app/views/index.volt"
fi

# task done!
echo -e "\033[92mScript successfully executed! \033[0m"
