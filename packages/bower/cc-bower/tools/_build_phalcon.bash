#! /bin/bash
# PhalconPHP build script [extended functions]

echo -e "\033[94mPhalcon App Building... \033[0m"

PROJECT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# cc-phalcon directory
APP_CORE_NAMESPACE="cc-phalcon"
APP_CORE_PATH="$PACKAGES_PATH$APP_CORE_NAMESPACE/"

buildTask() {
	# check file is present
	if [ ! -f $TOOLS_PATH"/_translations_phalcon.bash" ]; then
		echo -e "\033[31mTranslations tools are required.\033[0m"
		exit
	fi

	# backend
	if [ -d $BACKEND_PATH"dev/" ]; then
		echo -e "\033[95mExecuting build tasks in backend... \033[0m"
		gulp build -m backend
	fi

	# frontend
	if [ -d $FRONTEND_PATH"dev/" ]; then
		echo -e "\033[95mExecuting build tasks in frontend... \033[0m"
		gulp build -m frontend
	fi

	echo -e "\033[95mCompiling frontend translations... \033[0m"

	# translations for backend & frontend
	cd $PROJECT_PATH
	bash $TOOLS_PATH"/_translations_phalcon.bash" -c -b
	bash $TOOLS_PATH"/_translations_phalcon.bash" -c -f

	# build the phar file and copy to composer folder
	if [ -d $APP_CORE_PATH ]; then
		echo -e "\033[95mMaking a phar library file from $APP_CORE_PATH... \033[0m"
		cd $APP_CORE_PATH
		php box.phar build
		cp "$APP_CORE_NAMESPACE.phar" "$PACKAGES_PATH$APP_CORE_NAMESPACE.phar"
	fi
}
